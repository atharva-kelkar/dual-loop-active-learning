## Scripts for the _Dual-Loop Active Learning_ project
_Authors_: Atharva S. Kelkar, Bradley C. Dallin, Reid C. Van Lehn

_DOI_: 10.33774/chemrxiv-2021-668s4

The directory is divided into sub-folders as follows:

**1. core_functions** – Scripts for running the active learning loop, support functions, analyzing data 
  - **active_learning_functions** – Scripts for running running the GPR model, calculating acquisition function, and selecting pattern that maximizes acquisition function for sampling 
  - **analysis** – Scripts for analyzing output from dual-loop active learning algorithm
      - **autocorrelation** – Codes for calculating autocorrelation metrics, _i.e._, adjusted Geary's C and Moran's I
      - **data_arrays** – Output from fully trained GPR model
      - **global_vars** – Global variables
      - **graph_connectivity** – Codes for calculating graph connectivity metrics, _i.e._, node and edge connectivity, average clustering coefficient
      - **regression** – Codes for compiling all order parameter metrics
      - **SVM** – Codes for training SVM classifier to select most important features for classification
  - **choose_patterns_for_indus** – Scripts to choose patterns with highest and lowest HFE per polar area fraction for sampling using INDUS
  - **gridding** – Functions to make hexagonal grids, Voronoi areas corresponding to each cell
  - **hyperparameter_selection** – Code to select optimal value of \gamma
  - **order_parameter_results** – Comppiled values of order parameters for various patterns
  - **stopping_criterion** – Code for running stabilizing predictions (SP) metric calculation, pickle for stop set of SP metric
  - **trajectory_tools** – Bash script for centering trajectory on the patterned patch

**2. seed_pattern_generation** – Scripts for generating seed patterns and their labels

**3. trained_gpr_models** – Fully trained GPR models for each end group along with INDUS and CNN generated SAM labels


## Versions of software used

- NumPy 1.16.1
- TensorFlow 1.14
- Docker container for running GPU-enabled 3D CNN codes - atharvakelkar/tensorflow_1.14.0_keras_gpu

