#!/bin/bash

n_patterns=500

############### INPUT TO CODE #################
output_folder=gpr_stop_patterns/
output_file_prefix=stop_pattern
output_file_postfix=.txt
n=4 # Ligands in n x n pattern
n_tot=4 # Total number of ligands in n_tot x n_tot pattern
mol_frac=-1


############### MAKE FOLDER TO STORE FILES #################
mkdir -p $output_folder


############### RUN LOOP TO RUN CODES #################
for (( index=0; index<$n_patterns; index++ )); do

	echo $index
	python3.4 seed_pattern_generation.py ${mol_frac} ${n} ${output_folder} ${output_file_prefix}_${index}${output_file_postfix} ${n_tot} 

done

