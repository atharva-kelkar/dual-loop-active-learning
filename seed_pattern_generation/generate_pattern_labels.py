# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 02/11/2021
"""

import numpy as np
import pickle



if __name__ == "__main__":
    
    '''
    Define file name for patterns and labels
    '''
    patterns_file = 'compiled_pickle_patterns.pickle'
    labels_file = 'label_files/seed_pattern_labels_amide.txt'
    output_file = 'master_patterns_file_amide.pickle'
    
    print('***** MAKE SURE LABELS FILE IS INDEXED PROPERLY (Not an error message; just a warning) *****')
    pure_sams_included = 1
    sigma = 2
    print('Using a sigma value of {:.2f} for all labels'.format( sigma ) )
    
    '''
    Load patterns
    '''
    patterns = np.load( patterns_file )
    
    '''
    Load labels file
    '''
    labels = np.loadtxt( labels_file )
    
    '''
    Check shape of labels file and patterns file
    '''
    if labels.shape[0] != patterns.shape[0]:
        print('*** Shape mismatech between patterns {} and labels {}'.format( patterns.shape, 
                                                                              labels.shape 
                                                                              ) )
        
    '''
    Make array of the following form:
        [ pattern, label, sigma ]
    '''
    res = []
    for count in range( labels.shape[0] ):
        res.append([ patterns[count], 
                   labels[count],
                   sigma ]
                    ) 
    
    res = np.array( res )
        
    '''
    Write res array as pickle
    '''
    pickle.dump( res,
                open(output_file, 'wb') )
        
        
        
        
        
        
        
        
        
        
        
        
        
