# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 11/19/2020

"""

import numpy as np
import pickle


if __name__ == "__main__":
    
    # Input parameters
    n_patterns = 52
    n_tot = 8
    n_relevant = 4
    base_dir = 'gpr_seed_patterns/'
    file_prefix = 'seed_pattern_'
    file_postfix = '.txt'
    
    # Define array of all blank patterns
    seed_pattern_arr = []
    
    # Compile all into 1 numpy array
    for n_curr in range(n_patterns):
        curr_pattern = np.loadtxt( base_dir + file_prefix + str(n_curr) + file_postfix )
        curr_pattern = curr_pattern[ :n_relevant, :n_relevant ].reshape( n_relevant**2 )
        seed_pattern_arr.append( curr_pattern )
        
    seed_pattern_arr = np.array( seed_pattern_arr )
    
    pickle.dump( seed_pattern_arr, open( 'compiled_pickle_patterns.pickle', 'wb') )
        
    
