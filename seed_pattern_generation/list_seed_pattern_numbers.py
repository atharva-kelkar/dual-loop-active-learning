# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 06/29/2021

"""

'''
Code to calculate pattern number of seed patterns
'''

import numpy as np


def pattern_to_number( patterns ):
    
    if len( patterns.shape ) == 1:
        patterns = patterns[None, :]
    ## MAKE ARRAY OF n AND 2^n
    n_arr = np.arange( patterns.shape[1] )[::-1]
    two_raised_to_n = 2**n_arr
    
    ## ADD PATTERN * 2^n
    pattern_number = np.sum( patterns * two_raised_to_n[None, :], 
                            axis = 1 
                            )
    return pattern_number


if __name__ == "__main__":
    
    ## CHOOSE ANY MASTER PATTERNS FILE SINCE ALL HAVE THE ORDER OR PATTERNS
    patterns_pickle = "master_files/master_patterns_file_amine.pickle"

    ## LOAD DATA FROM PICKLE
    data = np.load( patterns_pickle, 
                   allow_pickle = True 
                   )
    
    ## MAKE ARRAY OF PATTERNS
    patterns = np.array( [x[0] for x in data] )

    ## ADD PATTERN * 2^n
    pattern_number = pattern_to_number( patterns )
