# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 10/15/2020

"""

import numpy as np
import sys

if __name__ == "__main__":
    testing = False
    if testing == False:
        mol_frac = float(sys.argv[1])    
        n = int(sys.argv[2])
        output_folder = sys.argv[3] 
        output_file = sys.argv[4]   
        n_tot = int(sys.argv[5])
    if testing == True:
        mol_frac = -1
        n = 4
        index = "-1"
        output_folder = "./"
        output_file = "test"
        n_tot = 8
    
    if mol_frac == -1:
        pattern = np.random.randint(0, 2, n*n ).reshape( n, n )
        
    pattern_full = np.zeros( (n_tot, n_tot) )
    pattern_full[:] = 2
    
    pattern_full[:n, :n] = pattern
    print( pattern.sum() )
    np.savetxt(output_folder + output_file, pattern_full.astype('int'), fmt = '%1d')