# -*- coding: utf-8 -*-
"""

@author: Atharva Kelkar
@date: 06/03/2021

"""

## IMPORT SUPPORTING FUNCTIONS
import numpy as np, time, sys, socket

## ADD R:\\ TO PATH IF SERVER IS NOT SWARM
if 'swarm' not in socket.gethostname():
    sys.path.append(r'R:\\')
    import matplotlib.pyplot as plt

## IMPORT MATPLOTLIB CORRECTLY IF USING SWARM
if 'swarm' in socket.gethostname():
    import matplotlib
    matplotlib.use('agg')
    import matplotlib.pyplot as plt
    ## ADD CORE_FUNCTIONS TO PYTHON PATH
    sys.path.append('/home/kelkar2/analysis_scripts/python_scripts/gpr/core_functions/')

## NOTE START TIME
start_time = time.time()

## IMPORT PREDICTIONS (WITH VORONOI AREA CALC) 
from analysis.gpr_model_analysis_voronoi_area import Predictions_wVoronoi

## IMPORT HISTOGRAMMING FUNCTION
from analysis.histogram_gpr_preds_per_num_polar import histogram_and_get_mid_points, make_bar_plot

## IMPORT PLOTTING FUNCTIONS
from python_scripts.plotting.plotting_functions import load_default_params

'''
Class to histogram predictions based on Voronoi area fraction bins, inherits properties from Predictions_wVoronoi
    Inputs:
        - folder_name: Name of folder containing GPR predictions
        - file_name: Name of file containing GPR predictions
        - master_folder_name: Name of folder containing master HFE predictions pickle
        - master_file_name: Name of file of master HFE predictions pickle
        - n_current: Number of current prediction (Usually 1500 - last index of GPR predictions)
        - voronoi_areas_pickle: Pickle containing mapped Voronoi areas from Voronoi calculations
        - bin_width: Width of area fraction histogram

'''
class Histogram_AreaFrac( Predictions_wVoronoi ):
    '''
    __init__ constructor
    '''
    def __init__( self,
                 folder_name, 
                 file_name, 
                 master_folder_name, 
                 master_file_name,
                 n_current,
                 voronoi_areas_pickle = '../gridding/average_voronoi_areas.pkl',
                 bin_width = 0.02
                 ):
        ## INITIATE PARENT CLASS
        Predictions_wVoronoi.__init__( self, 
                                      folder_name,
                                      file_name,
                                      voronoi_areas_pickle = voronoi_areas_pickle
                                      )
        ## LOAD PREDICTIONS
        self.load_predictions()
        ## LOAD MASTER MEAN AND STD
        self.load_mean_std( master_folder_name, master_file_name, n_current )
        ## UNSCALE MU AND SIGMA VALUES
        self.unscale_mu_sigma()        
        ## MAKE POLAR AREA FRAC USING VORONOI AREAS
        self.make_voronoi()
        
        ## ALLOCATE REMAINING VARIABLES
        self.bin_width = bin_width
        
    '''
    Function to calculate histogram bins based on bin_width
    '''
    @staticmethod
    def define_bins( array, bin_width ):
        
        ## GENERATE BINS FROM LOWER_LIMIT TO UPPER_LIMIT IN INCREMENTS OF BIN_WIDTH
        bins = np.arange( array.min(), 
                         array.max(), 
                         bin_width 
                         )
        ## ADD UPPER_LIMIT AS RIGHT END OF BINS
        return np.append( bins, 
                         array.max()
                         )
    
    '''
    Static method to generate histogram based on bins of mole fraction
        Returns:
            - histogram of given values_to_histogram
            - bar_width for making bar plot
            - mid pts of histogram bins
            
    '''
    @staticmethod
    def gen_histogram( values_to_histogram, bins ):
        
        histogram, bar_width, bar_mid_pts = histogram_and_get_mid_points( values_to_histogram, 
                                                                         bins = bins
                                                                         )
        return histogram, bar_width, bar_mid_pts
    
    '''
    Function to make bar plot from histogram
    '''
    def make_bar_plot( self ):
        ## DEFINE AND LOAD PARAMETERS FOR FORMATTING PLOTS
        x_label = 'Polar area fraction'
        y_label = 'Number of patterns'
        kwargs = load_default_params( x_label, y_label )
        kwargs['want_grid'] = 1
        
        ## MAKE BAR PLOT
        fig, ax = plt.subplots()
        ax = make_bar_plot( ax,
                           self.bar_mid_pts, 
#                           hist/hist.sum(), 
                           self.histogram, 
                           self.bar_width, 
                           0,
                           0,
                           0,
                           add_normal = 'no',
                           make_arrow = 'no',
                           **kwargs 
                           )
    
    '''
    Compute function
    '''
    def compute_hist( self, make_bar_plot = False ):
        ## GENERATE BINS
        self.bins = self.define_bins( self.polar_area_frac_all, 
                                     self.bin_width
                                     )
        ## GENERATE HISTOGRAM
        self.histogram, self.bar_width, self.bar_mid_pts = self.gen_histogram( self.polar_area_frac_all,
                                                                              self.bins
                                                                              )
        ## GENERATE BAR PLOT
        if make_bar_plot:
            self.make_bar_plot()
    

if __name__ == "__main__":
    
    testing = False
    
    if testing:
        n_current = 1500
        bin_width = 0.02
    else:
        n_current = int(sys.argv[1])
        bin_width = float(sys.argv[2])
    ## DEFINE FILE AND FOLDER NAMES
    ligand = 'amide'
    base_dir = '../../active_learning_amide/'
    folder_name = base_dir + 'active_learning_patterns/'
    file_name = 'gpr_prediction_iteration_{:d}.pickle'.format( n_current )
    
    ## DEFINE MASTER FOLDER AND MASTER FILE NAME
    master_folder_name = base_dir +  'pattern_labels/'
    master_file_name = 'master_patterns_labels.pickle'
    
    ## CREATE OBJECTS OF HISTOGRAM_AREAFRAC CLASS
    histogram = Histogram_AreaFrac( folder_name,
                                   file_name, 
                                   master_folder_name, 
                                   master_file_name, 
                                   n_current,
                                   bin_width = bin_width
                                   )
    
    ## RUN COMPUTE FUNCTION
    histogram.compute_hist()

    ## SAVE FIGURE
    plt.savefig( 'figures/20210603_histogram_{}_{}_binwidth_{}.png'.format( ligand, n_current, bin_width ),
                bbox_inches = 'tight'
                )























