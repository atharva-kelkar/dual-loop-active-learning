#!/bin/bash

bin_width=(0.02 0.03 0.04 0.05 0.06)

n_current=1500

for width in ${bin_width[@]}; do
	
	python3.4 histogram_gpr_preds_per_area_frac.py ${n_current} ${width}

done
