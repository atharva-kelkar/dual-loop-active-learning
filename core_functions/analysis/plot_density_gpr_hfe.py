# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 06/07/2021

"""


## IMPORT SUPPORTING FUNCTIONS
import numpy as np, time, sys, socket, copy, pandas as pd

## ADD R:\\ TO PATH IF SERVER IS NOT SWARM
if 'swarm' not in socket.gethostname():
    sys.path.append(r'R:\\')
    import matplotlib.pyplot as plt

## IMPORT MATPLOTLIB CORRECTLY IF USING SWARM
if 'swarm' in socket.gethostname():
    import matplotlib
    matplotlib.use('agg')
    import matplotlib.pyplot as plt
    ## ADD CORE_FUNCTIONS TO PYTHON PATH
    sys.path.append('/home/kelkar2/analysis_scripts/python_scripts/gpr/core_functions/')

## IMPORT PARENT CLASS NEEDED IN CODE
from histogram_gpr_preds_per_area_frac import Histogram_AreaFrac

## IMPORT CHOOSE_MAXDEV CLASS FOR HIST FUNCTION
from choose_patterns_for_indus.choose_max_dev_gpr_preds_area_frac import Choose_MaxDev_AreaFrac

## IMPORT HISTOGRAMMING FUNCTION
from analysis.histogram_gpr_preds_per_num_polar import histogram_and_get_mid_points, make_bar_plot

## IMPORT GRIDDATA FUNCTION
from scipy.interpolate import griddata

## IMPORT PLOTTING FUNCTIONS
from python_scripts.plotting.plotting_functions import load_default_params, format_plot
import plotting_functions as plot
from analysis.pattern_analysis import plot_linear_interpolation

## IMPORT PREDICTIONS_WVORONOI CLASS TO PLOT DEVIATIONS
from gpr_model_analysis_voronoi_area_envelope import Predictions_wVoronoi

'''
class to make an HFE density plot, inherits properties from Histogram_AreaFrac
'''
class HFEDensityPlot( Histogram_AreaFrac ):
    '''
    __init__ constructor
    '''
    def __init__( self,
                 folder_name, 
                 file_name, 
                 master_folder_name, 
                 master_file_name,
                 n_current,
                 voronoi_areas_pickle = '../gridding/average_voronoi_areas.pkl',
                 x_bin_width = 0.02,
                 y_bin_width = 0.5
                 ):
        
        ## INITIALIZE PARENT CLASS
        Histogram_AreaFrac.__init__( self,
                                    folder_name, 
                                    file_name, 
                                    master_folder_name, 
                                    master_file_name,
                                    n_current,
                                    voronoi_areas_pickle = voronoi_areas_pickle,
                                    bin_width = x_bin_width
                                    )
        
        ## RUN COMPUTE HISTOGRAM FUNCTION
        self.compute_hist( make_bar_plot = False )
        
        ## ALLOCATE REMAINING VARIABLES
        self.x_bin_width = x_bin_width
        self.y_bin_width = y_bin_width
        
        
    '''
    Method to make bins in the y directions
    '''
    @staticmethod
    def make_y_bins( bin_labels, values_to_bin, bin_width ):
        
        res = []
        
        for bin_label in np.unique( bin_labels ):
            hfe_to_bin = values_to_bin[ bin_labels == bin_label ]
            hfe_bins = Histogram_AreaFrac.define_bins( hfe_to_bin,
                                                      bin_width
                                                      )
            print( bin_label, hfe_bins.shape )
            if len( hfe_bins ) > 1:
                hfe_histogram, hfe_bar_width, hfe_bar_mid_pts = histogram_and_get_mid_points( hfe_to_bin, 
                                                                                             bins = hfe_bins
                                                                                             )
            elif len( hfe_bins ) == 1:
                hfe_histogram, hfe_bar_width, hfe_bar_mid_pts = np.array( [1] ), 0, hfe_to_bin[0]
                
            res.append( [ bin_label, hfe_bar_mid_pts, hfe_histogram ] )
        
        return np.array( res )
    
    '''
    Method to make x and y grid
    '''
    def make_x_y_grid( self ):
        
        x_grid_all = np.array([])
        y_grid_all = np.array([])
        count_all = np.array([])
        
        for array in self.y_bins:
            ## DEFINE QUANTITIES FROM Y-BIN ARRAY
            area_frac_bin = array[0]
            y_mid_pt = array[1]
            y_count = array[2]
    
            ## DEFINE X MID PT BASED ON AREA FRACTION
            print( int(area_frac_bin) )
            x_mid_pt = self.bar_mid_pts[ int(area_frac_bin) ]
            
            ## DEFINE X ARRAY TO BE THE SAME AS X MID PT ARRAY
            x_grid_arr = np.repeat( x_mid_pt, len( y_mid_pt ) )
            
            ## APPEND X, Y, AND COUNT GRID ARRAYS
            x_grid_all = np.concatenate( (x_grid_all, x_grid_arr) )
            y_grid_all = np.concatenate( (y_grid_all, y_mid_pt) )
            count_all = np.concatenate( (count_all, y_count) )
            
        self.x_grid_all = np.array( x_grid_all )
        self.y_grid_all = np.array( y_grid_all )
        self.count_all = np.array( count_all )
        
    
    '''
    Compute function
    '''
    def compute_density_plot( self ):
        ## ALLOCATE BINS TO POLAR_AREA_FRAC_ALL
        self.area_frac_bins = Choose_MaxDev_AreaFrac.allocate_bins( self.polar_area_frac_all,
                                                                   self.bins
                                                                   )
        ## ALLOCATE LAST BIN NUMBER TO LAST GROUP
        self.area_frac_bins[-1] = self.bins.shape[0] - 2
        
        ## MAKE HFE BINS FOR EVER
        self.y_bins = self.make_y_bins( self.area_frac_bins,
                                       self.unscaled_mu,
                                       self.y_bin_width
                                       )

        ## MAKE X AND Y GRIDS
        self.make_x_y_grid()
    
    
    '''
    Static method to make mid points
    '''
    @staticmethod
    def make_midpts( edges ):
        left_edges = edges[:-1]
        mid_points = left_edges + ( left_edges[1] - left_edges[0] )/2
        
        return mid_points
    
    '''
    Function to interpolate points on y
    '''
    def interpolate_on_y( self ):
        
        y_int = []
        for y_slice in self.hist_2d:
            y_int.append( griddata( self.y_midpts_2d, y_slice, self.y_midpts_2d_finer ) )
            
        return np.array( y_int )
    
    '''
    Static method to import positive deviation values
    '''
    @staticmethod
    def import_pos_dev( ligand, polar_area_frac_all ):
        ## IMPORT POSITIVE DEVIATION
        indus_file = 'dev_hfe/dev_all_area_frac.xls'
        sheet_name = 'pos_{}'.format( ligand )
        
        ## READ EXCEL FILE
        data = pd.read_excel( indus_file, 
                             sheet_name
                             )
        
        x = polar_area_frac_all[ data['Pattern Number'] ]
        y = data['HFE (kT)']
        
        return x, y
    
    '''
    Static method to compute area fraction and hfe within given area frac limits
    '''
    @staticmethod
    def select_within_frac( area_frac, hfe, lower_lim, upper_lim ):
        mask = (area_frac >= lower_lim) * (area_frac <= upper_lim)
        return area_frac[ mask ], hfe[ mask ]   
    
    '''
    Compute function for 2D histogram
    '''
    def compute_2d_hist( self ):
        ## CREATE X AND Y ARRAYS FOR HISTOGRAMMING
        self.hist_x = self.polar_area_frac_all
        self.hist_y = self.unscaled_mu
        
        ## MAKE 2D HISTOGRAM OF X AND Y ARRAYS
        self.hist_2d, self.bins_2d_x, self.bins_2d_y = np.histogram2d( np.ndarray.flatten( self.hist_x ), 
                                                                      np.ndarray.flatten( self.hist_y ),
                                                                      [35, 50]
                                                                      )
        
        ## PLOT 2D HISTOGRAM
        fig, ax = plt.subplots()
        my_cmap = copy.copy( matplotlib.cm.get_cmap('cool') ) # 'cool' works best
        my_cmap.set_under('w')
        my_cmap.set_over('w')
        
        self.x_midpts_2d = self.make_midpts( self.bins_2d_x )
        self.y_midpts_2d = self.make_midpts( self.bins_2d_y )
        
        ## CREATE FINER Y GRID
        self.y_midpts_2d_finer = np.linspace( self.y_midpts_2d.min(),
                                             self.y_midpts_2d.max(),
                                             num = 100,
                                             )
        # X GRID IS NEVER USED
        self.x_midpts_2d_finer = np.linspace( self.x_midpts_2d.min(),
                                             self.x_midpts_2d.max(),
                                             num = 1000,
                                             )
        
        ## INTERPOLATE ALL DATAPOINTS ON Y GRID
        self.hist_2d = self.hist_2d / np.max( self.hist_2d, axis = 1 )[ :,None ]
        self.hist_2d_int = self.interpolate_on_y()
        
        X, Y = np.meshgrid( self.x_midpts_2d, self.y_midpts_2d_finer )
        
        C = self.hist_2d_int.T #+ 0.5 * (self.hist_2d_int.T != 0)
#        C[ C == -np.inf ] = 0
        
        ## PLOT COLORMESH
        colormesh = ax.pcolor( X, Y,
                                  C,
                                  cmap = my_cmap,
                                  vmin = 0.000001,
                                  shading = 'nearest',
                                  alpha = 1.0,
                                  snap = True
                                  )
        
        ## ADD COLORBAR
        fig.colorbar( colormesh, 
                     ax = ax 
                     )

        ## DEFINE AND LOAD PARAMETERS FOR FORMATTING PLOTS
        x_label = 'Polar area fraction'
        y_label = 'HFE (k$_B$T)'
        kwargs = load_default_params( x_label, y_label )
        
        ax.set_ylim([40,105])
        ax = format_plot( ax, **kwargs )

#        X, Y = np.meshgrid( self.x_midpts_2d, self.y_midpts_2d )
#        C = self.hist_2d.T + 500 * (self.hist_2d.T != 0)
#        
#        ax.contourf( X, Y,
#                    C,
#                    cmap = my_cmap,
#                    vmin = 1,
#                    levels = np.linspace( C.min(), C.max(), num = 20 )
#                    )

        return ax
    
    '''
    Compute function for 2D histogram followed by choosing single bin to plot as distribution
    '''
    def compute_single_dist( self, ligand = 'amine', bin_number = 7 ):
        ## CREATE X AND Y ARRAYS FOR HISTOGRAMMING
        self.hist_x = self.polar_area_frac_all
        self.hist_y = self.unscaled_mu
        
        ## MAKE 2D HISTOGRAM OF X AND Y ARRAYS
        self.hist_2d, self.bins_2d_x, self.bins_2d_y = np.histogram2d( np.ndarray.flatten( self.hist_x ), 
                                                                      np.ndarray.flatten( self.hist_y ),
                                                                      [16, 50]
                                                                      )
        
        ## PLOT 2D HISTOGRAM
        fig, ax = plt.subplots()
        
        self.x_midpts_2d = self.make_midpts( self.bins_2d_x )
        self.y_midpts_2d = self.make_midpts( self.bins_2d_y )
        
        ## CREATE FINER Y GRID
        self.y_midpts_2d_finer = np.linspace( self.y_midpts_2d.min(),
                                             self.y_midpts_2d.max(),
                                             num = 100,
                                             )
        
        ## INTERPOLATE ALL DATAPOINTS ON Y GRID
        self.hist_2d = self.hist_2d / np.max( self.hist_2d, axis = 1 )[ :,None ]
        self.hist_2d_int = self.interpolate_on_y()

        ## CREATE OBJECTS OF PREDICTION CLASS
        predictions = Predictions_wVoronoi( folder_name , file_name )
        
        ## COMPUTE ENVELOPE
        predictions.compute_envelope( master_folder_name, 
                                     master_file_name, 
                                     n_current, 
                                     bin_width = 0.04
                                     )
        
        ## MAKE POLAR AREA FRAC FOR MASTER PATTERNS
        predictions.make_master_voronoi()
        
        ## EXTRACT POSITIVE DEVIATION DATA
        self.pos_dev_area_frac, self.pos_dev_hfe = self.import_pos_dev( ligand, 
                                                                       self.polar_area_frac_all 
                                                                       )
        
        ## DEFINE X LOWER AND UPPER LIMIT
        self.x_lower_lim = self.bins_2d_x[ bin_number ]
        self.x_upper_lim = self.bins_2d_x[ bin_number + 1 ]
        
        ## SELECT HISTOGRAM DATA TO PLOT
        self.histogram_x = self.y_midpts_2d_finer
        self.histogram_y = self.hist_2d_int[ bin_number, : ]
        
        ## SELECT SEED POINTS TO PLOT
        self.seed_area_frac_selected, self.seed_hfe_selected = self.select_within_frac( predictions.master_indus_polar_area_frac,
                                                                                       predictions.master_labels_indus,
                                                                                       self.x_lower_lim,
                                                                                       self.x_upper_lim
                                                                                       )  
        
        ## SELECT POS DEV POINTS TO PLOT
        self.pos_dev_area_frac_selected, self.pos_dev_hfe_selected = self.select_within_frac( self.pos_dev_area_frac, 
                                                                                             self.pos_dev_hfe,
                                                                                             self.x_lower_lim,
                                                                                             self.x_upper_lim
                                                                                             )  
        
        ## MAKE NEW FIGURE
        fig, ax = plt.subplots()
        
        ## PLOT HISTOGRAM
        ax.plot( self.histogram_x, 
                self.histogram_y,
                '-',
                linewidth = 3
                )
        
        ## PLOT SEED POINTS
        ax.plot( self.seed_hfe_selected,
                np.zeros( self.seed_hfe_selected.shape[0] ),
                'x',
                markersize = 12,
                markeredgewidth = 3,
                color = 'black'
                )
        
        ## PLOT POS DEV POINTS
        ax.plot( self.pos_dev_hfe_selected,
                np.zeros( self.pos_dev_hfe_selected.shape[0] ),
                '+',
                markersize = 12,
                markeredgewidth = 3,
                color = 'tab:red'
                )
        
        
        ## DEFINE AND LOAD PARAMETERS FOR FORMATTING PLOTS
        x_label = 'HFE (k$_B$T)'
        y_label = 'Relative probability'
        kwargs = load_default_params( x_label, y_label )
        
#        ax.set_ylim([40,105])
        ax = format_plot( ax, **kwargs )
        
        ax.set_xlim([50, 70])

        return ax








if __name__ == "__main__":
    
    testing = True
    
    if testing:
        n_current = 1500
        bin_width = 0.04
    else:
        n_current = int(sys.argv[1])
        bin_width = float(sys.argv[2])
    ## DEFINE FILE AND FOLDER NAMES
    ligand = sys.argv[1]
    base_dir = '../../active_learning_{}/'.format( ligand )
    folder_name = base_dir + 'active_learning_patterns/'
    file_name = 'gpr_prediction_iteration_{:d}.pickle'.format( n_current )
    
    ## DEFINE MASTER FOLDER AND MASTER FILE NAME
    master_folder_name = base_dir +  'pattern_labels/'
    master_file_name = 'master_patterns_labels.pickle'
    
    ## CREATE OBJECTS OF HISTOGRAM_AREAFRAC CLASS
    histogram = HFEDensityPlot( folder_name,
                               file_name, 
                               master_folder_name, 
                               master_file_name, 
                               n_current,
                               x_bin_width = bin_width
                               )
    
    ## PLOT DENSITY PLOT
    plt.rcParams['figure.figsize'] = (8,6)
    ax = histogram.compute_2d_hist()
    
    ## PLOT SINGLE DISTRIBUTION
#    ax = histogram.compute_single_dist( ligand = ligand )
    
#    plt.savefig('figures/{}_distribution_{:.2f}_inset.svg'.format( ligand, np.mean((histogram.x_lower_lim, histogram.x_upper_lim))), 
#                bbox_inches = 'tight',
#                format = 'svg'
#                )

    save_fig = False
    ## SAVE FIGURE
    if save_fig:
        fig_name = 'figures/20210715_density_plot_{}_{}.svg'.format( ligand, n_current )
        plt.savefig(fig_name, 
                    bbox_inches = 'tight',
                    format = fig_name.split('.')[-1]
                    ) 
    
    ## CREATE OBJECTS OF PREDICTION CLASS
    predictions = Predictions_wVoronoi( folder_name , file_name )
    
    ## COMPUTE ENVELOPE
    predictions.compute_envelope( master_folder_name, 
                                 master_file_name, 
                                 n_current, 
                                 bin_width = 0.04
                                 )
    
    ## CODE FOR PLOTTING DEVIATION POINTS ON DENSITY PLOT
    to_plot_dev = True
    
    if to_plot_dev:
            
        ## PLOT DEVIATION
        ax = plot.plot_deviation( ax, 
                                 ligand,
                                 predictions.polar_area_frac_all,
                                 plot_neg = True
                                 )
        ax.set_xlim( [-0.05,0.84] )
                
        save_fig = True
        if save_fig:
            fig_name = 'figures/20210716_density_plot_{}_with_dev.png'.format( ligand )
            plt.savefig( fig_name, 
                        bbox_inches = 'tight',
                        format = fig_name.split('.')[-1]
                        ) 
        
    ## CODE FOR PLOTTING DEVIATION POINTS ON DENSITY PLOT
    to_plot_seed = False
    
    if to_plot_seed:
        ## MAKE POLAR AREA FRAC FOR MASTER PATTERNS
        predictions.make_master_voronoi()

        ax.plot( predictions.master_indus_polar_area_frac[ :52 ], 
                predictions.master_labels_indus[ :52 ], 'x', 
                markersize = 12, color = 'black', markeredgewidth = 3,
                label = 'Seed patterns', zorder = 2
                )
        
        ax = plot_linear_interpolation( ax,
                                       predictions.unscaled_mu[0],
                                       predictions.unscaled_mu[-1],
                                       predictions.polar_area_frac_all.min(),
                                       predictions.polar_area_frac_all.max()
                                       )
        
#        ax.plot( predictions.master_indus_polar_area_frac[ 52: ], 
#                predictions.master_labels_indus[ 52: ], 'v', 
#                markersize = 8, color = 'red', markeredgewidth = 3,
#                markerfacecolor = 'None',
#                label = 'Non-seed INDUS patterns',
#                )
        
        x_label = 'Polar area fraction'
        y_label = 'HFE (k$_B$T)'
        kwargs = load_default_params( x_label, y_label )
        kwargs['want_legend'] = 1

        ax.set_xlim( [-0.05,0.84] )
        
        ax = format_plot( ax, **kwargs )
        
        save_fig = True
        if save_fig:
            fig_name = 'figures/20210714_density_plot_{}_with_seed_and_pos_dev.svg'.format( ligand )
            plt.savefig( fig_name, 
                        bbox_inches = 'tight',
                        format = fig_name.split('.')[-1]
                       ) 
        
        
        
        
        
        
        
        
        
        
        
        
        
    
    
