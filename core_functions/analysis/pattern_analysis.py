# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 01/05/2021

"""

import numpy as np
import matplotlib.pyplot as plt
plt.rcParams['figure.figsize'] = [6,6]
from analysis_scripts.python_scripts.autocorrelation.spatial_autocorrelation.geary_c \
                                    import make_position_array,\
                                    flatten_array,\
                                    make_weight_matrix,\
                                    calc_geary_c

def plot_linear_interpolation( ax, lower, upper, min_num, max_num, color = 'black' ):
    ax.plot( [ min_num, max_num ],
            [ lower, upper ],
            '--',
            linewidth = 3,
            color = color,
            label = 'Linear interpolation'
            )
    return ax


def plot_by_num_polar( patterns ):
    polar_groups_seed = np.sum( patterns.seed_patterns, axis = 1)
    polar_groups_new = np.sum( patterns.new_patterns, axis = 1)
    
    fig, ax = plt.subplots()
    
    ax.plot( polar_groups_new, patterns.new_labels, 
            'x', markersize = 12, markeredgewidth = 4, 
            color = 'red', alpha = 1.0, label = 'Patterns explored')
    ax.plot( polar_groups_seed, patterns.seed_labels, 
            'o', markersize = 12, color = 'blue', label = 'Seed patterns')
    
    ax.set_xlabel('Number of polar groups', fontsize = 15)
    ax.set_ylabel('HFE (k$_B$T)', fontsize = 15)
    ax.tick_params(axis='both', which='major', labelsize= 14)
    for axis in ['top','bottom','left','right']:
        ax.spines[axis].set_linewidth(2)
    leg = ax.legend(fontsize = 14)
    leg.get_frame().set_edgecolor('k')
    leg.get_frame().set_linewidth(2)
    

def plot_by_num_polar_indus_only( patterns ):
    polar_groups_seed = np.sum( patterns.seed_patterns, axis = 1)
    polar_groups_new = np.sum( patterns.new_patterns, axis = 1)
    
    fig, ax = plt.subplots()
    
    
    ax.plot( polar_groups_seed, patterns.seed_labels, 
            '^', markersize = 12, color = 'green', label = 'Seed patterns')
    ax.plot( polar_groups_new, patterns.new_labels, 
            'x', markersize = 12, markeredgewidth = 4, 
            color = 'red', alpha = 1.0, label = 'Patterns explored')
    
    ax.set_xlabel('Number of polar groups', fontsize = 15)
    ax.set_ylabel('HFE (k$_B$T)', fontsize = 15)
    ax.tick_params(axis='both', which='major', labelsize= 14)
    for axis in ['top','bottom','left','right']:
        ax.spines[axis].set_linewidth(2)
    leg = ax.legend(fontsize = 14)
    leg.get_frame().set_edgecolor('k')
    leg.get_frame().set_linewidth(2)
    plot_linear_interpolation( ax, 
                              patterns.seed_labels[ -2 ], 
                              patterns.seed_labels[ -1 ],
                              np.sum(patterns.patterns_array, axis = 1 ).min(),
                              np.sum(patterns.patterns_array, axis = 1 ).max()
                              )



def plot_by_num_polar_geary_C( patterns_array, labels, autocorrelation ):
    polar_groups = np.sum( patterns_array , axis = 1)
#    polar_groups_new = np.sum( patterns.new_patterns, axis = 1)
    
    fig, ax = plt.subplots()
    
    ax.plot( polar_groups[ np.ndarray.flatten( autocorrelation > 1 ) ], 
            labels[ np.ndarray.flatten( autocorrelation > 1 ) ], 
            'o', markersize = 12, fillstyle = 'none', alpha = 1,
            markeredgecolor = 'blue', markeredgewidth = 3,
            label = 'Gearys C > 1')
    ax.plot( polar_groups[ np.ndarray.flatten( autocorrelation < 1 ) ], 
            labels[ np.ndarray.flatten( autocorrelation < 1 )  ], 
            '^', markersize = 10, fillstyle = 'none', alpha = 1,
            markeredgecolor = 'red', markeredgewidth = 3,
            label = 'Gearys C < 1')
    
    ax.set_xlabel('Number of polar groups', fontsize = 15)
    ax.set_ylabel('HFE$_{GPR}$ (k$_b$T)', fontsize = 15)
    ax.tick_params(axis='both', which='major', labelsize= 14)
    for axis in ['top','bottom','left','right']:
        ax.spines[axis].set_linewidth(2)
    leg = ax.legend(fontsize = 14)
    leg.get_frame().set_edgecolor('k')
    leg.get_frame().set_linewidth(2)

'''
Function to set all nans in array to 0
'''
def set_nan_0( X ):
    X[ np.where(np.isnan(X)) ] = 0
    return X


'''
Patterns class
'''
class Patterns:
    
    def __init__( self, folder_name, file_name ):
        self.folder_name = folder_name
        self.file_name = file_name
    
    '''
    Function to load data from pickle file
    '''
    def load_file(self):
        self.data = np.load( self.folder_name + self.file_name )
    
    '''
    Function to make numpy array of only patterns given complete dataset
    '''
    def make_pattern_array( self ):
        a = [ x[0] for x in self.data ]
        self.patterns_array = np.array( a )
    
    '''
    Function to return labels array
    '''
    def make_labels_array( self ):
        self.labels = self.data[: , 1]
    
    '''
    Function to return sigma array
    '''
    def make_sigma_array( self ):
        self.sigma = self.data[: , 2]
    
    '''
    Functions splits all arrays into
        1. seed patterns/labels/sigma
        2. new patterns/labels/sigma
    Given number of split (52 is default for 52 seed patterns)
    '''
    def split_into_two( self, num_split ):
        self.seed_patterns = self.patterns_array[ : num_split ]
        self.seed_labels = self.labels[ : num_split ]
        self.seed_sigma = self.sigma[ : num_split ]
        
        self.new_patterns = self.patterns_array[ num_split : ]
        self.new_labels = self.labels[ num_split : ]
        self.new_sigma = self.sigma[ num_split : ]
        
        try:
            self.seed_autocorrelation = self.autocorrelation[ : num_split, : ]
            self.new_autocorrelation = self.autocorrelation[ num_split : , : ]
        except:
            print( '*** No autocorrelation values found ***' )
        
    '''
    Functions splits all arrays into
        1. sigma = 2
        2. sigma != 2
    '''
    def split_by_sigma( self, sigma ):
        self.patterns_array = self.patterns_array[ self.sigma == sigma ]
        self.labels = self.labels[ self.sigma == sigma ]
        self.sigma= self.sigma[ self.sigma == sigma ]
        
        
    '''
    Function to calculate Moran's I or Geary's C for all patterns
    '''
    def calc_moransI_gearyC( self ):
        self.autocorrelation = np.zeros( (self.patterns_array.shape[0] ,1) )
        count = 0
        
        for pattern in self.patterns_array:
            num = int(pattern.shape[0]**(1/2))
            x_pos_array, y_pos_array = make_position_array( pattern.reshape(num, num) )
            x_pos_array = flatten_array( x_pos_array )
            y_pos_array = flatten_array( y_pos_array )
            eps = 0.001
            weight_matrix = make_weight_matrix( x_pos_array, 
                                               y_pos_array, 
                                               'inverse_dist_squared', 
                                               cutoff = 1 + eps )
            self.autocorrelation[ count ] = calc_geary_c( pattern, weight_matrix )
            count += 1

        self.autocorrelation = set_nan_0( self.autocorrelation )

if __name__ == "__main__":
    
    '''
    Define file and folder name of master pickle file
    '''
    folder_name = '../pattern_labels/'
    file_name = 'master_patterns_labels_onlySeed.pickle'
    
    '''
    Load relevant pickle file
    '''
    patterns = Patterns( folder_name, file_name )
    
    '''
    Make pattern and label and std dev arrays
    '''
    patterns.load_file()
    patterns.make_pattern_array()
    patterns.make_labels_array()
    patterns.make_sigma_array()

    patterns.calc_moransI_gearyC()
    
#    patterns.split_by_sigma( 2 )
    patterns.split_into_two( 52 )
    
    '''
    Plot HFE (either INDUS or CNN) of studied patterns as a function of number of polar groups
    '''
#    plot_by_num_polar( patterns )
#    plt.savefig('20210205_polar_group_patterns_studied.png',
#                bbox_inches = 'tight'
#                )
    
    '''
    Plot HFE of INDUS patterns 
    '''
    patterns_indus = Patterns( folder_name, file_name )
    
    patterns_indus.load_file()
    patterns_indus.make_pattern_array()
    patterns_indus.make_labels_array()
    patterns_indus.make_sigma_array()
    
    patterns_indus.split_by_sigma( 2 )
    patterns_indus.split_into_two( 52 )
    
    plot_by_num_polar_indus_only( patterns_indus )
    plt.savefig('20210216_polar_group_indus_only_seed.png', bbox_inches = 'tight')
    
    '''
    Plot HFE by polar group numbers separated by Geary's C
    '''
#    patterns.split_into_two( 52 )
#    plot_by_num_polar_geary_C( patterns )
#    plt.savefig('20210127_gearyC_patterns_studied.png')
    
    '''
    Plot HFE by polar group numbers separated by Geary's C
    '''
#    patterns_indus.calc_moransI_gearyC()
#    plot_by_num_polar_geary_C( patterns.patterns_array,
#                              patterns.labels,
#                              patterns.autocorrelation
#                              )
#    plt.savefig('20210203_gearyC_patterns_studied.png')














    