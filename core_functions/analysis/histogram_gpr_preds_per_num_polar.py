# -*- coding: utf-8 -*-
"""

@author: Atharva Kelkar
@date: 04/30/2021

"""

import numpy as np
import sys
sys.path.append(r'R:\\')
import time
start_time = time.time()
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
plt.rcParams['figure.figsize'] = (8,6)
from analysis_scripts.python_scripts.plotting.plotting_functions import format_plot, load_default_params

'''
Returns N(x; mu, sigma) given x, mu, sigma
'''
def evaluate_normal( X, mu, sigma ):
    return (1/(np.sqrt(2*np.pi)*sigma)*np.exp( -1/2 * (X - mu)**2/(2*sigma**2) ))
    

'''
Function to make 1-sided arrow given x, y, dx, dy, delta
'''
def make_arrow( ax, x, y, dx, dy, 
               head_width = 0.010, 
               head_length = 1, 
               linestyle = '-',
               color = 'black'
               ):
    ax.arrow( x, y,
            dx, 0,
            head_width = head_width, head_length = head_length,
            fc = color, ec = color,
            linestyle = linestyle,
            linewidth = 2,
            length_includes_head = True
            )
    return ax

'''
Function to make arrows for std dev
'''
def make_dev_arrow( ax, mean, sigma, dev,
                      linestyle = '-',
                      color = 'black'
                      ):
    ## DEFINE DELTA VALUE FOR OVERLAPPING ARROWS
    delta = 2
    
    ## DEFINE LOW AND HIGH VALUES OF X AND Y COORDINATES
    low_x = mean - dev
    high_x = mean + dev
    y = evaluate_normal( low_x, mean, sigma )
#    high_y = evaluate_normal( high_x, mean, sigma )
    
    ## PLOT LEFT TO RIGHT ARROW ON PLOT
    ax = make_arrow( ax, high_x - delta, y, -(high_x - delta - low_x), 0,
                    linestyle = linestyle, color = color
                    )
    
    ## PLOT RIGHT TO LEFT ARROW ON PLOT
    ax = make_arrow( ax, high_x - delta, y, delta, 0, 
                    linestyle = linestyle, color = color
                    )    
    
    return ax

    
'''
Function to make bar plot given bins, histogram values, width of bin, and formatting parameters
'''
def make_bar_plot( ax,
                  bins, 
                  hist, 
                  width, 
                  mean, sigma, rmse, 
                  add_normal = 'no', make_arrow = 'no', 
                  color = 'seagreen',
                  label = '',
                  **kwargs ):
        
    ax.bar( bins, hist, width = width, color = color, label = label )
    
    normal_x = np.linspace( bins.min(), bins.max(), 2*bins.shape[0] )
    if add_normal == 'yes':
        ax.plot( normal_x, evaluate_normal( normal_x, mean, sigma),
                color = 'black',
                linewidth = 2
                )
    
    if make_arrow == 'yes':
        ax = make_dev_arrow( ax, mean, sigma, sigma )
        print(rmse)
        time.sleep(15)
        ax = make_dev_arrow( ax, mean, sigma, rmse, linestyle = '-', color = 'tab:red' )
    ax = format_plot( ax, **kwargs )
    
    return ax
    
'''
Function to select elements of array 'n' standard deviations away
    Inputs:
        - array
        - mean of array
        - standard deviation of array
        - n: parameter to select points 'n' standard deviations away
'''
def choose_n_std_away( array, mean, std, n = 1 ):
    return np.sum((array > mean + n * std) + (array < mean - n * std))

'''
Function to histogram given array and return mid points for plotting a bar plot
    Inputs:
        - X: Array to be histogrammed
        - bins: Number of bins to be used (default value = 15)
        - density: Whether to get normalized histogram (density = True) or raw value histogram (density = False)
    Outputs:
        - hist: histogram of given array X
        - width: Width of bar plot ( 0.85 * bin_width )
        - mid_points: mid points of bar plot bars
'''
def histogram_and_get_mid_points( X, bins = 15, density = False ):
    
    hist, bin_edges = np.histogram( X,
                                   bins = bins,
                                   density = density
                                   )
    left_edges = bin_edges[:-1]
    width = 0.85*(left_edges[1] - left_edges[0])
    mid_points = left_edges + ( left_edges[1] - left_edges[0] )/2

    return hist, width, mid_points

'''
Main function
'''
if __name__ == "__main__":
    
#    start_time = time.time()
    pickle_file = 'pattern_order_parameters_amine_1500_v2.pickle'
    out_folder = 'hfe_histograms_normalized/'
    rmse_file = 'order_parameter_results/amine_1500/amine_1500_rmse.csv'
    
    '''
    Load data from parameters pickle and rmse file
    '''
    data = np.load( pickle_file )
    rmse = np.loadtxt( rmse_file, 
                      skiprows = 1,
                      delimiter = ","
                      )
    
    '''
    Save labels and number of polar groups
    '''
    labels = data[ 'labels' ]
    num_polar = data[ 'num_polar' ]
    
    
    one_std_away = []
    
    '''
    Iterate through number of polar groups and plot histograms of hfe labels
    '''
    for chosen_num_polar in np.unique( num_polar )[3:14]:
        ## LOAD DATA OF RELEVANT NUM_POLAR
        mask = num_polar == chosen_num_polar
        labels_chosen = labels[ mask ]
        
        ## CALCULATE MEAN AND STD DEV OF CHOSEN LABELS
        mean_hfe = np.mean( labels_chosen )
        std_hfe = np.std( labels_chosen )
        
        ## MAKE MASKS FOR ONE AND TWO STANDARD DEVIATIONS AWAY
        mask_one_std = choose_n_std_away( labels_chosen, mean_hfe, std_hfe, n = 1 )
        mask_two_std = choose_n_std_away( labels_chosen, mean_hfe, std_hfe, n = 2 )
        
        one_std_away.append( [chosen_num_polar, np.sum(mask), mask_one_std, mask_two_std ] )

        ## FILTER RELEVANT RMSE
        rmse_chosen = rmse[ rmse[:,0] == chosen_num_polar, 1 ][0] # 1 for lin reg, 2 for ridge
        
        ## MAKE HISTOGRAMS OF CHOSEN LABELS
        hist, width, mid_points = histogram_and_get_mid_points( labels_chosen,
                                                               bins = 15,
                                                               density = True
                                                               )
        
        ## DEFINE AND LOAD PARAMETERS FOR FORMATTING PLOTS
        x_label = 'HFE (k$_B$T)'
        y_label = 'Number of patterns'
        kwargs = load_default_params( x_label, y_label )
        
        ## MAKE BAR PLOT
        fig, ax = plt.subplots()
        ax = make_bar_plot( ax,
                           mid_points, 
#                           hist/hist.sum(), 
                            hist, 
                           width, 
                           mean_hfe,
                           std_hfe,
                           rmse_chosen,
                           add_normal = 'yes',
                           make_arrow = 'yes',
                           **kwargs 
                           )
        ax.set_title('Number of polar groups = {}'.format( chosen_num_polar ), fontsize = 18 )
        ## SAVE FIGURE
        plt.savefig( '{}/amine_1500_hfe_histogram_numPolar_{}.png'.format( out_folder, chosen_num_polar ),
                    bbox_inches = 'tight'
                    )
    
    one_std_away = np.array( one_std_away )
    
    end_time = time.time()
    
    print('Time required to run entire code is {:0.1f} seconds'.format( end_time - start_time ) )
    
    
    
    
    
    
    
    
    
    
    
    
    
