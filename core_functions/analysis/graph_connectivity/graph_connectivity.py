# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 04/08/2021

"""

import numpy as np
import sys
import matplotlib.pyplot as plt
plt.rcParams['figure.figsize'] = [6,6]
import math
import networkx as nx
sys.path.append('R:\\')
import analysis_scripts.python_scripts.plotting.plotting_functions as plotting_functions
from gridding_functions import make_hex_grid, make_padded_hex_grid, make_patterns_array

'''
Function to make graph of polar atoms (value of 1 in pattern array) using next-neighbor connectivity
    Inputs:
        - pattern_array with 0s (nonpolar) and 1s (polar)
    Output:
        - Adjacency matrix of polar groups of patterns
'''
def make_graph( pattern ):
    ## DEFINE GRID SPACING (r_dist) AS 1
    r_dist = 1
    eps = r_dist/10 # tolerance factor for distance between lattice points due to floating point errors, anything < r_dist will work
    
    ## FIND X AND Y POSITIONS USING A HEXAGONAL GRID
    x_pos, y_pos = make_hex_grid( int(pattern.shape[0]**(1/2)), 
                                 int(pattern.shape[0]**(1/2)), 
                                 r_dist
                                 )
    
    ## FILTER OUT POLAR GROUP POSITIONS ONLY
    x_pos_polar = x_pos[ pattern == 1 ]
    y_pos_polar = y_pos[ pattern == 1 ]

    ## STACK X AND Y POSITIONS TOGETHER
    pos_polar = np.stack( (x_pos_polar, y_pos_polar) )
    
    ## CALCULATE PAIRWISE DISTANCES BETWEEN ALL POINTS
    pairwise_distance = np.linalg.norm( pos_polar.T[:, None, :] - pos_polar.T[None, :, :], axis = -1 )
    
    ## MAKE ADJACENCY MATRIX USING X AND Y POSITIONS
    ## SET ALL NON-NEIGHBORS TO 0
    pairwise_distance[ ~((pairwise_distance > r_dist - eps) * (pairwise_distance < r_dist + eps)) ] = 0
    
    return pairwise_distance
    

'''
Finds node connectivity using networkx, gives quirky message and 0 if error in connectivity
'''
def find_connectivity( graph, type ):
    try:
        if type == 'node':
            return nx.node_connectivity( graph )
        elif type == 'edge':
            return nx.edge_connectivity( graph )
    except:
        print('Node connectivity and free will are a myth')
        return 0

'''
Function to find Pearson's R between given property array and pattern labels
'''
def find_pearsons_r_given_num_polar( prop, labels, num_polar_array, chosen_num_polar ):
    ## FIRST FILTER ON THE BASIS OF NUM_POLAR_ARRAY AND CHOSEN_NUM_POLAR
    prop = prop[ num_polar_array == chosen_num_polar ]
    labels = labels[ num_polar_array == chosen_num_polar ]
    
    ## CALCULATE CORRELATION COEFFICIENT
    corr = np.corrcoef( np.stack((prop, labels)) )
    
    return corr[0, 1]


'''
Function to find average clustering coefficient
'''
def find_clustering( graph ):
    try:
        return nx.average_clustering( graph )
    except:
        print('Average clustering and determinism are a myth')
        return 0


if __name__ == "__main__":
    
    testing = True

#    patterns_labels_file = '../active_learning_run2/pattern_labels/master_patterns_labels.pickle'
    patterns_labels_file = '../active_learning_amine/active_learning_patterns/gpr_prediction_iteration_1500.pickle'
    std_indus = 2
    
    if testing:
        chosen_num_polar = 7
    if not testing:
        chosen_num_polar = int(sys.argv[1])
            
    '''
    Load pattern labels file
    '''
    patterns_labels = np.load( patterns_labels_file )
    
    '''
    Make easily amenable array of patterns
    '''
    patterns_array = make_patterns_array( patterns_labels[:,0] )
    
    '''
    Make INDUS mask based on std_indus
    '''
    indus_mask = patterns_labels[ :, 2 ] == std_indus
    
    '''
    Concatenate arrays
    '''
    final_array = np.concatenate( (patterns_array[ indus_mask ],
                                   patterns_labels[ indus_mask, 1:]), 
                                axis = -1 )
        
    '''
    Make array with sum of polar groups
    '''
    num_polar_array = np.sum( patterns_array, 
                             axis = 1 
                             )
    
    '''
    Define blank adjacency matrices list
    '''
    adj_all = []
    avg_connectivity_all = []
    
    for pattern in patterns_array:
        adj = make_graph( pattern )
        adj_all.append( adj )
        if math.isnan(adj.mean() ):
            avg_connectivity_all.append( 0 )
        else:
            avg_connectivity_all.append( adj.mean() )

    adj_all = np.array( adj_all )
    avg_connectivity_all = np.array( avg_connectivity_all )
    
    '''
    Make collection of networkx graphs
    '''
    nx_graphs = []
    
    for adj in adj_all:
        nx_graphs.append( nx.convert_matrix.from_numpy_array( adj ) )

    ## CALCULATE CONNECTIVITY OF ALL GRAPHS
    node_connectivity = [ find_connectivity( graph, 'node' ) for graph in nx_graphs ]
    edge_connectivity = [ find_connectivity( graph, 'edge' ) for graph in nx_graphs ]

    ## CALCULATE CLUSTERING COEFFICIENT OF ALL GRAPHS
    avg_clustering = [ find_clustering( graph ) for graph in nx_graphs ]

    node_connectivity = np.array( node_connectivity ).astype( 'float' )
    edge_connectivity = np.array( edge_connectivity ).astype( 'float' )
    avg_clustering = np.array( avg_clustering ).astype('float')

    p_r_node_conn = []
    p_r_edge_conn = []
    p_r_avg_clust = []
    
    for chosen_num_polar in range(7, 11):
        pearsons_r_node_connectivity = find_pearsons_r_given_num_polar( node_connectivity[ indus_mask ],
                                                                       patterns_labels[ indus_mask, 1 ].astype('float'),
                                                                       num_polar_array[ indus_mask ],
                                                                       chosen_num_polar
                                                                       )
        
        pearsons_r_edge_connectivity = find_pearsons_r_given_num_polar( edge_connectivity[ indus_mask ],
                                                                       patterns_labels[ indus_mask, 1 ].astype('float'),
                                                                       num_polar_array[ indus_mask ],
                                                                       chosen_num_polar
                                                                       )
        
        pearsons_r_avg_clustering = find_pearsons_r_given_num_polar( avg_clustering[ indus_mask ],
                                                                       patterns_labels[ indus_mask, 1 ].astype('float'),
                                                                       num_polar_array[ indus_mask ],
                                                                       chosen_num_polar
                                                                       )
        
        ## APPEND TO LIST
        p_r_node_conn.append([ chosen_num_polar, 
                              np.sum( num_polar_array[ indus_mask ] == chosen_num_polar ),
                              np.round( pearsons_r_node_connectivity, 3 ) ])
    
        p_r_edge_conn.append([ chosen_num_polar, 
                              np.sum( num_polar_array[ indus_mask ] == chosen_num_polar ),
                              np.round( pearsons_r_edge_connectivity, 3 ) ])
    
        p_r_avg_clust.append([ chosen_num_polar, 
                              np.sum( num_polar_array[ indus_mask ] == chosen_num_polar ),
                              np.round( pearsons_r_avg_clustering, 3 ) ])
    
    p_r_node_conn = np.array( p_r_node_conn )
    p_r_edge_conn = np.array( p_r_edge_conn )
    p_r_avg_clust = np.array( p_r_avg_clust )


