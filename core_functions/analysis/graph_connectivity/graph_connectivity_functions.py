# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 04/08/2021

"""

import numpy as np
import sys
import matplotlib.pyplot as plt
plt.rcParams['figure.figsize'] = [6,6]
import math
import networkx as nx
sys.path.append('R:\\')
import plotting.plotting_functions as plotting_functions
from gpr.core_functions.gridding.gridding_functions import make_hex_grid, make_padded_hex_grid, make_patterns_array

'''
Function to make graph of polar atoms (value of 1 in pattern array) using next-neighbor connectivity
    Inputs:
        - pattern_array with 0s (nonpolar) and 1s (polar)
        - group_index: Value of 0 or 1 (0 for nonpolar graph, 1 for polar graph)
        - padding_type: 'normal' or 'padded' depending on how you want the hexagonal grid
    Output:
        - Adjacency matrix of polar groups of patterns
'''
def make_graph( pattern, group_index = 1, padding_type = 'normal'):
#    print('*** Group index chosen for making graphs is {} ***'.format( group_index ) )
    ## DEFINE GRID SPACING (r_dist) AS 1
    r_dist = 1
    eps = r_dist/10 # tolerance factor for distance between lattice points due to floating point errors, anything < r_dist will work
    
    ## FIND X AND Y POSITIONS USING A HEXAGONAL GRID
    if padding_type == 'normal':
        x_pos, y_pos = make_hex_grid( int(pattern.shape[0]**(1/2)), 
                                     int(pattern.shape[0]**(1/2)), 
                                     r_dist
                                     )
    elif padding_type == 'padded':
        x_pos, y_pos = make_padded_hex_grid( int(pattern.shape[0]**(1/2)), 
                                            int(pattern.shape[0]**(1/2)), 
                                            r_dist
                                            )
    else:
        print("*** Input either 'normal' or 'padded' as the input type ***")
    
    ## FILTER OUT POLAR GROUP POSITIONS ONLY
    x_pos_polar = x_pos[ pattern == group_index ]
    y_pos_polar = y_pos[ pattern == group_index ]

    ## STACK X AND Y POSITIONS TOGETHER
    pos_polar = np.stack( (x_pos_polar, y_pos_polar) )
    
    ## CALCULATE PAIRWISE DISTANCES BETWEEN ALL POINTS
    pairwise_distance = np.linalg.norm( pos_polar.T[:, None, :] - pos_polar.T[None, :, :], axis = -1 )
    
    ## MAKE ADJACENCY MATRIX USING X AND Y POSITIONS
    ## SET ALL NON-NEIGHBORS TO 0
    pairwise_distance[ ~((pairwise_distance > r_dist - eps) * (pairwise_distance < r_dist + eps)) ] = 0
    
    return pairwise_distance
    

'''
Finds node connectivity using networkx, gives quirky message and 0 if error in connectivity
'''
def find_connectivity( graph, type ):
    try:
        if type == 'node':
            return nx.node_connectivity( graph )
        elif type == 'edge':
            return nx.edge_connectivity( graph )
    except:
        print('Node connectivity and free will are a myth')
        return 0

'''
Function to find Pearson's R between given property array and pattern labels
'''
def find_pearsons_r_given_num_polar( prop, labels, num_polar_array, chosen_num_polar ):
    ## FIRST FILTER ON THE BASIS OF NUM_POLAR_ARRAY AND CHOSEN_NUM_POLAR
    prop = prop[ num_polar_array == chosen_num_polar ]
    labels = labels[ num_polar_array == chosen_num_polar ]
    
    ## CALCULATE CORRELATION COEFFICIENT
    corr = np.corrcoef( np.stack((prop, labels)) )
    
    return corr[0, 1]

'''
Function to find Pearson's R between given property array and pattern labels with n_mc Monte Carlo runs
    Inputs:
        - prop: x value in correlation coefficient calculation
        - labels: y value in correlation coefficient calculation (mean of Gaussian prediction)
        - std_dev: Standard deviation of 
        - num_polar_array: Array of number of polar groups
        - chosen_num_polar: Number of polar groups to be chosen 
        - n_mc: Number of Monte Carlo steps
'''
def find_pearsons_r_given_num_polar_mc( prop, labels, std_dev, num_polar_array, chosen_num_polar, n_mc ):
    ## FIRST FILTER ON THE BASIS OF NUM_POLAR_ARRAY AND CHOSEN_NUM_POLAR
    prop = prop[ num_polar_array == chosen_num_polar ]
    std_dev = std_dev[ num_polar_array == chosen_num_polar ]
    labels = labels[ num_polar_array == chosen_num_polar ]
    
    corr_all = []
    ## SAMPLE GAUSSIAN N_MC TIMES
    for i in range(n_mc):
        ## PICK SAMPLE FROM GAUSSIAN WITH MEAN 0 AND STD DEV 1
        gaussian = np.random.normal( 0, 1, prop.shape[0] )
        labels_sampled = labels + std_dev * gaussian
        ## CALCULATE CORRELATION COEFFICIENT
        corr = np.corrcoef( np.stack((prop, labels_sampled)) )
        corr_all.append( corr[0, 1] )
    
    return corr_all


'''
Function to find average clustering coefficient
'''
def find_clustering( graph ):
    try:
        return nx.average_clustering( graph )
    except:
        print('Average clustering and determinism are a myth')
        return 0

'''
Define blank adjacency matrices list
    Inputs:
        - patterns_array: Array of all flattened patterns
        - group_index: 0 for making graphs of nonpolar groups, 1 for making graphs of polar groups
        - padding_type: 'normal' or 'padded' for normal or padded patterns
    Outputs:
        - adj_all: Adjacency matrices of all graphs of given patterns
        - avg_connectivity_all: Average of connectivity (sum_adj/num_nodes) for each graph in adj_all
'''
def calculate_adjacency_avg_connectivity( patterns_array, group_index, padding_type ):
    adj_all = []
    avg_connectivity_all = []
    
    for pattern in patterns_array:
        adj = make_graph( pattern, group_index, padding_type )
        adj_all.append( adj )
        if math.isnan(adj.mean() ):
            avg_connectivity_all.append( 0 )
        else:
            avg_connectivity_all.append( adj.mean() )

    adj_all = np.array( adj_all )
    avg_connectivity_all = np.array( avg_connectivity_all )
    return adj_all, avg_connectivity_all

'''
Function to convert list of adjacency matrices to graphs
'''
def make_graphs_from_adjacency_list( adj_all ):
    nx_graphs = []
    
    for adj in adj_all:
        nx_graphs.append( nx.convert_matrix.from_numpy_array( adj ) )

    return nx_graphs

'''
Function to find node or edge connectivity from a list of graphs
    Inputs:
        - list_of_graphs: list of networkx graphs whose connectivity you wish to calculate
        - type: 'node' or 'edge' depending on which connectivity you wish to calculate
        
'''
def find_connectivity_of_list( list_of_graphs, type ):
    
    res = [ find_connectivity( graph, type ) for graph in list_of_graphs ]
    
    return np.array( res ).astype('float')

'''
Function to make numpy array from given array X
'''
def make_numpy_array( X ):
    temp_all = []
    for x in X:
        temp = np.insert( x[2], 0, x[1] )
        temp = np.insert( temp, 0, x[0] )
        temp_all.append( temp )
    return np.array( temp_all )

if __name__ == "__main__":
    
    testing = True

#    patterns_labels_file = '../active_learning_amine/pattern_labels/master_patterns_labels.pickle'
    patterns_labels_file = '../active_learning_run2/pattern_labels/master_patterns_labels.pickle'
    std_indus = 2
    
    if testing:
        chosen_num_polar = 7
    if not testing:
        chosen_num_polar = int(sys.argv[1])
            
    '''
    Load pattern labels file
    '''
    patterns_labels = np.load( patterns_labels_file )
    
    '''
    Make easily amenable array of patterns
    '''
    patterns_array = make_patterns_array( patterns_labels[:,0] )
    
    '''
    Make INDUS mask based on std_indus
    '''
    indus_mask = patterns_labels[ :, 2 ] == std_indus
    
    '''
    Concatenate arrays
    '''
    final_array = np.concatenate( (patterns_array[ indus_mask ],
                                   patterns_labels[ indus_mask, 1:]), 
                                axis = -1 )
        
    '''
    Make array with sum of polar groups
    '''
    num_polar_array = np.sum( patterns_array, 
                             axis = 1 
                             )
    
    '''
    Make adjacency and average connectivity from array of patterns
    '''
    adj_all, avg_connectivity_all = calculate_adjacency_avg_connectivity( patterns_array )
    
    '''
    Make collection of networkx graphs
    '''
    nx_graphs = make_graphs_from_adjacency_list( adj_all )

    ## CALCULATE CONNECTIVITY OF ALL GRAPHS
    node_connectivity = find_connectivity_of_list( nx_graphs, 'node' ) 
    edge_connectivity = find_connectivity_of_list( nx_graphs, 'edge' ) 

    ## CALCULATE CLUSTERING COEFFICIENT OF ALL GRAPHS
    avg_clustering = [ find_clustering( graph ) for graph in nx_graphs ]

    avg_clustering = np.array( avg_clustering ).astype('float')

    p_r_node_conn = []
    p_r_edge_conn = []
    p_r_avg_clust = []
    
    for chosen_num_polar in range(7, 11):
        pearsons_r_node_connectivity = find_pearsons_r_given_num_polar( node_connectivity[ indus_mask ],
                                                                       patterns_labels[ indus_mask, 1 ].astype('float'),
                                                                       num_polar_array[ indus_mask ],
                                                                       chosen_num_polar
                                                                       )
        
        pearsons_r_edge_connectivity = find_pearsons_r_given_num_polar( edge_connectivity[ indus_mask ],
                                                                       patterns_labels[ indus_mask, 1 ].astype('float'),
                                                                       num_polar_array[ indus_mask ],
                                                                       chosen_num_polar
                                                                       )
        
        pearsons_r_avg_clustering = find_pearsons_r_given_num_polar( avg_clustering[ indus_mask ],
                                                                       patterns_labels[ indus_mask, 1 ].astype('float'),
                                                                       num_polar_array[ indus_mask ],
                                                                       chosen_num_polar
                                                                       )
        
        ## APPEND TO LIST
        p_r_node_conn.append([ chosen_num_polar, 
                              np.sum( num_polar_array[ indus_mask ] == chosen_num_polar ),
                              np.round( pearsons_r_node_connectivity, 3 ) ])
    
        p_r_edge_conn.append([ chosen_num_polar, 
                              np.sum( num_polar_array[ indus_mask ] == chosen_num_polar ),
                              np.round( pearsons_r_edge_connectivity, 3 ) ])
    
        p_r_avg_clust.append([ chosen_num_polar, 
                              np.sum( num_polar_array[ indus_mask ] == chosen_num_polar ),
                              np.round( pearsons_r_avg_clustering, 3 ) ])
    
    p_r_node_conn = np.array( p_r_node_conn )
    p_r_edge_conn = np.array( p_r_edge_conn )
    p_r_avg_clust = np.array( p_r_avg_clust )


