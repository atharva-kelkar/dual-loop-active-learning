# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 04/11/2021
"""

import time
import numpy as np
import sys
sys.path.append('/home/kelkar2/analysis_scripts/python_scripts/gpr/')
from core_functions.choose_patterns_for_indus.choose_next_indus_pattern import make_pattern_array
from core_functions.analysis.gpr_model_analysis import Predictions
from core_functions.active_learning_functions.active_learning_functions import write_pattern_file
from core_functions.analysis.graph_connectivity.graph_connectivity_functions import calculate_adjacency_avg_connectivity, \
                                                                                    make_graphs_from_adjacency_list,\
                                                                                    find_connectivity_of_list,\
                                                                                    find_clustering,\
                                                                                    find_pearsons_r_given_num_polar,\
                                                                                    find_pearsons_r_given_num_polar_mc,\
                                                                                    make_numpy_array

if __name__ == "__main__":
    
    n_pred = 1500
    base_folder = '../../../active_learning_amide/'
    ## GPR prediction folder and file
    gpr_pred_folder = 'active_learning_patterns/'
    gpr_pred_file = 'gpr_prediction_iteration_{}.pickle'.format( n_pred )
    ## Master pattern folder and file
    master_hfe_folder = 'pattern_labels/'
    master_hfe_file = 'master_patterns_labels.pickle'
    group_index = 1
    padding_type = 'normal'
    n_mc = 100
    
    start_time = time.time()
    
    '''
    Instantiate an object of class Predictions
    '''
    predictions = Predictions( base_folder + gpr_pred_folder, 
                              gpr_pred_file 
                              )
    
    '''
    Load all GPR predictions and relevant mean and std, unscale mu and sigma gotten from patterns file
    '''
    predictions.load_predictions()
    predictions.load_mean_std( base_folder + master_hfe_folder, 
                              master_hfe_file, 
                              n_pred 
                              )
    predictions.unscale_mu_sigma()
    
    '''
    Load master HFE file
    '''
    predictions.load_master_hfe( base_folder + master_hfe_folder, 
                                master_hfe_file, 
                                n_pred
                                )
    
    '''
    Make INDUS and CNN mask, and then other mask based on INDUS and CNN masks
    '''
    predictions.split_indus_cnn( 2.0, 6.0 )
    indus_mask = predictions.make_mask( 'indus' )
    cnn_mask = predictions.make_mask( 'cnn' )
    not_indus_mask = ~( indus_mask )
    

    '''
    Make adjacency and average connectivity arrays from patterns_all
    '''
    adj_all, avg_connectivity_all = calculate_adjacency_avg_connectivity( predictions.patterns_all, 
                                                                         group_index = group_index,
                                                                         padding_type = padding_type
                                                                         )
    
    '''
    Make list of networkx graphs from adj_all
    '''
    nx_graphs = make_graphs_from_adjacency_list( adj_all )
    
    ## CALCULATE CONNECTIVITY OF ALL GRAPHS
    node_connectivity = find_connectivity_of_list( nx_graphs, 'node' ) 
    edge_connectivity = find_connectivity_of_list( nx_graphs, 'edge' ) 
    
    ## CALCULATE CLUSTERING COEFFICIENT OF ALL GRAPHS
    avg_clustering = [ find_clustering( graph ) for graph in nx_graphs ]
    avg_clustering = np.array( avg_clustering ).astype('float')
    
    ## DEFINE BLANK RESULTS ARRAYS
    p_r_node_conn_mc = []
    p_r_edge_conn_mc = []
    p_r_avg_clust_mc = []
    
    for chosen_num_polar in np.unique( predictions.num_polar_all ):
        pearsons_r_node_connectivity_mc = find_pearsons_r_given_num_polar_mc( node_connectivity,
                                                                       predictions.unscaled_mu[:,0],
                                                                       predictions.unscaled_sigma[:,0],
                                                                       predictions.num_polar_all,
                                                                       chosen_num_polar,
                                                                       n_mc
                                                                       )
        
        pearsons_r_edge_connectivity_mc = find_pearsons_r_given_num_polar_mc( edge_connectivity,
                                                                       predictions.unscaled_mu[:,0],
                                                                       predictions.unscaled_sigma[:,0],
                                                                       predictions.num_polar_all,
                                                                       chosen_num_polar,
                                                                       n_mc
                                                                       )
        
        pearsons_r_avg_clustering_mc = find_pearsons_r_given_num_polar_mc( avg_clustering,
                                                                       predictions.unscaled_mu[:,0],
                                                                       predictions.unscaled_sigma[:,0],
                                                                       predictions.num_polar_all,
                                                                       chosen_num_polar,
                                                                       n_mc
                                                                       )
        
        ## APPEND TO LIST
        p_r_node_conn_mc.append([ chosen_num_polar, 
                              np.sum( predictions.num_polar_all == chosen_num_polar ),
                              np.round( pearsons_r_node_connectivity_mc, 4 ) ])
    
        p_r_edge_conn_mc.append([ chosen_num_polar, 
                              np.sum( predictions.num_polar_all == chosen_num_polar ),
                              np.round( pearsons_r_edge_connectivity_mc, 4 ) ])
    
        p_r_avg_clust_mc.append([ chosen_num_polar, 
                              np.sum( predictions.num_polar_all == chosen_num_polar ),
                              np.round( pearsons_r_avg_clustering_mc, 4 ) ])
    
    ## FORMAT NUMBERS TO HAVE 3 DECIMAL PLACES
    np.set_printoptions(formatter={'float': lambda x: "{0:0.3f}".format(x)})

    p_r_node_conn_mc = np.array( p_r_node_conn_mc )
    p_r_edge_conn_mc = np.array( p_r_edge_conn_mc )
    p_r_avg_clust_mc = np.array( p_r_avg_clust_mc )
    
    ## SAVE IN FORMAT COMPATIBLE WITH EXCEL SAVING
    p_r_node_conn_mc_comp = make_numpy_array( p_r_node_conn_mc )
    p_r_edge_conn_mc_comp = make_numpy_array( p_r_edge_conn_mc )
    p_r_avg_clust_mc_comp = make_numpy_array( p_r_avg_clust_mc )
    
    ## SAVE PEARSONS R ARRAYS AS CSV FILES
    ligand = 'amide'
    type = 'all'
    np.savetxt('{}_{}_{}_GPR_preds_{}_{}_pearsonsR_node_conn_MC.csv'.format( ligand, type, group_index, n_pred, padding_type ),
               p_r_node_conn_mc_comp, delimiter = ",", fmt = "%s"
               )
    np.savetxt('{}_{}_{}_GPR_preds_{}_{}_pearsonsR_edge_conn_MC.csv'.format( ligand, type, group_index, n_pred, padding_type ),
               p_r_edge_conn_mc_comp, delimiter = ",", fmt = "%s"
               )
    np.savetxt('{}_{}_{}_GPR_preds_{}_{}_pearsonsR_avg_clust_MC.csv'.format( ligand, type, group_index, n_pred, padding_type ),
               p_r_avg_clust_mc_comp, delimiter = ",", fmt = "%s"
               )
    
    print('**** Total time taken for code to run is {:0.2f} seconds ****'.format( time.time() - start_time ) )
    
    
    
