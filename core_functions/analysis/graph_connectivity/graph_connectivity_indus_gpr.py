# -*- coding: utf-8 -*-
"""
@author: Atharva
@date: 04/11/2021
"""

import numpy as np
import sys
sys.path.append('/home/kelkar2/analysis_scrips/python_scripts/gpr/')
from choose_next_indus_pattern import make_pattern_array
from gpr_model_analysis import Predictions
from active_learning_functions import write_pattern_file
import networkx
from graph_connectivity_functions import calculate_adjacency_avg_connectivity, \
                                            make_graphs_from_adjacency_list,\
                                            find_connectivity_of_list,\
                                            find_clustering,\
                                            find_pearsons_r_given_num_polar


if __name__ == "__main__":
    
    n_pred = 1500
    base_folder = '../active_learning_amine/'
    ## GPR prediction folder and file
    gpr_pred_folder = 'active_learning_patterns/'
    gpr_pred_file = 'gpr_prediction_iteration_{}.pickle'.format( n_pred )
    ## Master pattern folder and file
    master_hfe_folder = 'pattern_labels/'
    master_hfe_file = 'master_patterns_labels.pickle'

    '''
    Instantiate an object of class Predictions
    '''
    predictions = Predictions( base_folder + gpr_pred_folder, 
                              gpr_pred_file 
                              )
    
    '''
    Load all GPR predictions and relevant mean and std, unscale mu and sigma gotten from patterns file
    '''
    predictions.load_predictions()
    predictions.load_mean_std( base_folder + master_hfe_folder, 
                              master_hfe_file, 
                              n_pred 
                              )
    predictions.unscale_mu_sigma()
    
    '''
    Load master HFE file
    '''
    predictions.load_master_hfe( base_folder + master_hfe_folder, 
                                master_hfe_file, 
                                n_pred
                                )
    
    '''
    Make INDUS and CNN mask, and then other mask based on INDUS and CNN masks
    '''
    predictions.split_indus_cnn( 2.0, 6.0 )
    indus_mask = predictions.make_mask( 'indus' )
    cnn_mask = predictions.make_mask( 'cnn' )
    not_indus_mask = ~( indus_mask )
    

    '''
    Make adjacency and average connectivity arrays from patterns_all
    '''
    adj_all, avg_connectivity_all = calculate_adjacency_avg_connectivity( predictions.patterns_all )
    
    '''
    Make list of networkx graphs from adj_all
    '''
    nx_graphs = make_graphs_from_adjacency_list( adj_all )
    
    ## CALCULATE CONNECTIVITY OF ALL GRAPHS
    node_connectivity = find_connectivity_of_list( nx_graphs, 'node' ) 
    edge_connectivity = find_connectivity_of_list( nx_graphs, 'edge' ) 
    
    ## CALCULATE CLUSTERING COEFFICIENT OF ALL GRAPHS
    avg_clustering = [ find_clustering( graph ) for graph in nx_graphs ]
    avg_clustering = np.array( avg_clustering ).astype('float')
    
    ## DEFINE BLANK RESULTS ARRAYS
    p_r_node_conn = []
    p_r_edge_conn = []
    p_r_avg_clust = []
    
    for chosen_num_polar in np.unique( predictions.num_polar_all ):
        pearsons_r_node_connectivity = find_pearsons_r_given_num_polar( node_connectivity[indus_mask],
                                                                       predictions.unscaled_mu[indus_mask,0],
                                                                       predictions.num_polar_all[indus_mask],
                                                                       chosen_num_polar
                                                                       )
        
        pearsons_r_edge_connectivity = find_pearsons_r_given_num_polar( edge_connectivity[indus_mask],
                                                                       predictions.unscaled_mu[indus_mask,0],
                                                                       predictions.num_polar_all[indus_mask],
                                                                       chosen_num_polar
                                                                       )
        
        pearsons_r_avg_clustering = find_pearsons_r_given_num_polar( avg_clustering[indus_mask],
                                                                       predictions.unscaled_mu[indus_mask,0],
                                                                       predictions.num_polar_all[indus_mask],
                                                                       chosen_num_polar
                                                                       )
        
        ## APPEND TO LIST
        p_r_node_conn.append([ chosen_num_polar, 
                              np.sum( predictions.num_polar_all[indus_mask] == chosen_num_polar ),
                              np.round( pearsons_r_node_connectivity, 4 ) ])
    
        p_r_edge_conn.append([ chosen_num_polar, 
                              np.sum( predictions.num_polar_all[indus_mask] == chosen_num_polar ),
                              np.round( pearsons_r_edge_connectivity, 4 ) ])
    
        p_r_avg_clust.append([ chosen_num_polar, 
                              np.sum( predictions.num_polar_all[indus_mask] == chosen_num_polar ),
                              np.round( pearsons_r_avg_clustering, 4 ) ])
    
    ## FORMAT NUMBERS TO HAVE 3 DECIMAL PLACES
    np.set_printoptions(formatter={'float': lambda x: "{0:0.3f}".format(x)})

    p_r_node_conn = np.array( p_r_node_conn )
    p_r_edge_conn = np.array( p_r_edge_conn )
    p_r_avg_clust = np.array( p_r_avg_clust )
    
    ## SAVE PEARSONS R ARRAYS AS CSV FILES
    ligand = 'amine'
    type = 'indus'
    np.savetxt('{}_{}_GPR_preds_{}_pearsonsR_node_conn.csv'.format( ligand, type, n_pred ),
               p_r_node_conn, delimiter = ","
               )
    np.savetxt('{}_{}_GPR_preds_{}_pearsonsR_edge_conn.csv'.format( ligand, type, n_pred ),
               p_r_edge_conn, delimiter = ","
               )
    np.savetxt('{}_{}_GPR_preds_{}_pearsonsR_avg_clust.csv'.format( ligand, type, n_pred ),
               p_r_avg_clust, delimiter = ","
               )
    
    
    
