# -*- coding: utf-8 -*-
"""
@author: Atharva
@date: 04/11/2021

"""

import numpy as np
## FORMAT NUMBERS TO HAVE 3 DECIMAL PLACES
np.set_printoptions(formatter={'float': lambda x: "{0:0.3f}".format(x)})
import sys
sys.path.append('/home/kelkar2/analysis_scripts/python_scripts/gpr/core_functions')
from choose_patterns_for_indus.choose_next_indus_pattern import make_pattern_array
from analysis.gpr_model_analysis import Predictions
from active_learning_functions.active_learning_functions import write_pattern_file
from analysis.graph_connectivity.graph_connectivity_functions import calculate_adjacency_avg_connectivity, \
                                                                        make_graphs_from_adjacency_list,\
                                                                        find_connectivity_of_list,\
                                                                        find_clustering,\
                                                                        find_pearsons_r_given_num_polar
import pickle


def calc_graph_props( patterns_all, padding_type, group_index ):
    adj_all, avg_connectivity_all = calculate_adjacency_avg_connectivity( patterns_all,
                                                                         padding_type = padding_type,
                                                                         group_index = group_index
                                                                         )
    
    '''
    Make list of networkx graphs from adj_all
    '''
    nx_graphs = make_graphs_from_adjacency_list( adj_all )
    
    ## CALCULATE CONNECTIVITY OF ALL GRAPHS
    node_connectivity = find_connectivity_of_list( nx_graphs, 'node' ) 
    edge_connectivity = find_connectivity_of_list( nx_graphs, 'edge' ) 
    
    ## CALCULATE CLUSTERING COEFFICIENT OF ALL GRAPHS
    avg_clustering = [ find_clustering( graph ) for graph in nx_graphs ]
    avg_clustering = np.array( avg_clustering ).astype('float')
    
    return node_connectivity, edge_connectivity, avg_clustering


if __name__ == "__main__":
    
    n_pred = 1500
    ligand = 'amide'
    base_folder = '../../../active_learning_{}/'.format( ligand )
    ## GPR prediction folder and file
    gpr_pred_folder = 'active_learning_patterns/'
    gpr_pred_file = 'gpr_prediction_iteration_{}.pickle'.format( n_pred )
    ## Master pattern folder and file
    master_hfe_folder = 'pattern_labels/'
    master_hfe_file = 'master_patterns_labels.pickle'
      
    
    '''
    Instantiate an object of class Predictions
    '''
    predictions = Predictions( base_folder + gpr_pred_folder, 
                              gpr_pred_file 
                              )
    
    '''
    Load all GPR predictions and relevant mean and std, unscale mu and sigma gotten from patterns file
    '''
    predictions.load_predictions()
    predictions.load_mean_std( base_folder + master_hfe_folder, 
                              master_hfe_file, 
                              n_pred 
                              )
    predictions.unscale_mu_sigma()
    
    '''
    Load master HFE file
    '''
    predictions.load_master_hfe( base_folder + master_hfe_folder, 
                                master_hfe_file, 
                                n_pred
                                )
    
    '''
    Make INDUS and CNN mask, and then other mask based on INDUS and CNN masks
    '''
    predictions.split_indus_cnn( 2.0, 6.0 )
    indus_mask = predictions.make_mask( 'indus' )
    cnn_mask = predictions.make_mask( 'cnn' )
    not_indus_mask = ~( indus_mask )
    

    '''
    Make adjacency and average connectivity arrays from patterns_all
    '''
    padding_type = 'normal'
    group_index = 0
    
    node_connectivity_normal_0, \
    edge_connectivity_normal_0, \
    avg_clustering_normal_0 = calc_graph_props( predictions.patterns_all,
                                                padding_type = padding_type,
                                                group_index = group_index
                                                )
    
    padding_type = 'padded'
    group_index = 0
    
    node_connectivity_padded_0, \
    edge_connectivity_padded_0, \
    avg_clustering_padded_0 = calc_graph_props( predictions.patterns_all,
                                                padding_type = padding_type,
                                                group_index = group_index
                                                )

    padding_type = 'normal'
    group_index = 1
    
    node_connectivity_normal_1, \
    edge_connectivity_normal_1, \
    avg_clustering_normal_1 = calc_graph_props( predictions.patterns_all,
                                                padding_type = padding_type,
                                                group_index = group_index
                                                )
        
    '''
    Calculate Geary's C and Moran's I
    '''
    ## CALCULATE PADDED MORAN'S I
    predictions.calc_moransI_gearyC( 'padded', 'morans I')
    padded_morans_i = predictions.autocorrelation
    ## CALCULATE PADDED ADJUSTED GEARY'S C
    predictions.calc_moransI_gearyC( 'padded', 'adjusted geary C')
    padded_adj_geary_c = predictions.autocorrelation
    ## CALCULATE MORAN'S I
    predictions.calc_moransI_gearyC( 'normal', 'morans I')
    morans_i = predictions.autocorrelation
    ## CALCULATE ADJUSTED GEARY'S C
    predictions.calc_moransI_gearyC( 'normal', 'adjusted geary C')
    adj_geary_c = predictions.autocorrelation

    
    ## SAVE ALL ORDER PARAMETER MATRICES AS PICKLE FILES
    dict = {'node_conn_normal_0'    : node_connectivity_normal_0,
            'edge_conn_normal_0'    : edge_connectivity_normal_0,
            'avg_clust_normal_0'    : avg_clustering_normal_0,
            
            'node_conn_normal_1'    : node_connectivity_normal_1,
            'edge_conn_normal_1'    : edge_connectivity_normal_1,
            'avg_clust_normal_1'    : avg_clustering_normal_1,
            
            'node_conn_padded_0'    : node_connectivity_padded_0,
            'edge_conn_padded_0'    : edge_connectivity_padded_0,
            'avg_clust_padded_0'    : avg_clustering_padded_0,
            
            'padded adj geary C'    : padded_adj_geary_c,
            'padded morans I'       : padded_morans_i,
            'adj geary C'           : adj_geary_c,
            'morans I'              : morans_i,
            'num_polar'             : predictions.num_polar_all,
            'patterns_all'          : predictions.patterns_all,
            'labels'                : predictions.unscaled_mu[ :, 0 ],
            'std dev'               : predictions.unscaled_sigma[ :, 0 ]
            }
    
    ## SAVE DICT IN PICKLE
    pickle.dump( dict, 
                open('pattern_order_parameters_{}_{}_v2.pickle'.format( ligand, n_pred ), 'wb') 
                )
    
    
    
    
    
    
    
    
    
    
    
    
    
    
