# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 04/16/2021

"""

import numpy as np
from sklearn.linear_model import Lasso


'''
Function to load all parameters from dictionary based on parameter names
'''
def load_parameters( data, parameter_names ):
    count = 0
    for parameter in parameter_names:
        if count == 0:
            arr = data[ parameter ].squeeze()[:,None]
            count += 1
        else:
#            print(arr.shape)
#            print(data[parameter].shape)
            arr = np.concatenate((arr, data[ parameter ].squeeze()[:,None]), axis = -1)
            
    return arr

'''
Function to normalize given array X as (X-mu)/sigma
'''
def normalize( y ):
    if len(y.shape) == 1:
        return (y - np.mean(y))/np.std(y)
    elif len(y.shape) == 2:
        return ( y - np.mean(y, axis = 0))/np.std( y, axis = 0 )
    else:
        print('** Do not know how to deal with this shape - modify normalize function **')
        
'''
Function to fit a least squares model to the given data
'''
def least_squares_fit( X, y ):
    a = np.dot(X.T, X)
    a = np.linalg.inv( a )
    a = np.dot( a, X.T )
    return np.dot( a, y )

'''
Function to fit a ridge regression model to the given data
'''
def least_squares_fit_ridge( X, y, lbd ):
    a = np.dot(X.T, X)
    a = np.linalg.inv( a + lbd * np.eye( a.shape[0] ) )
    a = np.dot( a, X.T )
    return np.dot( a, y )

'''
Function to find RMSE of y - X*beta
'''
def find_rmse( X, beta, y ):
    y_pred = np.dot(X, beta[:,None])
#    print( y_pred[:5], y[:5] )
    y_diff = y_pred - y
    y_diff_sq = ( y_diff )**2
    y_diff_sq_sum = np.sum( y_diff_sq )
    
    return np.sqrt( y_diff_sq_sum / y.shape[0] )

'''
Function to save data in easily analyzable format
'''
def save_in_good_format( y_title, x_title, weights ):
    
    temp_1 = np.concatenate( (x_title[None,:], weights), axis = 0)
    temp_2 = np.concatenate( (y_title[:,None], temp_1), axis = -1)
    
    return temp_2
    
'''
'''
def multiply_regress( params, beta ):
    return np.sum(params * np.tile(beta.squeeze(), 
                                   (params.shape[0],1)
                                   ),
                  axis = 1
                  )

'''
Function to make predictions 
'''
def make_predictions_all( num_polar_all, parameters, range_to_check, reg_beta ):
    
    predictions = np.zeros( parameters.shape[0] )
    for num_polar in range_to_check:
        mask = num_polar_all == num_polar
        parameters_chosen = normalize( parameters[ mask, : ] )
        reg_beta_chosen = reg_beta[ 1:-1, reg_beta[0,:].astype('float') == num_polar ].astype('float')
        predictions[ mask ] = multiply_regress( parameters_chosen, reg_beta_chosen )
    
    return predictions

'''
Main function
'''
if __name__ == "__main__":
    
    '''
    Define file names and parameters
    '''
    pickle_file = 'pattern_order_parameters_amine_1500_v2.pickle'
    parameter_names = ['node_conn_normal_0',
                       'node_conn_normal_1',
                       'node_conn_padded_0',
#                       'edge_conn', 
                       'avg_clust_normal_0',
                       'avg_clust_normal_1',
                       'avg_clust_padded_0',
                       'padded adj geary C', 
                       'padded morans I',
                       'adj geary C', 
                       'morans I',
                       
                       ]
    
    '''
    Compile all parameters from pickle file
    '''
    data = np.load( pickle_file )
    
    parameters = load_parameters( data, parameter_names )

    '''
    Define labels array with standard deviation
    '''
    labels = data[ 'labels' ]
    std_dev = data[ 'std dev' ]

    '''
    Perform regression with parameters array as X and labels as Y for different number of polar groups
    '''
    beta_all = []
    beta_all_ridge = []
    beta_all_lasso = []
    rmse_all = []
    rmse_all_ridge = []
    rmse_all_lasso = []
    
    range_to_check = np.unique( data['num_polar'] )[3:14]
    range_to_check = np.unique( 7 ) # Only one number of polar groups to check right now
    
    for num_polar in range_to_check:
        mask = data['num_polar'] == num_polar
        labels_chosen = labels[ mask ][:, None]
        std_dev_mask = std_dev[ mask ]
        parameters_chosen = parameters[ mask, : ]
        
        parameters_chosen_norm = normalize( parameters_chosen )
#        print( labels_chosen.shape )
        labels_chosen_norm = normalize( labels_chosen )
#        print( labels_chosen )
        
        ## ADD BIAS TERM TO PARAMETERS_CHOSEN
        parameters_chosen_norm = np.concatenate( (parameters_chosen_norm, np.ones((parameters_chosen.shape[0],1))) , 
                                                axis = -1 
                                                )
#        labels_chosen = labels[ mask ][:, None]
        
        beta_curr = least_squares_fit( parameters_chosen_norm, 
                                           labels_chosen_norm
                                           )
        rmse_curr = find_rmse( parameters_chosen_norm, 
                              beta_curr.squeeze(),
                              labels_chosen_norm
                              )
        
        beta_all.append( beta_curr )
        rmse_all.append( rmse_curr )
        
        beta_curr_ridge = least_squares_fit_ridge( parameters_chosen_norm, 
                                                       labels_chosen_norm, 
                                                       lbd = 1
                                                       )
        
        rmse_curr_ridge = find_rmse( parameters_chosen_norm, 
                                    beta_curr_ridge.squeeze(),
                                    labels_chosen_norm
                                    )
        beta_all_ridge.append( beta_curr_ridge )
        rmse_all_ridge.append( rmse_curr_ridge )
        
        lasso = Lasso( alpha = 0.05, 
                      max_iter = 10000
#                      normalize = True 
                      )
        lasso.fit( parameters_chosen_norm, labels_chosen_norm )
        
        beta_curr_lasso = lasso.coef_
        rmse_curr_lasso = find_rmse( parameters_chosen_norm, 
                                    beta_curr_lasso.squeeze(),
                                    labels_chosen_norm
                                    )
        
        beta_all_lasso.append( beta_curr_lasso )
        rmse_all_lasso.append( rmse_curr_lasso )
    
    ## FORMAT NUMBERS TO HAVE 3 DECIMAL PLACES
    np.set_printoptions(formatter={'float': lambda x: "{0:0.3f}".format(x)})
    
    ## CONVERT TO NUMPY ARRAY
    beta_all = np.array( beta_all ).squeeze()
    beta_all_ridge = np.array( beta_all_ridge ).squeeze()
    beta_all_lasso = np.array( beta_all_lasso ).squeeze()

    rmse_all = np.array( rmse_all ).squeeze()
    rmse_all_ridge = np.array( rmse_all_ridge ).squeeze()
    rmse_all_lasso = np.array( rmse_all_lasso ).squeeze()


    ## SAVE ARRAYS IN PROPER FORMAT
    parameter_names = np.array( parameter_names )
    parameter_names = np.append( parameter_names, 'bias')
    parameter_names = np.insert( parameter_names, 0, 'Features')
    
    ## SAVE DATA 
    data_all = save_in_good_format( parameter_names,
                                   range_to_check,
                                   beta_all.T
                                   )
    
    data_all_ridge = save_in_good_format( parameter_names,
                                         range_to_check,
                                         beta_all_ridge.T
                                         )
 
    data_all_lasso = save_in_good_format( parameter_names,
                                   range_to_check,
                                   beta_all_lasso.T
                                   )
    
#    np.savetxt('amine_1500_weights_all.csv',
#               data_all,
#               delimiter = ",",
#               fmt = "%s"
#               )
    
#    np.savetxt('amine_1500_weights_all_ridge.csv',
#               data_all_ridge,
#               delimiter = ",",
#               fmt = "%s"
#               )

    np.savetxt('amine_1500_weights_all_lasso.csv',
               data_all_lasso,
               delimiter = ",",
               fmt = "%s"
               )

    
    
    predictions_all = make_predictions_all( data['num_polar'],
                                           parameters,
                                           range_to_check,
                                           data_all[:, 1:]
                                           )
    
    predictions_ridge = make_predictions_all( data['num_polar'],
                                             parameters,
                                             range_to_check,
                                             data_all_ridge[:, 1:]
                                             )
    
    predictions_lasso = make_predictions_all( data['num_polar'],
                                             parameters,
                                             range_to_check,
                                             data_all_lasso[:, 1:]
                                             )
    
    res = np.concatenate((data['num_polar'][:,None], 
                          parameters, 
                          data['labels'][:,None],
                          predictions_all[:,None],
                          predictions_ridge[:,None],
                          predictions_lasso[:,None]
                          ), 
                        axis = 1)



