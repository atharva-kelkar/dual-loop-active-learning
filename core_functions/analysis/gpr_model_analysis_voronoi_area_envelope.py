# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 01/25/2021

"""

import numpy as np, pandas as pd, sys, time, socket

## ADD R:\\ TO PATH IF SERVER IS NOT SWARM
if 'swarm' not in socket.gethostname():
    sys.path.append(r'R:\\')
    import matplotlib.pyplot as plt

## IMPORT MATPLOTLIB CORRECTLY IF USING SWARM
if 'swarm' in socket.gethostname():
    import matplotlib
    matplotlib.use('agg')
    import matplotlib.pyplot as plt
    ## ADD CORE_FUNCTIONS TO PYTHON PATH
    sys.path.append('/home/kelkar2/analysis_scripts/python_scripts/gpr/core_functions/')
    
    ## IMPORT SUPPORT FUNCTIONS
from analysis_scripts.python_scripts.gpr.active_learning.make_hypercube import gen_all_patterns

from analysis_scripts.python_scripts.gpr.core_functions.analysis.pattern_analysis import set_nan_0, plot_by_num_polar_geary_C, plot_linear_interpolation

import analysis.plotting_functions as plot


## IMPORT PREDICTIONS CLASS FROM GPR_MODEL_ANALYSIS.PY
from analysis.gpr_model_analysis import Predictions
        
## IMPORT HISTOGRAM_AREAFRAC FOR DEFINE_BINS FUNCTION
from histogram_gpr_preds_per_area_frac import Histogram_AreaFrac

## IMPORT Choose_MaxDev_AreaFrac FOR ALLOCATE_BINS FUNCTION
from choose_patterns_for_indus.choose_max_dev_gpr_preds_area_frac import Choose_MaxDev_AreaFrac


'''
class that inherits properties from Predictions and makes Voronoi areas

    Changelog:
        - 06/03/2021: 
            - Changed class name from Predictions to Predictions_wVoronoi
            - Added inheritance from Predictions class
'''
class Predictions_wVoronoi( Predictions ):
    
    def __init__( self, 
                 folder_name, 
                 file_name, 
                 voronoi_areas_pickle = '../gridding/average_voronoi_areas.pkl'
                 ):
        
        ## INITIALIZE PARENT CLASS
        Predictions.__init__( self, 
                             folder_name, 
                             file_name 
                             )
        
        ## ALLOCATE NEW VARIABLES TO CHILD CLASS
        self.voronoi_areas_pickle = voronoi_areas_pickle
        
        
    '''
    Function to load predictions given folder name and file name
    '''
    def make_voronoi( self ):
        self.voronoi_areas          =   np.load( self.voronoi_areas_pickle, allow_pickle = True )[:, 2] # COL 2 HAS ALL AREAS, 0 AND 1 ARE INDICES
        self.polar_area_all         =   np.sum( self.patterns_all * self.voronoi_areas, axis = 1 )
        self.polar_area_frac_all    =   self.polar_area_all/4 # 4 BECAUSE OF 2.0x2.0 GRID
    
    '''
    Function to make polar area frac for master patterns
    '''
    def make_master_voronoi( self ):
        self.master_indus_polar_area_frac = np.sum( self.master_patterns_indus * self.voronoi_areas, axis = 1 )/4
        self.master_cnn_polar_area_frac = np.sum( self.master_patterns_cnn * self.voronoi_areas, axis = 1 )/4

    '''
    Function to make data_array with area fraction, a writeable array that can be used to read GPR predictions in an Excel file
        Inputs:
            - indus_mask: 1 if INDUS calculated, 0 if not
            - cnn_mask: 1 if CNN calculated, 0 if not
    '''
    def make_data_array_with_area_frac( self, indus_mask, cnn_mask ):
    
        self.data_array = np.concatenate((self.num_polar_all[:,None],
                                          self.polar_area_frac_all[:,None],
                                          self.unscaled_mu,
                                          self.unscaled_sigma,
                                          indus_mask[:,None],
                                          cnn_mask[:,None]),
                                        axis = -1
                                        )
    
    '''
    Static method to calculate top and bottom edge of envelope
    '''
    @staticmethod
    def calc_envelope( bins, values ):
        ## RECORD BIN NUMBER, MIN, MAX VALUES FOR ENVELOPE EDGES
        edges = np.array( [ (x, np.min( values[ bins == x ] ), np.max( values[ bins == x ] )) for x in np.unique( bins ) ] )
        
        ## UNNECESSARY BLOCK - KEPT FOR THE TIME BEING
        ## APPEND ARRAY WITH FIRST LAST VALUE OF ENVELOPE
#        edges[0,2] = values[0]
#        ap = np.array( [ edges[-1,0] + 1, values[-1][0], values[-1][0] ] )
#        
#        edges = np.concatenate( (edges, ap[ None, : ] ), 
#                              axis = 0 
#                              )
        
        return edges
    
    '''
    Static method to append mid points array to edges for ease of plotting
    '''
    @staticmethod
    def append_mid_pts( edges, mid_pts, min_val, max_val ):
        
        for i in range(len(edges)):
            if edges[i,0] == -1: # -1 IS FOR LEFT END
                edges[i,0] = min_val
                
            elif edges[i,0] == 1000: # 1000 IS FOR RIGHT END
                edges[i,0] = max_val
            
            else:
                edges[i,0] = mid_pts[ int(edges[i,0]) ]
            
        return edges
    
    '''
    Function to compute envelope
    '''
    def compute_envelope( self, 
                         master_folder_name, 
                         master_file_name, 
                         n_current, 
                         bin_width = 0.04
                         ):
        
        ## LOAD ALL PREDICTIONS, MEAN, AND STD
        self.load_predictions()
        self.load_mean_std( master_folder_name, master_file_name, n_current )
        
        ## MAKE VORONOI AREAS
        self.make_voronoi()
        
        ## CALCULATE UNSCALED MU AND SIGMA VALUES
        self.unscale_mu_sigma()
        
        ## LOAD MASTER FILE HFEs AND PATTERNS
        self.load_master_hfe( master_folder_name, master_file_name, n_current )
        
        ## SPLIT MASTER FILE HFEs AND PATTERNS INTO INDUS AND CNN VALUES
        self.split_indus_cnn( 2.0, 6.0 ) # 2.0 --> INDUS sigma, 6.0 --> CNN sigma
        
        ## MAKE MASK FOR INDUS AND CNN PATTERNS AMONG ALL PATTERNS
        self.indus_mask = self.make_mask( 'indus' )
        self.cnn_mask = self.make_mask( 'cnn' )

        ## COMPUTE BINS WITH BIN_WIDTH
        self.bins = Histogram_AreaFrac.define_bins( self.polar_area_frac_all,
                                                   bin_width 
                                                   )
        
        ## CALCULATE MID POINTS OF BINS
        self.histogram, _ , self.mid_pts = Histogram_AreaFrac.gen_histogram( self.polar_area_frac_all,
                                                                            self.bins
                                                                            )
        
        ## ALLOCATE ARRAY TO BINS
        self.area_frac_bins = Choose_MaxDev_AreaFrac.allocate_bins( self.polar_area_frac_all,
                                                                   self.bins
                                                                   )
        
        ## MANUALLY CHANGE TWO VALUES OF AREA FRAC BINS
        self.area_frac_bins[0] = -1
        self.area_frac_bins[-1] = 1000
        
        ## RECORD MIN AND MAX OF EVERY BIN AS LOWER AND UPPER EDGE OF ENVELOPE
        self.envelope = self.calc_envelope( self.area_frac_bins,
                                           self.unscaled_mu
                                           )
        
        ## APPEND MID POINTS TO ENVELOPE
        self.envelope = self.append_mid_pts( self.envelope, 
                                            self.mid_pts,
                                            self.polar_area_frac_all.min(),
                                            self.polar_area_frac_all.max()
                                            )
        


if __name__ == "__main__":
    
    
    ## DEFINE FILE AND FOLDER NAMES
    n_current = int(sys.argv[1])
    ligand = sys.argv[2]
    base_dir = '../../active_learning_{}/'.format( ligand )
    folder_name = base_dir + 'active_learning_patterns/'
    file_name = 'gpr_prediction_iteration_{:d}.pickle'.format( n_current )
    
    ## DEFINE MASTER FOLDER AND MASTER FILE NAME
    master_folder_name = base_dir +  'pattern_labels/'
    master_file_name = 'master_patterns_labels.pickle'
    
    ## CREATE OBJECTS OF PREDICTION CLASS
    predictions = Predictions_wVoronoi( folder_name , file_name )
    
    ## COMPUTE ENVELOPE
    predictions.compute_envelope( master_folder_name, 
                                 master_file_name, 
                                 n_current, 
                                 bin_width = 0.04
                                 )
    

    
    ## SAVE DATA_ARRAY IN CSV FILE
    save_data_array = False
    if save_data_array:
        np.savetxt('{}_1500_gpr_pred_area_frac.csv'.format( ligand ), predictions.data_array, 
                   delimiter = ","
                   )
    
    '''
    Plot GPR model HFE predictions as a function of number of polar groups
    Differentiate surfaces with and without INDUS and CNN predictions
    '''
    to_plot_mu = True
    plt.rcParams['figure.figsize'] = (5,5)

    if to_plot_mu:
        
        fig, ax = plot.plot_mu_vs_mf_by_type_envelope( predictions.polar_area_frac_all,
                                                      predictions.unscaled_mu, 
                                                      predictions.envelope,
                                                      predictions.indus_mask, 
                                                      predictions.cnn_mask, 
                                                      'Polar area fraction',
                                                      to_plot_cnn = False
                                                      )
        
        ax.set_xticks([0,0.2,0.4,0.6,0.8])
        
        save_mu = False
        
        if save_mu:
            plot.plt.savefig('figures/20210614_gpr_model_predictions_envelope_3_{}_{:d}.svg'.format( ligand, n_current ), 
                             bbox_inches = 'tight',
                             format = 'svg'
                             )

    '''
    Section to plot positive and negative deviation points
    '''
    to_plot_dev = True
    
    if to_plot_dev:
        ax = plot.plot_deviation( ax, 
                                 ligand,
                                 predictions.polar_area_frac_all
                                 )
        
        save_fig = True
        
        if save_fig:
            fig_name = 'figures/20210716_envelope_with_dev_{}_{:d}.svg'.format( ligand, n_current )
            plot.plt.savefig( fig_name, 
                             bbox_inches = 'tight',
                             format = fig_name.split('.')[-1]
                             )
    
