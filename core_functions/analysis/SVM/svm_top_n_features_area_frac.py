# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 05/16/2021

"""

import numpy as np, sys
from svm_extremes_normalized import nFoldCV_SVM, choose_using_mask, generate_std_mask, make_binary_labels
from regression.regression_order_parameters_normalized import load_parameters, normalize
from analyze_svm_results import make_coefficients_array
from scipy.stats import rankdata

'''
class nFoldCV_TopFeatures inherits properties and functions from nFoldCV_SVM

    Utility:
        - nFoldCV_TopFeatures uses the top 'n' features calculated from nFoldCV_SVM to build new SVM classifier

'''
class nFoldCV_TopFeatures( nFoldCV_SVM ):
    
    '''
    __init__ constructor
    '''
    def __init__(self,
                 parameters,
                 labels,
                 n_folds = 5,
                 penalty = 'l1',
                 C = 0.005,
                 dual = False,
                 max_iter = 10000,
                 n_features = 2
                 ):
        ## INITIALIZE PARENT CLASS
        nFoldCV_SVM.__init__( self, parameters, labels, n_folds, penalty, C, dual, max_iter )
        
        ## ALLOCATE PARAMETERS WHICH HAVE NOT ALREADY BEEN ALLOCATED TO SELF
        self.n_features = n_features
        
    '''
    '''
    @staticmethod
    def select_top_n_features( coeffs, n_features ):
        ## RANK ABSOLUTE VALUE OF COEFFICIENTS
        ranks = rankdata( np.abs(coeffs) )
        ## ADJUST RANKS TO BE 1 --> HIGHEST
        ranks = np.shape(ranks)[0] - ranks + 1
        ## RETURN DATA WITH HIGHEST RANKS
        return ranks <= n_features
        
    '''
    Compute function to run all calculations
    '''
    def compute_top_features( self ):
        ## RUN RESULTS FROM PARENT CLASS (NFOLD SVM WITH NO TOP FEATURE SELECTION)
        results_svm = self.compute()
        
        ## CONVERT COEFFICIENT ARRAY TO MALLEABLE FORMAT
        coeffs = make_coefficients_array( np.array( results_svm )[:, 0] )
        
        ## CALCULATE MEAN OF COEFFICIENTS ACROSS ALL FOLDS
        coeffs = coeffs.mean( axis = 0 )
        
        ## SELECT TOP 'N' FEATURES FROM COEFFS ARRAY
        top_n_mask = self.select_top_n_features( coeffs, self.n_features )
        
        ## FILTER PARAMETERS USING MASK
        self.parameters_new = self.parameters[ :, top_n_mask ]
        
        print('** Top features selected! Running new LASSO fit! **')
        ## MAKE NEW NFOLDCV_SVM OBJECT WITH PARAMETERS_NEW AS THE INPUT SET OF PARAMETERS
        n_fold_cv_new = nFoldCV_SVM( self.parameters_new,
                                    self.labels
                                    )
        
        ## COMPUTE AND RETURN RESULTS FROM NEW OBJECT    
        return n_fold_cv_new.compute()
    
    '''
    Compute function to run calculations with given features
    '''
    def compute_using_given_features( self, feature_mask ):

        ## FILTER PARAMETERS USING MASK
        self.parameters_new = self.parameters[ :, feature_mask ]
        
        print('** Given features selected! Running new LASSO fit! **')
        ## MAKE NEW NFOLDCV_SVM OBJECT WITH PARAMETERS_NEW AS THE INPUT SET OF PARAMETERS
        n_fold_cv_new = nFoldCV_SVM( self.parameters_new,
                                    self.labels,
                                    C = 1 # Large value of C so that weight penalty is neglected
                                    )
        
        ## COMPUTE AND RETURN RESULTS FROM NEW OBJECT    
        return n_fold_cv_new.compute()
    
'''
Function to choose given features from list of features
'''
def make_feature_mask( all_features, features_to_choose ):
    ## MAKE ARRAY OF ALL FALSE ENTRIES 
    chosen_features = all_features == 'badgers'
    
    ## ITERATE THROUGH ALL FEATURES AND MAKE ARRAY OF CHOSEN FEATURES
    for feature in features_to_choose:
        chosen_features = np.any( (chosen_features, ( all_features == feature )), axis = 0 )

    return chosen_features


'''
Function to make results readable
'''
def make_results_readable( res_list ):
    readable_list = []
    for res in res_list:
        mean_coeff = res[:,0].mean()[0][0]
        std_coeff = res[:,0].std()[0][0]
        train_performance = res[:,1].mean()
        val_performance = res[:,2].mean()
        train_pts = res[:,3][0]
        val_pts = res[:,4][0]
        
        readable_list.append( [ mean_coeff, std_coeff, train_performance, val_performance, train_pts, val_pts ] )
        
    return np.array( readable_list )
    
if __name__ == "__main__":
    
    '''
    Define file names and parameters
    '''
    base_folder = '/home/kelkar2/analysis_scripts/python_scripts/gpr/core_functions/order_parameter_results/'
    ligand = sys.argv[1]
    pickle_file = '{}_1500/pattern_order_parameters_{}_1500_area_frac.pickle'.format( ligand, ligand )
    out_file = 'results/20210616_{}_1500_SVM_results_area_frac.csv'.format( ligand )
    ## IMPORT LIST OF PARAMETER NAMES FROM GLOBAL_VARS
    from global_vars.global_vars import PARAMETER_NAMES
    
    ## DEFINE HOW TO CHOOSE TOP FEATURES
    feature_selection_choice = 'given features' # 'top from SVM' or 'given features'
    features_to_choose = [ 'adj geary C' ] # 'avg_clust_normal_0'
    
    '''
    Load all parameters from pickle file
    '''
    data = np.load( base_folder + pickle_file )
    
    parameters = load_parameters( data, PARAMETER_NAMES )

    '''
    Define labels array with standard deviation
    '''
    labels = data[ 'labels' ][:, None]
    std_dev = data[ 'std dev' ]

    '''
    Perform regression with parameters array as X and labels as Y for different number of polar groups
    '''
    
#    range_to_check = np.unique( data['num_polar'] )[5:11]
    range_to_check = np.unique( data['area frac bins'] )[5:14]
#    range_to_check = np.unique( 7 ) # Only one number of polar groups to check right now
    
    results_list = []
    
    for bin_number in range_to_check:
        ## GENERATE MASK WITH NUMBER OF POLAR GROUPS
        mask = data['area frac bins'] == bin_number
        
        # CHOOSE LABELS AND PARAMETERS USING MASK
        parameters_chosen, labels_chosen = choose_using_mask( parameters, 
                                                              labels, 
                                                              mask 
                                                              )
        
        ## NORMALIZE PARAMETERS AND LABELS CHOSEN
        parameters_chosen_norm, labels_chosen_norm = normalize( parameters_chosen ), normalize( labels_chosen )
        
        ## CREATE MASK FOR VALUES N STD AWAY (ON NORMALIZED DATA)
        std_mask = generate_std_mask( labels_chosen_norm, 
                                     n = 2
                                     )
        
        ## CHOOSE LABELS AND PARAMETERS USING STD_MASK
        parameters_chosen_norm, labels_chosen_norm = choose_using_mask( parameters_chosen_norm, 
                                                                       labels_chosen_norm, 
                                                                       std_mask 
                                                                       )
        
        ## ALLOCATE LABELS VALUES OF -1 AND 1
        labels_classify = make_binary_labels( labels_chosen_norm, [-1,1] )
        
        
        ## MAKE N-FOLD CV OBJECT WITH CHOSEN LABELS AND PARAMETERS
        n_fold_svm = nFoldCV_TopFeatures( parameters_chosen_norm,
                                         labels_classify.squeeze(),
                                         n_features = 2
                                         )
        
        print( '*** Computing classifier with top features for bin_number = {} ***'.format( bin_number ) )
        
        ## IF CHOOSING TOP FEATURES FROM SVM, RUN COMPUTE_TOP_FEATURES
        if feature_selection_choice == 'top from SVM':
            results_list.append( n_fold_svm.compute_top_features() )
        
        ## IF CHOOSING PRE-SELECTED FEATURES, RUN COMPUTE_USING_GIVEN_FEATURES FUNCTION
        elif feature_selection_choice == 'given features':
            ## MAKE FEATURE MASK
            feature_mask = make_feature_mask( np.array(PARAMETER_NAMES), features_to_choose )
            
#            print( feature_mask )
            
            ## RUN COMPUTE_USING_GIVEN_FEATURES WITH FEATURE MASK OF CHOSEN FEATURES
            results_list.append( n_fold_svm.compute_using_given_features( feature_mask ) )

    ## MAKE RESULTS LIST READABLE
    results_list = make_results_readable( np.array(results_list) )
    results_list = np.concatenate( (range_to_check[:,None], results_list), axis = 1)
    
    
    ## OUTPUT RESULTS TO CSV
    np.savetxt( out_file,
               results_list,
               delimiter = ","
               )
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    