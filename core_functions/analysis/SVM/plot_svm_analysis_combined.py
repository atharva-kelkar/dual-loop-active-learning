# -*- coding: utf-8 -*-
"""
Created on Wed May  5 18:37:55 2021

@author: Atharva Kelkar
@date: 05/05/2021
"""

import socket
import sys
sys.path.append(r'R://')
################################################
## LOAD MATPLOTLIB CORRECTLY DEPENDING ON SERVER
if 'swarm' in socket.gethostname():
    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt
else:
    import matplotlib.pyplot as plt
################################################
import numpy as np    
from analysis_scripts.python_scripts.plotting.plotting_functions import format_plot, load_default_params


'''
Function to convert shorthand parameter names to full parameter
'''
def convert_to_full_names( parameters ):
    ## IMPORT PARAMETERS KEY DICTIONARY
    from parameters_key import parameters_key
    
    parameters_full = []
    
    ## LOOP OVER ALL PARAMETERS IN PARAMETERS
    for parameter in parameters:
        parameters_full.append( parameters_key[ parameter ] )
    
    return np.array( parameters_full )

'''
Function to choose the color of the 
'''
def choose_color( ligand ):
    # IMPORT COLORS KEY DICTIONARY
    from parameters_key import colors_key
    
    return colors_key[ ligand ]

'''
Function to load Voronoi bin numbers and mid points from Excel file
'''
def load_voronoi_bins():
    
    path_to_excel = 'voronoi_bins_0.04.csv'
    data = np.loadtxt( path_to_excel, skiprows = 1, delimiter = "," )
    
    return np.array( data )

if __name__ == "__main__":
    
    ligand = 'amide'
    base_folder = 'results/'
    pickle_name = base_folder + '{}_1500_SVM_analysis_area_frac_nopolar.pickle'.format( ligand )
    fig_name = 'feature_figures/{}_area_frac/20210722_{}_feature_importance.svg'.format( ligand, ligand )
    ## LOAD ANALYSIS RESULTS PICKLE
    data = np.load( pickle_name,
                   allow_pickle = True
                   )
    
    ## LOAD RELEVANT PARTS OF THE PICKLE
    num_polar_all = data['num_polar']
    coeffs = data['mean coefficients']
    coeffs_std = data['std dev coefficients']
    train_score = data['train scores']
    val_score = data['val scores']
    num_train = data['# training points']
    num_val = data['# validation points']
    parameters = np.array( data['parameter names'] )
    parameters = convert_to_full_names( parameters )

    color = choose_color( ligand )
    
    ## FILTER OUT FIRST ROW OF ARRAY (NOT SIGNIFICANT)
    coeffs = coeffs[ 1: ]
    coeffs_std = coeffs_std[ 1: ]
    num_polar_all = num_polar_all[ 1: ]
    
    ## LOAD VORONOI BIN WIDTH PARAMS
    voronoi_bins = load_voronoi_bins()
    
    ## CONVERT BIN NUMBER TO POLAR AREA FRAC
    polar_area_frac = np.array( [ voronoi_bins[ voronoi_bins[:,0] == num, 2] for num in num_polar_all ] )
    
    ## MAKE ALL SCALED ARRAYS
    coeffs_all_scaled = coeffs / np.abs( coeffs ).max( axis = 1 )[:, None]
    coeffs_std_scaled = coeffs_std / np.abs( coeffs ).max( axis = 1 )[:, None] 
    
    ## MAKE MASK OF PARAMETERS WHICH ARE NON-ZERO
    mask_non_zero = np.any( coeffs_all_scaled != 0, axis = 0 )
    
    ## FILTER OUT NON-ZERO PARAMS
    parameters_non_zero = parameters[ mask_non_zero ]
    
    
    fig, ax = plt.subplots()
    
    ## LOOP OVER ALL NON-ZERO PARAMETERS AND IMPORTANCE PLOT
    for parameter in parameters_non_zero:
        ## MAKE MASK OF PARAMETER
        param_mask = parameters == parameter
        
        ## ISOLATE PARAMETER IMPORTANCE
        param_coeffs = coeffs_all_scaled[ : , param_mask ]
        param_std = coeffs_std_scaled[ : , param_mask ]
        
        ## PLOT PARAMCOEFF
        ax.errorbar( polar_area_frac,
                    param_coeffs,
                    yerr = param_std.squeeze(),
                    label = parameter,
                    marker = 'o',
                    capsize = 4, capthick = 2,
                    linewidth = 2
                    )
    
    ## FORMAT PLOT
    xlabel = 'Polar area fraction'
    ylabel = 'Feature Importance'
    kwargs = load_default_params( xlabel, ylabel )
    kwargs['want_legend'] = 1
    ax.set_ylim([-1.2, 1.2])
    
    ax = format_plot( ax, **kwargs )
    
    ## SAVE PLOT
    plt.savefig( fig_name,
                bbox_inches = 'tight',
                format = fig_name.split('.')[-1]
                )













