#!/bin/bash

ligands=('amine' 'amide' 'hydrox')

for ligand in ${ligands[@]}; do

	python3.4 classify_using_1_feature.py $ligand

done


