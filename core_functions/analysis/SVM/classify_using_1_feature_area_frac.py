# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 05/16/2021

"""

import sys, numpy as np, pickle
sys.path.append('/home/kelkar2/analysis_scripts/python_scripts/gpr/core_functions/')
## IMPORT MATPLOTLIB CORRECTLY
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from svm_extremes_normalized import nFoldCV_SVM, choose_using_mask, generate_std_mask, make_binary_labels
from regression.regression_order_parameters_normalized import load_parameters, normalize
from analyze_svm_results import make_coefficients_array
from scipy.stats import rankdata
from analysis_scripts.python_scripts.plotting.plotting_functions import format_plot, load_default_params
from analysis.histogram_gpr_preds_per_num_polar import histogram_and_get_mid_points, make_bar_plot

def format_plot_local( ax, x_label, y_label ):    

    ## FORMAT PLOT
    kwargs = {'x_label': x_label,
              'y_label': y_label,
              'label_fontsize': 14,
              'want_legend': 1,
              'legend_fontsize': 14,
              'tick_fontsize': 14,
              'want_grid': -1
            }
    
    ax = format_plot( ax, **kwargs )


'''
Function to plot chosen parameters
'''
def plot_by_chosen_parameter( X, y, y_binary ):
    
    fig, ax = plt.subplots()
    
    ## PLOT BOTH SETS OF HFEs AS A FUNCTION OF PARAMETER VALUE
    ax.plot( X[ y_binary == 1 ], y[ y_binary == 1 ], 'o', 
            markersize = 12, color = 'tab:red', label = "Hydrophilic"
            )
    
    ax.plot( X[ y_binary == -1 ], y[ y_binary == -1 ], 'x', 
            markersize = 12, color = 'black', label = "Hydrophobic",
            markeredgewidth = 3
            )
    
    ## FORMAT PLOT
    ax = format_plot_local( ax,
                           'Average clustering of nonpolar groups', 
                           'HFE (k$_B$T)'
                           )
    
    return fig, ax
    
'''
Function to plot chosen parameter as a histogram
'''
def plot_histogram_of_chosen_parameter( X, y_binary, x_label, y_label ):
    
    fig, ax = plt.subplots()
    
    ## HISTOGRAM X INTO TWO BINS
    X_one, width_one, mid_points_one = histogram_and_get_mid_points( X[ y_binary == 1 ] )
    print( X_one, width_one, mid_points_one )
    
    ## HISTOGRAM X INTO TWO BINS
    X_mone, width_mone, mid_points_mone = histogram_and_get_mid_points( X[ y_binary == -1 ] )
    
    ## DEFINE AND LOAD PARAMETERS FOR FORMATTING PLOTS
    kwargs = load_default_params( x_label, y_label )
    kwargs['want_legend'] = 1
    
    ## PLOT BAR PLOT FOR X_ONE
    ax = make_bar_plot( ax,
                       mid_points_one, 
                       X_one, 
                       width_one, 
                       0,
                       0,
                       0,
                       add_normal = 'no',
                       make_arrow = 'no',
                       color = 'tab:red',
                       label = 'Hydrophilic',
                       **kwargs 
                       )
    
    ## PLOT BAR PLOT FOR X_MONE
    ax = make_bar_plot(ax,
                       mid_points_mone, 
                       X_mone, 
                       width_mone, 
                       0,
                       0,
                       0,
                       add_normal = 'no',
                       make_arrow = 'no',
                       color = 'black',
                       label = 'Hydrophobic',
                       **kwargs 
                       )
    
    return fig, ax
    

if __name__ == "__main__":
    
    '''
    Define file names and parameters
    '''
    base_folder = '/home/kelkar2/analysis_scripts/python_scripts/gpr/core_functions/order_parameter_results/'
    ligand = sys.argv[1] # 'amide'
    pickle_file = '{}_1500/pattern_order_parameters_{}_1500_area_frac.pickle'.format( ligand, ligand )
    out_file = '{}_1500_SVM_results.pickle'.format( ligand )
    ## SPECIFY TYPE OF PLOT YOU WANT
    plot_type = 'compile values for boxplot' # 'hfe vs parameter' or 'histogram' or 'compile values' or 'compile values for boxplot'
    ## IMPORT LIST OF PARAMETER NAMES FROM GLOBAL_VARS
    from global_vars.global_vars import PARAMETER_NAMES
    
    
    '''
    Load all parameters from pickle file
    '''
    data = np.load( base_folder + pickle_file )
    
    parameters = load_parameters( data, PARAMETER_NAMES )

    '''
    Define labels array with standard deviation
    '''
    labels = data[ 'labels' ][:, None]
    std_dev = data[ 'std dev' ]

    '''
    Perform regression with parameters array as X and labels as Y for different number of polar groups
    '''
    
    range_to_check = np.unique( data['area frac bins'] )[5:14]
#    range_to_check = np.unique( data['num_polar'] )[5:11]
#    range_to_check = np.unique( 7 ) # Only one number of polar groups to check right now
    
    results_list = []
    
    for bin_number in range_to_check:
        ## GENERATE MASK WITH NUMBER OF POLAR GROUPS
        mask = data['area frac bins'] == bin_number
        
        # CHOOSE LABELS AND PARAMETERS USING MASK
        parameters_chosen, labels_chosen = choose_using_mask( parameters, 
                                                              labels, 
                                                              mask 
                                                              )
        
        ## NORMALIZE PARAMETERS AND LABELS CHOSEN
        parameters_chosen_norm, labels_chosen_norm = normalize( parameters_chosen ), normalize( labels_chosen )
        
        ## CREATE MASK FOR VALUES N STD AWAY (ON NORMALIZED DATA)
        std_mask = generate_std_mask( labels_chosen_norm, 
                                     n = 2
                                     )
        
        ## CHOOSE LABELS AND PARAMETERS USING STD_MASK
        parameters_chosen_norm, labels_chosen_norm = choose_using_mask( parameters_chosen_norm, 
                                                                       labels_chosen_norm, 
                                                                       std_mask 
                                                                       )
        
        parameters_chosen, labels_chosen = choose_using_mask( parameters_chosen, 
                                                             labels_chosen, 
                                                             std_mask 
                                                             )
        
        ## ALLOCATE LABELS VALUES OF -1 AND 1
        labels_classify = make_binary_labels( labels_chosen_norm, [-1,1] )
        
        ## SELECT PARAMETER TO PLOT AGAINST
        parameter_to_plot_name = 'avg_clust_normal_0'
        
        ## FILTER USING PARAMETER TO PLOT AGAINST
        parameter_to_plot = parameters_chosen_norm[ :, np.array( PARAMETER_NAMES ) == parameter_to_plot_name ]
        parameter_to_plot_unnorm = parameters_chosen[ :, np.array( PARAMETER_NAMES ) == parameter_to_plot_name ]
        
        if plot_type == 'hfe vs parameter':
            ## PLOT HFE AS A FUNCTION OF CHOSEN PARAMETERS
            fig, ax = plot_by_chosen_parameter( parameter_to_plot,
                                               labels_chosen_norm,
                                               labels_classify
                                               )
        
            ## SAVE FIGURE
            plt.savefig('feature_figures/{}/20210617_{}_1500_area_frac_{}.png'.format( ligand, parameter_to_plot_name, bin_number ), 
                        bbox_inches = 'tight'
                        )
        elif plot_type == 'histogram':
            ## PLOT HFE AS A FUNCTION OF CHOSEN PARAMETERS
            fig, ax = plot_histogram_of_chosen_parameter( parameter_to_plot,
                                                           labels_classify,
                                                           parameter_to_plot_name,
                                                           '# patterns'
                                                           )
            plt.savefig('feature_figures/{}/20210617_{}_1500_feature_hist_area_frac_{}.png'.format( ligand, parameter_to_plot_name, bin_number ), 
                        bbox_inches = 'tight'
                        )
            
        elif plot_type == 'compile values':
            ## COLLATE PARAMETER VALUES FOR LABELS +/-1
            results_list.append( [ bin_number, 
                                  parameter_to_plot_unnorm.shape[0],
                                  parameter_to_plot_unnorm[ labels_classify == -1 ].shape[0],
                                  np.mean( parameter_to_plot_unnorm[ labels_classify == -1 ]),
                                  np.std( parameter_to_plot_unnorm[ labels_classify == -1 ]),
                                  parameter_to_plot[ labels_classify == 1 ].shape[0],
                                  np.mean( parameter_to_plot_unnorm[ labels_classify == 1 ]),
                                  np.std( parameter_to_plot_unnorm[ labels_classify == 1 ])
                                  ] 
                                )
            
        elif plot_type == 'compile values for boxplot':
            ## COLLATE PARAMETER VALUES FOR LABELS +/-1
            results_list.append( [ bin_number, 
                                  parameter_to_plot_unnorm[ labels_classify == -1 ],
                                  parameter_to_plot_unnorm[ labels_classify == 1 ]
                                  ] 
                                )
            
            
        print( '*** Plotting classes with top feature for area_frac_bin = {} ***'.format( bin_number ) )

    ## SAVE RESULTS ARRAY IF COMPILE VALUES
    if plot_type == 'compile values':
        np.savetxt('results/{}/20210617_{}_1500_extremes_{}_area_frac_{}.csv'.format( ligand, 
                                                                                       ligand, 
                                                                                       parameter_to_plot_name, 
                                                                                       bin_number
                                                                                       ),
                   np.array( results_list ),
                   delimiter = ","
                   )
                
    ## SAVE RESULTS ARRAY IF COMPILE VALUES
    if plot_type == 'compile values for boxplot':
        
        print('** WRITING PICKLE FOR BOXPLOT **')
        file_name = 'results/{}/20210620_{}_1500_extremes_{}_area_frac_for_boxplot.pickle'.format( ligand,
                                                                                                  ligand, 
                                                                                                  parameter_to_plot_name, 
                                                                                                  )
        
        pickle.dump( results_list, 
                    open( file_name, 'wb' )
                    )
        
                   