# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 05/13/2021

"""



parameters_key = {'node_conn_normal_0': "Nonpolar group node conn", 
                  'node_conn_normal_1': "Polar group node conn", 
                  'node_conn_padded_0': "Nonpolar group node conn (padded)", 
                  'avg_clust_normal_0': "Nonpolar group avg clustering", 
                  'avg_clust_normal_1': "Polar group avg clustering", 
                  'avg_clust_padded_0': "Polar group avg clustering (padded)", 
                  'padded adj geary C': "Adjusted Geary's C (padded)", 
                  'padded morans I': "Moran's I (padded)", 
                  'adj geary C': "Adjusted Geary's C",
                  'morans I': "Moran's I",
		  'polar area frac': "polar area fraction"
                    }


colors_key = {'amide'    : 'tab:green',
             'amine'    : 'tab:blue',
             'hydrox'   : 'tab:red'
             }
