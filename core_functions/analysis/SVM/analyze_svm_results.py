# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 05/05/2021

"""

import numpy as np, sys, pickle
sys.path.append('/home/kelkar2/analysis_scripts/python_scripts/gpr/core_functions/analysis/')
from global_vars.global_vars import PARAMETER_NAMES


'''
Function to make workable coefficients array from raw coefficients array
'''
def make_coefficients_array( array ):
    return np.array( [ x[0] for x in array ] )

'''
Function to convert raw data to malleable data (Author is very proud of this word usage)
'''
def convert_raw_data( analysis_data ):
    coeffs = make_coefficients_array( analysis_data[ : , 0 ] )
    train_scores = np.mean( analysis_data[:, 1] ) 
    val_scores = np.mean( analysis_data[:, 2] )
    num_train_points = int(np.mean( analysis_data[:, 3] ))
    num_val_points = int(np.mean(analysis_data[:, 4]))
    
    return coeffs, train_scores, val_scores, num_train_points, num_val_points
    
    
    
if __name__ == "__main__":
    
    ligand = sys.argv[1]
    base_folder = 'results/'
    pickle_file = base_folder + '{}_1500_SVM_results_area_frac_nopolar.pickle'.format( ligand )
    out_file = base_folder + '{}_1500_SVM_analysis_area_frac_nopolar.pickle'.format( ligand )
    out_csv = base_folder + '{}_1500_SVM_analysis_area_frac_nopolar.csv'.format( ligand )

    ## LOAD PICKLE FILE
    data = np.load( pickle_file )
    
    ## DEFINE BLANK RESULTS ARRAYS
    num_polar_all = np.zeros( len(data) )
    coeffs_all_mean = np.zeros(( len(data), len(PARAMETER_NAMES) ))
    coeffs_all_std = np.zeros(( len(data), len(PARAMETER_NAMES) ))
    train_scores = np.zeros( len(data) )
    val_scores = np.zeros( len(data) )
    num_train_points = np.zeros( len(data) )
    num_val_points = np.zeros( len(data) )
    
    ## LOOP THROUGH DATA
    for count in range( len(data) ):
        ## EXTRACT NUMBER OF POLAR GROUPS
        num_polar = data[ count ][ 0 ]
        num_polar_all[ count ] = num_polar
        print('**** Analyzing data for num_polar = {} ****'.format( num_polar ) )

        ## EXTRACT DATA OF COEFFICIENTS AND CLASS SCORES
        analysis_data = np.array( data[ count ][ 1 ] )

        ## EXTRACT ALL USEFUL DATA FROM COEFFS
        coeffs, \
        train_scores[count], \
        val_scores[count], \
        num_train_points[count], \
        num_val_points[count] = convert_raw_data( analysis_data )

        ## AVERAGE OVER ALL COEFFICIENTS TO GET MEAN AND STD OF COEFFICIENTS
        coeffs_all_mean[ count, : ], coeffs_all_std[ count, : ] = coeffs.mean( axis = 0 ), coeffs.std( axis = 0 )

    ## STORE ALL RELEVANT DATA AS ONE DICTIONARY
    dict = { 'num_polar': num_polar_all,
            'mean coefficients': coeffs_all_mean,
            'std dev coefficients': coeffs_all_std,
            'train scores': train_scores,
            'val scores': val_scores,
            '# training points': num_train_points,
            '# validation points': num_val_points,
            'parameter names': PARAMETER_NAMES
            }        
    
    ## WRITE DICTIONARY TO PICKLE
    pickle.dump( dict, open(out_file, 'wb'))
    
    ## STORE IN EASILY READABLE FORMAT
    res = np.concatenate( (num_polar_all[:,None],
                         coeffs_all_mean,
                         coeffs_all_std,
                         train_scores[:,None],
                         val_scores[:,None]),
                        axis = -1
                         )
    np.savetxt( out_csv, 
               res,
               delimiter = ","
               )
        
        
        
        
        
