# -*- coding: utf-8 -*-
"""
Created on Wed May  5 18:37:55 2021

@author: Atharva Kelkar
@date: 05/05/2021
"""

import socket
import sys
sys.path.append(r'R://')
################################################
## LOAD MATPLOTLIB CORRECTLY DEPENDING ON SERVER
if 'swarm' in socket.gethostname():
    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt
else:
    import matplotlib.pyplot as plt
################################################
import numpy as np    
from analysis_scripts.python_scripts.plotting.plotting_functions import format_plot

'''
Function to plot bar plot
'''
def plot_bar_plot(num_polar, coeffs, coeffs_std, train_score, val_score, num_train, num_val, parameters, color):
    
    ## FIND NON-ZERO PARAMETERS TO PLOT
    non_zero_coeffs_mask = (coeffs != 0).squeeze()
    print(coeffs)
    coeffs_non_zero = coeffs[ non_zero_coeffs_mask ]
    coeffs_std_non_zero = coeffs_std[ non_zero_coeffs_mask ]
    print( parameters.shape )
    print( non_zero_coeffs_mask.shape )
    print( coeffs_non_zero )
    parameters_non_zero = parameters[ non_zero_coeffs_mask ]

    ## DEFINE FIG, AX OBJECT
    fig, ax = plt.subplots()
    
    ## PLOT BAR GRAPH
    ax.bar( parameters_non_zero, coeffs_non_zero/np.abs(coeffs_non_zero).max(), 
           color = color
           )
    ax.set_xticklabels( parameters_non_zero, rotation = 90 )
    ax.set_ylim([-1.2,1.2])
    
    ## ADD ERRORBARS
    ax.errorbar( parameters_non_zero, coeffs_non_zero/np.abs(coeffs_non_zero).max(), 
                linestyle = 'None', color = 'black',
                yerr = coeffs_std_non_zero, 
                capsize = 5, capthick = 2 
                )
    
    ## FORMAT PLOT
    kwargs = {'x_label': '',
              'y_label': 'Feature importance',
              'label_fontsize': 14,
              'want_legend': -1,
              'legend_fontsize': 14,
              'tick_fontsize': 14,
              'want_grid': -1
            }
    
    ax = format_plot( ax, **kwargs )

    ax.set_title('{} polar groups; {} total samples; val score = {:0.2f}'.format( num_polar, 
                                                                                 int(num_train+num_val), 
                                                                                 val_score ), 
                fontsize = 14 
                )
    
    return fig, ax

'''
Function to convert shorthand parameter names to full parameter
'''
def convert_to_full_names( parameters ):
    ## IMPORT PARAMETERS KEY DICTIONARY
    from parameters_key import parameters_key
    
    parameters_full = []
    
    ## LOOP OVER ALL PARAMETERS IN PARAMETERS
    for parameter in parameters:
        parameters_full.append( parameters_key[ parameter ] )
    
    return np.array( parameters_full )

'''
Function to choose the color of the 
'''
def choose_color( ligand ):
    # IMPORT COLORS KEY DICTIONARY
    from parameters_key import colors_key
    
    return colors_key[ ligand ]

if __name__ == "__main__":
    
    ligand = 'amide'
    base_folder = 'results/'
    pickle_name = base_folder + '{}_1500_SVM_analysis_area_frac.pickle'.format( ligand )
    
    ## LOAD ANALYSIS RESULTS PICKLE
    data = np.load( pickle_name )
    
    ## LOAD RELEVANT PARTS OF THE PICKLE
    num_polar_all = data['num_polar']
    coeffs = data['mean coefficients']
    coeffs_std = data['std dev coefficients']
    train_score = data['train scores']
    val_score = data['val scores']
    num_train = data['# training points']
    num_val = data['# validation points']
    parameters = np.array( data['parameter names'] )
    parameters = convert_to_full_names( parameters )

    color = choose_color( ligand )
    
    ## LOOP THROUGH NUMBER OF POLAR GROUPS AND PLOT FEATURES AND COEFFICIENTS ON BAR GRAPHS
    for num_polar in num_polar_all[1:]:
        mask = num_polar_all == num_polar
        
        fig, ax = plot_bar_plot(num_polar, 
                                coeffs[ mask ].squeeze(),
                                coeffs_std[ mask ].squeeze(),
                                train_score[ mask ].squeeze(),
                                val_score[ mask ].squeeze(),
                                num_train[ mask ].squeeze(),
                                num_val[ mask ].squeeze(),
                                parameters,
                                color
                                )
    
        plt.savefig('feature_figures/{}_1500_svm_features_bin_number_{}.png'.format( ligand, num_polar ),
                    bbox_inches = 'tight'
                    )
    
    
    
