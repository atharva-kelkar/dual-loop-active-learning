# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 05/03/2021

"""

import numpy as np, sys
sys.path.append('/home/kelkar2/analysis_scripts/python_scripts/gpr/core_functions/analysis/')
from regression.regression_order_parameters_normalized import load_parameters, normalize
## IMPORT LINEAR SVC FROM SKLEARN.SVM
from sklearn.svm import LinearSVC
import pickle

'''
Function to choose parameters and labels using mask
'''
def choose_using_mask( parameters, labels, mask ):
    return parameters[ mask.squeeze(), : ], labels[ mask.squeeze(), : ]

'''
Function to make mask for data 'n' standard deviations away from data mean
'''
def generate_std_mask( labels, n = 1 ):
    return (labels > labels.mean() + n * labels.std()) | (labels < labels.mean() - n * labels.std()) 

'''
Function to make binary labels from labels_chosen
    - Methodology for allocating labels:
        -- value < mean: new_labels[0]
        -- value > mean: new_labels[1]
'''
def make_binary_labels( labels_chosen, new_labels ):
    new_label_arr = np.copy( labels_chosen )
    if len(new_labels) == 2:
        new_label_arr[ labels_chosen <= labels_chosen.mean() ] = new_labels[0]
        new_label_arr[ labels_chosen > labels_chosen.mean() ] = new_labels[1]
    
    return new_label_arr

'''
Function to run 5-fold CV for SVC
'''
class nFoldCV_SVM():
    
    '''
    __init__ constructor
    
    '''
    def __init__(self,
                 parameters,
                 labels,
                 n_folds = 5,
                 penalty = 'l1',
                 C = 0.005,
                 dual = False,
                 max_iter = 10000
                 ):
        self.parameters = parameters
        self.labels = labels
        self.n_folds = n_folds
        self.penalty = penalty
        self.C = C
        self.dual = dual
        self.max_iter = max_iter
        self.num_points = labels.shape[0]
    
    
    '''
    Function to make 5 folds from given data
    '''
    def generate_n_folds( self ):
        folds = np.arange( self.num_points )
        np.random.shuffle( folds )
        self.folds = np.array_split( folds, self.n_folds )
    
    
    '''
    Function to run LinearSVC
    '''
    def run_svm( self, X_train, y_train, X_val, y_val ):
        ## MAKE LINEARSVC OBJECT
        svc = LinearSVC( penalty = self.penalty,
                        C = self.C,
                        dual = self.dual,
                        max_iter = self.max_iter
                        )
        ## FIT MODEL TO TRAINING DATA
        svc.fit( X_train, y_train )
        
        train_score = svc.score( X_train, y_train )
        val_score = svc.score( X_val, y_val )
        print( 'Score on training data is {:0.2f} and val data is {:0.2f}; '.format( train_score, val_score ) + \
              '# of training points is {} and # of validation points is {}; total number of points is {}'\
              .format( len(y_train), len(y_val), len(y_train) + len(y_val) ) )

        ## RETURN THE FOLLOWING DATA - COEFFICIENTS, TRAIN SCORE, VAL SCORE
        return [ svc.coef_, train_score, val_score, len(y_train), len(y_val) ]

    '''
    Function to make training data given parameters, labels, and fold values
        Methodology:
            - Delete all parameters and labels values that correspond to fold values
    '''
    @staticmethod
    def make_training_data( parameters, labels, fold ):
        return np.delete( parameters, fold, axis = 0 ), np.delete( labels, fold, axis = 0 )
        
    '''
    Function to make validation data given parameters, labels, and fold values
        Methodology:
            - Keep all parameters and labels values that correspond to fold values
    '''
    @staticmethod
    def make_validation_data( parameters, labels, fold ):
        return parameters[ fold ], labels[ fold ]
    
    
    '''
    Function to run n-fold CV
    '''
    def compute( self ):
        ## GENERATE N FOLDS
        self.generate_n_folds()
        
        ## DEFINE BLANK RESULTS ARRAY
        self.results = []
        
        ## CYCLE THROUGH FOLDS IN ORDER
        for fold in self.folds:
            ## MAKE TRAINING DATA BY DELETING DATA IN FOLD
            X_train, y_train = self.make_training_data( self.parameters, self.labels, fold )
            X_train = normalize( X_train )
            ## MAKE VALIDATION DATA BY KEEPING DATA IN FOLD
            X_val, y_val = self.make_validation_data( self.parameters, self.labels, fold )
            X_val = normalize( X_val )
            ## RUN SVM WITH TRAINING AND VALIDATION DATA
            self.results.append( self.run_svm( X_train, y_train,
                                              X_val, y_val
                                              ) 
                                )
        
        return self.results



'''
Main function
'''
if __name__ == "__main__":
    
    '''
    Define file names and parameters
    '''
    base_folder = '/home/kelkar2/analysis_scripts/python_scripts/gpr/core_functions/order_parameter_results/'
    ligand = 'amide'
    pickle_file = '{}_1500/pattern_order_parameters_{}_1500_v2.pickle'.format( ligand, ligand )
    out_file = '20210513_{}_1500_SVM_results.pickle'.format( ligand )
    ## IMPORT LIST OF PARAMETER NAMES FROM GLOBAL_VARS
    from global_vars.global_vars import PARAMETER_NAMES
    
    '''
    Load all parameters from pickle file
    '''
    data = np.load( base_folder + pickle_file )
    
    parameters = load_parameters( data, PARAMETER_NAMES )

    '''
    Define labels array with standard deviation
    '''
    labels = data[ 'labels' ][:, None]
    std_dev = data[ 'std dev' ]

    '''
    Perform regression with parameters array as X and labels as Y for different number of polar groups
    '''
    
    range_to_check = np.unique( data['num_polar'] )[5:11]
#    range_to_check = np.unique( 7 ) # Only one number of polar groups to check right now
    
    results_list = []
    
    for num_polar in range_to_check:
        ## GENERATE MASK WITH NUMBER OF POLAR GROUPS
        mask = data['num_polar'] == num_polar
        
        # CHOOSE LABELS AND PARAMETERS USING MASK
        parameters_chosen, labels_chosen = choose_using_mask( parameters, 
                                                              labels, 
                                                              mask 
                                                              )
        
        ## NORMALIZE PARAMETERS AND LABELS CHOSEN
        parameters_chosen_norm, labels_chosen_norm = normalize( parameters_chosen ), normalize( labels_chosen )
        
        ## CREATE MASK FOR VALUES N STD AWAY (ON NORMALIZED DATA)
        std_mask = generate_std_mask( labels_chosen_norm, 
                                     n = 2
                                     )
        
        ## CHOOSE LABELS AND PARAMETERS USING STD_MASK
        parameters_chosen_norm, labels_chosen_norm = choose_using_mask( parameters_chosen_norm, 
                                                                       labels_chosen_norm, 
                                                                       std_mask 
                                                                       )
        
        ## ALLOCATE LABELS VALUES OF -1 AND 1
        labels_classify = make_binary_labels( labels_chosen_norm, [-1,1] )
        
        
        ## MAKE N-FOLD CV OBJECT WITH CHOSEN LABELS AND PARAMETERS
        n_fold_svm = nFoldCV_SVM( parameters_chosen_norm,
                                 labels_classify.squeeze(),
                                 )
        ## RUN COMPUTE FUNCTION AND APPEND TO OVERALL RESULTS LIST
        results_list.append( [ num_polar, n_fold_svm.compute() ] )
        
    ## STORE OUTPUT IN PICKLE
    pickle.dump( results_list, open(out_file, 'wb') )
        














