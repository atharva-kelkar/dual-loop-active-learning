# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 06/11/2021

"""

from gpr_model_analysis_voronoi_area import Predictions_wVoronoi

from plotting_functions import plot_mu_vs_mf_by_type_subplots

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


def make_plot_data( folder_name, file_name, master_folder_name, master_file_name, n_current ):
    
    ## CREATE OBJECTS OF PREDICTION CLASS
    predictions = Predictions_wVoronoi( folder_name , file_name )
    
    ## LOAD ALL PREDICTIONS, MEAN, AND STD
    predictions.load_predictions()
    predictions.load_mean_std( master_folder_name, master_file_name, n_current )
    
    ## MAKE VORONOI AREAS
    predictions.make_voronoi()
    
    ## CALCULATE UNSCALED MU AND SIGMA VALUES
    predictions.unscale_mu_sigma()
    
    ## LOAD MASTER FILE HFEs AND PATTERNS
    predictions.load_master_hfe( master_folder_name, master_file_name, n_current )
    
    ## SPLIT MASTER FILE HFEs AND PATTERNS INTO INDUS AND CNN VALUES
    predictions.split_indus_cnn( 2.0, 6.0 ) # 2.0 --> INDUS sigma, 6.0 --> CNN sigma
    
    ## MAKE MASK FOR INDUS AND CNN PATTERNS AMONG ALL PATTERNS
    indus_mask = predictions.make_mask( 'indus' )
    cnn_mask = predictions.make_mask( 'cnn' )
    other_mask = ~(indus_mask | cnn_mask)

    return predictions, indus_mask, cnn_mask, other_mask


if __name__ == "__main__":
    
    ## DEFINE FILE AND FOLDER NAMES
    n_current = 52
    
    ## DEFINE AMINE METRICS
    base_dir = '../../active_learning_amine/'
    folder_name = base_dir + 'active_learning_patterns/'
    file_name = 'gpr_prediction_iteration_{:d}.pickle'.format( n_current )
    master_folder_name = base_dir +  'pattern_labels/'
    master_file_name = 'master_patterns_labels.pickle'
    
    amine_data = make_plot_data( folder_name, 
                                file_name, 
                                master_folder_name, 
                                master_file_name, 
                                n_current 
                                )
    
    ## DEFINE HYDROX METRICS
    base_dir = '../../active_learning_run3/'
    folder_name = base_dir + 'active_learning_patterns/'
    file_name = 'gpr_prediction_iteration_{:d}.pickle'.format( n_current )
    master_folder_name = base_dir +  'pattern_labels/'
    master_file_name = 'master_patterns_labels.pickle'
    
    hydrox_data = make_plot_data( folder_name, 
                                 file_name, 
                                 master_folder_name, 
                                 master_file_name, 
                                 n_current 
                                 )
    
    ## DEFINE AMIDE METRICS
    base_dir = '../../active_learning_amide/'
    folder_name = base_dir + 'active_learning_patterns/'
    file_name = 'gpr_prediction_iteration_{:d}.pickle'.format( n_current )
    master_folder_name = base_dir +  'pattern_labels/'
    master_file_name = 'master_patterns_labels.pickle'
    
    amide_data = make_plot_data( folder_name, 
                                 file_name, 
                                 master_folder_name, 
                                 master_file_name, 
                                 n_current 
                                 )
#    plt.rcParams['figure.figsize' = (7,5) ]
    fig, ax = plt.subplots( nrows=1, ncols=3, figsize=(7,2.5) )
    x_label = 'Polar area fraction'
    
    ## PLOT AMINE DATA
    data = amine_data
    ax[0] = plot_mu_vs_mf_by_type_subplots( ax[0],
                                            data[0].polar_area_frac_all,
                                            data[0].unscaled_mu,
                                            data[1],
                                            data[2],
                                            data[3],
                                            x_label
                                            )
    
    ## PLOT HYDROX DATA
    data = hydrox_data
    ax[1] = plot_mu_vs_mf_by_type_subplots( ax[1],
                                            data[0].polar_area_frac_all,
                                            data[0].unscaled_mu,
                                            data[1],
                                            data[2],
                                            data[3],
                                            x_label
                                            )
    
    ## PLOT AMIDE DATA
    data = amide_data
    ax[2] = plot_mu_vs_mf_by_type_subplots( ax[2],
                                            data[0].polar_area_frac_all,
                                            data[0].unscaled_mu,
                                            data[1],
                                            data[2],
                                            data[3],
                                            x_label
                                            )
    
    plt.tight_layout()
    
    plt.savefig('all_together.png', dpi = 100, bbox_inches = "tight" )
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    