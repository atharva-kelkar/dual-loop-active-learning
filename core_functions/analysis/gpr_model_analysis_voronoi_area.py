# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 01/25/2021

"""

import numpy as np, pandas as pd, sys, time
sys.path.append('/home/kelkar2/analysis_scripts/python_scripts/gpr/core_functions/')
## IMPORT SUPPORT FUNCTIONS
from analysis_scripts.python_scripts.gpr.active_learning.make_hypercube import gen_all_patterns

from analysis_scripts.python_scripts.gpr.core_functions.analysis.pattern_analysis import set_nan_0, plot_by_num_polar_geary_C, plot_linear_interpolation

import analysis.plotting_functions as plot


## IMPORT PREDICTIONS CLASS FROM GPR_MODEL_ANALYSIS.PY
from analysis.gpr_model_analysis import Predictions
        

'''
class that inherits properties from Predictions and makes Voronoi areas

    Changelog:
        - 06/03/2021: 
            - Changed class name from Predictions to Predictions_wVoronoi
            - Added inheritance from Predictions class
'''
class Predictions_wVoronoi( Predictions ):
    
    def __init__( self, 
                 folder_name, 
                 file_name, 
                 voronoi_areas_pickle = '/home/kelkar2/analysis_scripts/python_scripts/gpr/core_functions/gridding/average_voronoi_areas.pkl'
                 ):
        
        ## INITIALIZE PARENT CLASS
        Predictions.__init__( self, 
                             folder_name, 
                             file_name 
                             )
        
        ## ALLOCATE NEW VARIABLES TO CHILD CLASS
        self.voronoi_areas_pickle = voronoi_areas_pickle
        
        
    '''
    Function to load predictions given folder name and file name
    '''
    def make_voronoi( self ):
        self.voronoi_areas          =   np.load( self.voronoi_areas_pickle, allow_pickle = True )[:, 2] # COL 2 HAS ALL AREAS, 0 AND 1 ARE INDICES
        self.polar_area_all         =   np.sum( self.patterns_all * self.voronoi_areas, axis = 1 )
        self.polar_area_frac_all    =   self.polar_area_all/4 # 4 BECAUSE OF 2.0x2.0 GRID
    
    '''
    Function to make polar area frac for master patterns
    '''
    def make_master_voronoi( self ):
        self.master_indus_polar_area_frac = np.sum( self.master_patterns_indus * self.voronoi_areas, axis = 1 )/4
        self.master_cnn_polar_area_frac = np.sum( self.master_patterns_cnn * self.voronoi_areas, axis = 1 )/4

    '''
    Function to make data_array with area fraction, a writeable array that can be used to read GPR predictions in an Excel file
        Inputs:
            - indus_mask: 1 if INDUS calculated, 0 if not
            - cnn_mask: 1 if CNN calculated, 0 if not
    '''
    def make_data_array_with_area_frac( self, indus_mask, cnn_mask ):
    
        self.data_array = np.concatenate((self.num_polar_all[:,None],
                                          self.polar_area_frac_all[:,None],
                                          self.unscaled_mu,
                                          self.unscaled_sigma,
                                          indus_mask[:,None],
                                          cnn_mask[:,None]),
                                        axis = -1
                                        )

if __name__ == "__main__":
    
    
    ## DEFINE FILE AND FOLDER NAMES
    n_current = int(sys.argv[1])
    ligand = 'amide'
    base_dir = '../../active_learning_amide/'
    folder_name = base_dir + 'active_learning_patterns/'
    file_name = 'gpr_prediction_iteration_{:d}.pickle'.format( n_current )
    
    ## DEFINE MASTER FOLDER AND MASTER FILE NAME
    master_folder_name = base_dir +  'pattern_labels/'
    master_file_name = 'master_patterns_labels.pickle'
    
    ## CREATE OBJECTS OF PREDICTION CLASS
    predictions = Predictions_wVoronoi( folder_name , file_name )
    
    ## LOAD ALL PREDICTIONS, MEAN, AND STD
    predictions.load_predictions()
    predictions.load_mean_std( master_folder_name, master_file_name, n_current )
    
    ## MAKE VORONOI AREAS
    predictions.make_voronoi()
    
    ## CALCULATE UNSCALED MU AND SIGMA VALUES
    predictions.unscale_mu_sigma()
    
    ## PLOT GPR MU AS A FUNCTION OF MOLE FRAC
#    plot_mu_vs_mf( predictions )
    
    ## LOAD MASTER FILE HFEs AND PATTERNS
    predictions.load_master_hfe( master_folder_name, master_file_name, n_current )
    
    ## SPLIT MASTER FILE HFEs AND PATTERNS INTO INDUS AND CNN VALUES
    predictions.split_indus_cnn( 2.0, 6.0 ) # 2.0 --> INDUS sigma, 6.0 --> CNN sigma
    
    ## MAKE MASK FOR INDUS AND CNN PATTERNS AMONG ALL PATTERNS
    indus_mask = predictions.make_mask( 'indus' )
    cnn_mask = predictions.make_mask( 'cnn' )
    other_mask = ~(indus_mask | cnn_mask)

    ## MAKE DATA ARRAY TO CONVERT DATA INTO READABLE FORMAT
    predictions.make_data_array_with_area_frac( indus_mask, 
                                               cnn_mask 
                                               )
    
    ## SAVE DATA_ARRAY IN CSV FILE
#    np.savetxt('{}_1500_gpr_pred_area_frac.csv'.format( ligand ), predictions.data_array, 
#               delimiter = ","
#               )
    
    '''
    Plot GPR model HFE predictions as a function of number of polar groups
    Differentiate surfaces with and without INDUS and CNN predictions
    '''
    to_plot_mu = True

    if to_plot_mu:
        
        fig, ax = plot.plot_mu_vs_mf_by_type( predictions.polar_area_frac_all,
                                             predictions.unscaled_mu, 
                                             indus_mask, 
                                             cnn_mask, 
                                             other_mask,
                                             'Polar area fraction',
                                             plot_randomly = True,
                                             n = 5
                                             )
        ax.set_xticks([0,0.2,0.4,0.6,0.8])
        plot.plt.savefig('figures/20210610_gpr_model_predictions_differentiated_{}_{:d}.png'.format( ligand, n_current ), 
                    bbox_inches = 'tight',
                    format = 'png'
                    )

    '''
    Section to plot positive and negative deviation points
    '''
    to_plot_dev = False
    
    if to_plot_dev:
        '''
        Import negative deviation points from neg_dev_indus
        '''
        indus_file = 'dev_hfe/dev_all.xls'
        sheet_name = 'neg_{}'.format( ligand )
        x_label = 'Number of polar'
        y_label = 'HFE (kT)'
        marker = 'x'
        label = 'Negative deviation INDUS'
        
        ## READ EXCEL FILE
        data = pd.read_excel( indus_file, 
                             sheet_name
                             )
        
        x = predictions.polar_area_frac_all[ data['Pattern Number'] ]
        y = data['HFE (kT)']
        
        ## PLOT DEVIATIONS
        ax = plot.plot_dev_indus( ax,
                                 x,
                                 y,
                                 x_label,
                                 y_label,
                                 marker,
                                 label
                                 )
        
        
        '''
        Import positive deviation points from pos_dev_indus
        All parameters stay the same except indus_file, label, marker
        '''
        sheet_name = 'pos_{}'.format( ligand )
        marker = '+'
        label = 'Positive deviation INDUS'
        
        ## READ EXCEL FILE
        data = pd.read_excel( indus_file, 
                             sheet_name
                             )
        
        x = predictions.polar_area_frac_all[ data['Pattern Number'] ]
        y = data['HFE (kT)']
        
        ax = plot.plot_dev_indus( ax,
                                 x,
                                 y,
                                 x_label,
                                 y_label,
                                 marker,
                                 label
                                 )
    
        ## ADJUST LEGEND ACCORDING TO NEED    
        ax.get_legend().remove()
    #    leg = ax.legend(fontsize = 14)
    #    leg.get_frame().set_edgecolor('k')
    #    leg.get_frame().set_linewidth(2)
        
        plot.plt.savefig('figures/20210602_max_dev_gpr_model_predictions_differentiated_{}_{:d}.png'.format( ligand, n_current ), 
                        bbox_inches = 'tight',
                        format = 'png'
                        )
    '''
    Parity plots of INDUS HFEs and GPR predictions with error bars
    '''
#    plot.parity_plot( predictions, 'indus', indus_mask )
#    plot.plt.gcf().subplots_adjust(bottom=0.15)
#    plot.plt.savefig('20210308_parity_plot_indus_gpr.png', bbox_inches = 'tight')
    
    '''
    Histogram unscaled sigma values
    '''
#    plot.histogram_unscaled_sigma( predictions.unscaled_sigma[ other_mask ], 300 )
#    plot.plt.savefig('20210205_sigma_histogram_300.png')
    
    '''
    Calculate Moran's I or Geary's C of all patterns
    '''
#    predictions.calc_moransI_gearyC( 'padded' )
#    plot.plot_by_num_polar_geary_C( predictions.patterns_all, predictions.unscaled_mu, predictions.autocorrelation )
#    plot.plt.savefig('20210205_gpr_predictions_gearyC.png', bbox_inches = 'tight' )
    
