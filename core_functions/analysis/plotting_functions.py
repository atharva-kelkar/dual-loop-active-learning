# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 06/01/2021

"""

'''
Plotting (and some support) functions for gpr_model_analysis.py
'''

import numpy as np, socket, sys, pandas as pd
## IMPORT MATPLOTLIB CORRECTLY
import matplotlib
if 'swarm' in socket.gethostname():
    matplotlib.use('Agg')
if 'swarm' not in socket.gethostname():
    sys.path.append(r"R:\\")
import matplotlib.pyplot as plt
## IMPORT GLOBAL PLOTTING FUNCTIONS
from analysis_scripts.python_scripts.plotting.plotting_functions import plot_parity_line
from analysis_scripts.python_scripts.gpr.core_functions.analysis.pattern_analysis import plot_linear_interpolation
## IMPORT FUNCTIONS FOR CALCULATION OF AUTOCORRELATION
from analysis_scripts.python_scripts.autocorrelation.spatial_autocorrelation.geary_c \
                                    import make_position_array,\
                                    flatten_array,\
                                    make_weight_matrix,\
                                    calc_geary_c,\
                                    calc_morans_i
from analysis_scripts.python_scripts.gpr.core_functions.gridding.gridding_functions import make_hex_grid, \
                                                                                make_padded_hex_grid

from sklearn.utils import shuffle

'''
Function to calculate RMSE given two arrays
'''
def calc_rmse( X, Y ):
    return np.sqrt( np.sum((X-Y)**2)/X.shape[0] )



'''
Function to plot mu as a function of mole fraction
'''
def plot_mu_vs_mf( predictions ):
    fig, ax = plt.subplots()
    
    ax.plot( np.sum(predictions.patterns_all, axis = 1 ), 
            predictions.unscaled_mu, 'o', 
            fillstyle = 'none', markersize = 12
            )
    ax.set_xlabel('Number of polar groups', fontsize = 15)
    ax.set_ylabel('HFE$_{GPR}$ (k$_B$T)', fontsize = 15)
    ax.set_xticks([0,2,4,6,8,10,12,14,16])
    ax.tick_params(axis='both', which='major', labelsize= 14)
    for axis in ['top','bottom','left','right']:
        ax.spines[axis].set_linewidth(2)



'''
Function to plot mu as a function of mole fraction but differentiated INDUS, CNN, and other points
'''
def plot_mu_vs_mf_by_type( x, y, indus_mask, cnn_mask, other_mask, x_label, plot_randomly = False, n = 10 ):
    fig, ax = plt.subplots()
    

    
    other_plot_x = x[ other_mask ]
    other_plot_y = y[ other_mask ]
    
    if plot_randomly:
        other_plot_x, other_plot_y = shuffle( other_plot_x, other_plot_y, random_state = 0 )
        other_plot_x = other_plot_x[::n]
        other_plot_y = other_plot_y[::n]
        
    ax.plot( other_plot_x, 
            other_plot_y, 'o', 
            fillstyle = 'none', 
            markersize = 12, markeredgecolor = 'blue', markeredgewidth = 3,
            label = 'Non-INDUS, non-CNN patterns'
            )
#    ax.plot( x[ cnn_mask ], 
#            y[ cnn_mask ], 's', 
#            fillstyle = 'none', 
#            markersize = 12, color = 'red', markeredgewidth = 3,
#            label = 'CNN patterns'
#            )
    ax.plot( x[ indus_mask ], 
            y[ indus_mask ], '^', 
            markersize = 12, color = 'green', markeredgewidth = 3,
            label = 'INDUS patterns'
            )
    print( np.sort(y[ indus_mask ])[0][:5] )
    ax.set_xlabel(x_label, fontsize = 15)
    ax.set_ylabel('HFE$_{GPR}$ (k$_B$T)', fontsize = 15)
    ax.set_xticks([0,2,4,6,8,10,12,14,16])
    ax.set_ylim([40,105])
    ax.tick_params(axis='both', which='major', labelsize= 14)
    for axis in ['top','bottom','left','right']:
        ax.spines[axis].set_linewidth(2)
    leg = ax.legend(fontsize = 14)
    leg.get_frame().set_edgecolor('k')
    leg.get_frame().set_linewidth(2)
    ax = plot_linear_interpolation( ax, 
                              y[ 0 ], 
                              y[ -1 ],
                              x.min(),
                              x.max()
                              )
    
    return fig, ax
    
    
'''
Function to plot parity plot between INDUS and CNN values with error bars
'''
def parity_plot( predictions, mask_type, mask ):
    hfe_values, _ = predictions.order_hfe_values( mask )
    
    gpr_predictions = predictions.unscaled_mu[ mask ]
    gpr_sigma = predictions.unscaled_sigma[ mask ]
    
    lower, upper = 35, 110
    fig, ax = plot_parity_line( lower, upper )
    
    ax.errorbar( hfe_values, gpr_predictions, yerr = gpr_sigma, 
                marker = 'o', linestyle = 'None', markersize = 12,
                capsize = 4, capthick = 2, barsabove = False, alpha = 0.7 )
    if mask_type == 'indus':
        ax.set_xlabel('HFE$_{INDUS}$ (k$_B$T)', fontsize = 15)
    elif mask_type == 'cnn':
        ax.set_xlabel('HFE$_{CNN}$ (k$_B$T)', fontsize = 15)
    
    ax.set_ylabel('HFE$_{GPR}$ (k$_B$T)', fontsize = 15)
    ax.set_xlim([ lower , upper ])
    ax.set_ylim([ lower , upper ])
    ax.tick_params(axis='both', which='major', labelsize= 14)
    for axis in ['top','bottom','left','right']:
        ax.spines[axis].set_linewidth(2)
    print(hfe_values.shape)
    print(gpr_predictions.shape)
    print('Correlation coefficient is {} and RMSE is {} kT'.format( np.corrcoef(
                                                            np.stack((hfe_values, 
                                                                     np.ndarray.flatten(gpr_predictions))))[0,1], 
                                                              calc_rmse( hfe_values[:, None], gpr_predictions) ),
                                                              ) 
#    leg = ax.legend(fontsize = 14)
#    leg.get_frame().set_edgecolor('k')
#    leg.get_frame().set_linewidth(2)

'''
Function to plot highest deviation INDUS points
'''
def plot_dev_indus( ax, x, y, x_label, y_label, marker, label, color = 'black' ):

    
    ## PLOT WITH GIVEN MARKER
    ax.plot( x,
            y,
            marker,
            color = color,
            markeredgewidth = 4, markersize = 12, zorder = 12,
            label = label
            )   
    
    return ax    



'''
Histogram unscaled sigma values
'''
def histogram_unscaled_sigma( unscaled_sigma, iter_number ):
    fig, ax = plt.subplots()
    
    ax.hist( x = unscaled_sigma, bins = 10, rwidth = 0.8 )
    ax.set_xlabel( 'Unscaled $\sigma$ from GPR model', fontsize = 15 )
    ax.set_ylabel( '# of patterns with given $\sigma$', fontsize = 15 )
    ax.set_title( 'Iteration number {}'.format( iter_number ), fontsize = 15 )
    ax.tick_params(axis='both', which='major', labelsize= 14)
    for axis in ['top','bottom','left','right']:
        ax.spines[axis].set_linewidth(2)
        

'''
Function to calculate autocorrelation (Geary's C, Moran's I, or adjusted Geary's C)
    Inputs:
        - patterns_all: Array of all patterns
        - geary_c_type: can be 'normal' or 'padded'
        - metric: 'moransI' or 'gearyC' or 'adjusted gearyC'
'''
def calc_autocorrelation( patterns_all, geary_c_type, metric ):
    
    autocorrelation = np.zeros( (patterns_all.shape[0] ,1) )        
    count = 0
    
    print('geary_c_type: {}, metric: {}'.format( geary_c_type, metric ) )
    
    for pattern in patterns_all:
        if geary_c_type == 'padded':
            num = int(pattern.shape[0]**(1/2)) + 2
            pattern_new = np.zeros( (num, num) )
            pattern_new[ 1:num-1, 1:num-1 ] = pattern.reshape( num - 2, num - 2 )
        elif geary_c_type == 'normal':
            num = int(pattern.shape[0]**(1/2))
        
        x_pos_array, y_pos_array = make_hex_grid( num, num, r = 1 )
        x_pos_array = flatten_array( x_pos_array )
        y_pos_array = flatten_array( y_pos_array )
        eps = 0.001
        weight_matrix = make_weight_matrix( x_pos_array, 
                                           y_pos_array, 
                                           'inverse_dist_squared', 
                                           cutoff = 1 + eps )
        if (metric == 'gearyC') | (metric == 'geary C') | \
            (metric == 'adjusted gearyC') | (metric == 'adjusted geary C'):
            autocorrelation[ count ] = calc_geary_c( pattern, weight_matrix )
        elif (metric == 'moransI') | (metric == 'morans I'):
            autocorrelation[ count ] = calc_morans_i( pattern, weight_matrix )
        else:
            print('geary_c_type: {}, metric: {}\n Some error here! Check inputs!'.format( geary_c_type, metric ) )
        
        if (metric == 'adjusted gearyC') | (metric == 'adjusted geary C'):
            autocorrelation[ count ] = 1 - autocorrelation[ count ]
            
        count += 1

    return autocorrelation



'''
Function to plot mu as a function of mole fraction but differentiated INDUS, CNN, and other points
'''
def plot_mu_vs_mf_by_type_subplots( ax, x, y, indus_mask, cnn_mask, other_mask, x_label ):
    
    ax.plot( x[ other_mask ], 
            y[ other_mask ], 'o', 
            fillstyle = 'none', 
            markersize = 8, markeredgecolor = 'blue', markeredgewidth = 3,
            label = 'Non-INDUS, non-CNN patterns'
            )
    ax.plot( x[ cnn_mask ], 
            y[ cnn_mask ], 's', 
            fillstyle = 'none', 
            markersize = 8, color = 'red', markeredgewidth = 3,
            label = 'CNN patterns'
            )
    ax.plot( x[ indus_mask ], 
            y[ indus_mask ], '^', 
            markersize = 8, color = 'green', markeredgewidth = 3,
            label = 'INDUS patterns'
            )
    print( np.sort(y[ indus_mask ])[0][:5] )
    ax.set_xlabel(x_label, 
                  fontsize = 10, 
                  fontname = 'Arial' 
                  )
    ax.set_ylabel('HFE$_{GPR}$ (k$_B$T)', 
                  fontsize = 10,
                  fontname = 'Arial'
                  )
    ax.set_xticks([0,2,4,6,8,10,12,14,16])
    ax.set_ylim([40,105])
    ax.tick_params(axis='both', 
                   which='major', 
                   labelsize= 10
#                   Family = 'Arial'
                   )
    
    for axis in ['top','bottom','left','right']:
        ax.spines[axis].set_linewidth(1)
#    leg = ax.legend(fontsize = 10)
#    leg.get_frame().set_edgecolor('k')
#    leg.get_frame().set_linewidth(1)
    ax = plot_linear_interpolation( ax, 
                              y[ 0 ], 
                              y[ -1 ],
                              x.min(),
                              x.max()
                              )
    
    return ax


'''
Function to plot envelope
'''

def plot_mu_vs_mf_by_type_envelope( x, y, envelope, indus_mask, cnn_mask, x_label, to_plot_cnn = True ):
    
    ## MAKE FIGURE
    fig, ax = plt.subplots()
    
    ## PLOT LOWER EDGE OF ENVELOPE
    ax.plot( envelope[:,0],
            envelope[:,1],
            '-',
            color = 'hotpink',
            linewidth = 2
            )
    
    ## PLOT UPPER EDGE OF ENVELOPE
    ax.plot( envelope[:,0],
            envelope[:,2],
            '-',
            color = 'hotpink',
            linewidth = 2
            )
    
    ax.fill_between( envelope[:,0],
                    envelope[:,1],
                    envelope[:,2],
                    color = 'pink',
                    alpha = 0.5
                    )
    
    ## PLOT CNN AND INDUS POINTS
    if to_plot_cnn:        
        ax.plot( x[ cnn_mask ], 
                y[ cnn_mask ], 'o', 
                fillstyle = 'none', 
                markersize = 10, color = 'deepskyblue', markeredgewidth = 3,
                label = 'CNN patterns'
                )
    ax.plot( x[ indus_mask ], 
            y[ indus_mask ], '^', 
            markersize = 12, color = 'silver', markeredgewidth = 3, #'seagreen' --> 'silver'
            label = 'INDUS patterns'
            )
 
    
    
    
    ## SET AND FORMAT X LABEL, Y LABEL, TICKS
    ax.set_xlabel(x_label, fontsize = 15)
    ax.set_ylabel('HFE$_{GPR}$ (k$_B$T)', fontsize = 15)
    ax.set_xticks([0,2,4,6,8,10,12,14,16])
    ax.set_ylim([40,105])
    ax.tick_params(axis='both', which='major', labelsize= 14)
    
    ## FORMAT OTHER ELEMENTS OF PLOT
    for axis in ['top','bottom','left','right']:
        ax.spines[axis].set_linewidth(2)
        
    ## SET AND FORMAT LEGEND
    leg = ax.legend(fontsize = 14)
    leg.get_frame().set_edgecolor('k')
    leg.get_frame().set_linewidth(2)
    
    ## PLOT LINEAR INTERPOLATION LINE
    ax = plot_linear_interpolation( ax, 
                              y[ 0 ], 
                              y[ -1 ],
                              x.min(),
                              x.max()
                              )
    
    return fig, ax


'''
'''
def plot_deviation( ax, ligand, polar_area_frac_all, plot_pos = True, plot_neg = True ):
    
    '''
    Import negative deviation points from neg_dev_indus
    '''
    indus_file = 'dev_hfe/dev_all_area_frac.xls'
    sheet_name = 'neg_{}'.format( ligand )
    x_label = 'Number of polar'
    y_label = 'HFE (kT)'
    marker = '_'
    label = 'Negative deviation INDUS'
    
    ## READ EXCEL FILE
    data = pd.read_excel( indus_file, 
                         sheet_name
                         )
    
    x = polar_area_frac_all[ data['Pattern Number'] ]
    y = data['HFE (kT)']
    
    if plot_neg:
        ## PLOT DEVIATIONS
        ax = plot_dev_indus( ax,
                            x,
                            y,
                            x_label,
                            y_label,
                            marker, 
                            label,
                            color = 'blue'
                            )
        
    
    '''
    Import positive deviation points from pos_dev_indus
    All parameters stay the same except indus_file, label, marker
    '''
    sheet_name = 'pos_{}'.format( ligand )
    marker = '+'
    label = 'Positive deviation INDUS'
    
    ## READ EXCEL FILE
    data = pd.read_excel( indus_file, 
                         sheet_name
                         )
    
    x = polar_area_frac_all[ data['Pattern Number'] ]
    y = data['HFE (kT)']
    
    if plot_pos:
        ax = plot_dev_indus( ax,
                            x,
                            y,
                            x_label,
                            y_label,
                            marker,
                            label,
                            color = 'tab:red', 
                            )

    ## ADJUST LEGEND ACCORDING TO NEED   
    try:
        ax.get_legend().remove()
    except:
        print('No legend found in the plot, one named Bradley Dallin found in the Van Lehn group')
#    leg = ax.legend(fontsize = 14)
#    leg.get_frame().set_edgecolor('k')
#    leg.get_frame().set_linewidth(2)

    return ax


'''
Function to plot envelope ONLY
'''

def plot_envelope( ax, envelope, x_label, label = '', edgecolor = 'hotpink', fillcolor = 'pink' ):
        
    ## PLOT LOWER EDGE OF ENVELOPE
    ax.plot( envelope[:,0],
            envelope[:,1],
            '-',
            color = edgecolor,
            linewidth = 2
            )
    
    ## PLOT UPPER EDGE OF ENVELOPE
    ax.plot( envelope[:,0],
            envelope[:,2],
            '-',
            color = edgecolor,
            linewidth = 2
            )
    
    ax.fill_between( envelope[:,0],
                    envelope[:,1],
                    envelope[:,2],
                    color = fillcolor,
                    alpha = 0.5,
                    label = label
                    )
    
    ## SET AND FORMAT X LABEL, Y LABEL, TICKS
    ax.set_xlabel(x_label, fontsize = 15)
    ax.set_ylabel('HFE$_{GPR}$ (k$_B$T)', fontsize = 15)
    ax.set_ylim([40,105])
    ax.tick_params(axis='both', which='major', labelsize= 14)
    
    ## FORMAT OTHER ELEMENTS OF PLOT
    for axis in ['top','bottom','left','right']:
        ax.spines[axis].set_linewidth(2)
        
    ## SET AND FORMAT LEGEND
    leg = ax.legend(fontsize = 14)
    leg.get_frame().set_edgecolor('k')
    leg.get_frame().set_linewidth(2)

    
    return ax



