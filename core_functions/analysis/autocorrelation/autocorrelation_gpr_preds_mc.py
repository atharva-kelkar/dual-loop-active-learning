# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 04/11/2021
"""

import numpy as np
import sys
sys.path.append('/home/kelkar2/analysis_scripts/python_scripts/gpr/')
from core_functions.choose_patterns_for_indus.choose_next_indus_pattern import make_pattern_array
from core_functions.analysis.gpr_model_analysis import Predictions
from core_functions.active_learning_functions.active_learning_functions import write_pattern_file
from core_functions.analysis.graph_connectivity.graph_connectivity_functions import calculate_adjacency_avg_connectivity, \
                                                                                    make_graphs_from_adjacency_list,\
                                                                                    find_connectivity_of_list,\
                                                                                    find_clustering,\
                                                                                    find_pearsons_r_given_num_polar,\
                                                                                    find_pearsons_r_given_num_polar_mc,\
                                                                                    make_numpy_array


if __name__ == "__main__":
    
    n_pred = 1500
    base_folder = '../../../active_learning_amide/'
    ## GPR prediction folder and file
    gpr_pred_folder = 'active_learning_patterns/'
    gpr_pred_file = 'gpr_prediction_iteration_{}.pickle'.format( n_pred )
    ## Master pattern folder and file
    master_hfe_folder = 'pattern_labels/'
    master_hfe_file = 'master_patterns_labels.pickle'
    ## Number of Monte Carlo steps
    n_mc = 100

    '''
    Instantiate an object of class Predictions
    '''
    predictions = Predictions( base_folder + gpr_pred_folder, 
                              gpr_pred_file 
                              )
    
    '''
    Load all GPR predictions and relevant mean and std, unscale mu and sigma gotten from patterns file
    '''
    predictions.load_predictions()
    predictions.load_mean_std( base_folder + master_hfe_folder, 
                              master_hfe_file, 
                              n_pred 
                              )
    predictions.unscale_mu_sigma()
    
    '''
    Load master HFE file
    '''
    predictions.load_master_hfe( base_folder + master_hfe_folder, 
                                master_hfe_file, 
                                n_pred
                                )
    
    '''
    Make INDUS and CNN mask, and then other mask based on INDUS and CNN masks
    '''
    predictions.split_indus_cnn( 2.0, 6.0 )
    indus_mask = predictions.make_mask( 'indus' )
    cnn_mask = predictions.make_mask( 'cnn' )
    not_indus_mask = ~( indus_mask )
    
    '''
    Calculate Geary's C and Pearson's R
    '''
    ## CALCULATE MORAN'S I
    predictions.calc_moransI_gearyC( 'normal', 'morans I')
    morans_i = predictions.autocorrelation
    ## CALCULATE ADJUSTED GEARY'S C
    predictions.calc_moransI_gearyC( 'normal', 'adjusted geary C')
    adj_geary_c = predictions.autocorrelation
    
    ## DEFINE BLANK RESULTS ARRAYS
    p_r_morans_i_mc = []
    p_r_adj_geary_c_mc = []
    
    for chosen_num_polar in np.unique( predictions.num_polar_all ):

        pearsons_r_morans_i_mc = find_pearsons_r_given_num_polar_mc( morans_i[:,0],
                                                               predictions.unscaled_mu[:,0],
                                                               predictions.unscaled_sigma[:,0],
                                                               predictions.num_polar_all,
                                                               chosen_num_polar,
                                                               n_mc
                                                               )
                
        pearsons_r_adj_geary_c_mc = find_pearsons_r_given_num_polar_mc( adj_geary_c[:,0],
                                                               predictions.unscaled_mu[:,0],
                                                               predictions.unscaled_sigma[:,0],
                                                               predictions.num_polar_all,
                                                               chosen_num_polar,
                                                               n_mc
                                                               )
        
        ## APPEND TO LIST
        p_r_morans_i_mc.append([ chosen_num_polar, 
                              np.sum( predictions.num_polar_all == chosen_num_polar ),
                              np.round( pearsons_r_morans_i_mc, 4 ) ])
        p_r_adj_geary_c_mc.append([ chosen_num_polar, 
                              np.sum( predictions.num_polar_all == chosen_num_polar ),
                              np.round( pearsons_r_adj_geary_c_mc, 4 ) ])
    
    ## FORMAT NUMBERS TO HAVE 3 DECIMAL PLACES
    np.set_printoptions(formatter={'float': lambda x: "{0:0.3f}".format(x)})

    p_r_morans_i_mc = np.array( p_r_morans_i_mc )
    p_r_adj_geary_c_mc = np.array( p_r_adj_geary_c_mc )
    
    ## SAVE PEARSONS R ARRAYS AS CSV FILES
    ligand = 'amide'
    type = 'all'
    padding = 'normal'
    
    p_r_morans_i_mc_comp = make_numpy_array( p_r_morans_i_mc )
    p_r_adj_geary_c_mc_comp = make_numpy_array( p_r_adj_geary_c_mc )
    
    np.savetxt('{}_{}_GPR_preds_{}_pearsonsR_{}_morans_i_MC.csv'.format( ligand, type, n_pred, padding ),
               p_r_morans_i_mc_comp, delimiter = ",", fmt = "%s"
               )
    np.savetxt('{}_{}_GPR_preds_{}_pearsonsR_{}_adj_geary_c_MC.csv'.format( ligand, type, n_pred, padding ),
               p_r_adj_geary_c_mc_comp, delimiter = ",", fmt = "%s"
               )
    
    
