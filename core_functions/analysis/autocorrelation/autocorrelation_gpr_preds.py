# -*- coding: utf-8 -*-
"""
@author: Atharva
@date: 04/11/2021
"""

import numpy as np
import sys
sys.path.append('/home/kelkar2/analysis_scrips/python_scripts/gpr/')
from choose_next_indus_pattern import make_pattern_array
from gpr_model_analysis import Predictions
from active_learning_functions import write_pattern_file
import networkx
from graph_connectivity_functions import calculate_adjacency_avg_connectivity, \
                                            make_graphs_from_adjacency_list,\
                                            find_connectivity_of_list,\
                                            find_clustering,\
                                            find_pearsons_r_given_num_polar


if __name__ == "__main__":
    
    n_pred = 1500
    base_folder = '../active_learning_amine/'
    ## GPR prediction folder and file
    gpr_pred_folder = 'active_learning_patterns/'
    gpr_pred_file = 'gpr_prediction_iteration_{}.pickle'.format( n_pred )
    ## Master pattern folder and file
    master_hfe_folder = 'pattern_labels/'
    master_hfe_file = 'master_patterns_labels.pickle'
    padding_type = 'normal'

    '''
    Instantiate an object of class Predictions
    '''
    predictions = Predictions( base_folder + gpr_pred_folder, 
                              gpr_pred_file 
                              )
    
    '''
    Load all GPR predictions and relevant mean and std, unscale mu and sigma gotten from patterns file
    '''
    predictions.load_predictions()
    predictions.load_mean_std( base_folder + master_hfe_folder, 
                              master_hfe_file, 
                              n_pred 
                              )
    predictions.unscale_mu_sigma()
    
    '''
    Load master HFE file
    '''
    predictions.load_master_hfe( base_folder + master_hfe_folder, 
                                master_hfe_file, 
                                n_pred
                                )
    
    '''
    Make INDUS and CNN mask, and then other mask based on INDUS and CNN masks
    '''
    predictions.split_indus_cnn( 2.0, 6.0 )
    indus_mask = predictions.make_mask( 'indus' )
    cnn_mask = predictions.make_mask( 'cnn' )
    not_indus_mask = ~( indus_mask )
    
    '''
    Calculate Geary's C and Pearson's R
    '''
    ## CALCULATE GEARY'S C
    predictions.calc_moransI_gearyC( padding_type, 'geary C')
    geary_c = predictions.autocorrelation
    ## CALCULATE MORAN'S I
    predictions.calc_moransI_gearyC( padding_type, 'morans I')
    morans_i = predictions.autocorrelation
    ## CALCULATE ADJUSTED GEARY'S C
    predictions.calc_moransI_gearyC( padding_type, 'adjusted geary C')
    adj_geary_c = predictions.autocorrelation
    
    ## DEFINE BLANK RESULTS ARRAYS
    p_r_geary_c = []
    p_r_morans_i = []
    p_r_adj_geary_c = []
    
    for chosen_num_polar in np.unique( predictions.num_polar_all ):
        pearsons_r_geary_c = find_pearsons_r_given_num_polar( geary_c[:,0],
                                                               predictions.unscaled_mu[:,0],
                                                               predictions.num_polar_all,
                                                               chosen_num_polar
                                                               )
        
        pearsons_r_morans_i = find_pearsons_r_given_num_polar( morans_i[:,0],
                                                               predictions.unscaled_mu[:,0],
                                                               predictions.num_polar_all,
                                                               chosen_num_polar
                                                               )
        
        pearsons_r_adj_geary_c = find_pearsons_r_given_num_polar( adj_geary_c[:,0],
                                                               predictions.unscaled_mu[:,0],
                                                               predictions.num_polar_all,
                                                               chosen_num_polar
                                                               )
        
        ## APPEND TO LIST
        p_r_geary_c.append([ chosen_num_polar, 
                              np.sum( predictions.num_polar_all == chosen_num_polar ),
                              np.round( pearsons_r_geary_c, 4 ) ])
        p_r_morans_i.append([ chosen_num_polar, 
                              np.sum( predictions.num_polar_all == chosen_num_polar ),
                              np.round( pearsons_r_morans_i, 4 ) ])
        p_r_adj_geary_c.append([ chosen_num_polar, 
                              np.sum( predictions.num_polar_all == chosen_num_polar ),
                              np.round( pearsons_r_adj_geary_c, 4 ) ])
    
    ## FORMAT NUMBERS TO HAVE 3 DECIMAL PLACES
    np.set_printoptions(formatter={'float': lambda x: "{0:0.3f}".format(x)})

    p_r_geary_c = np.array( p_r_geary_c )
    p_r_morans_i = np.array( p_r_morans_i )
    p_r_adj_geary_c = np.array( p_r_adj_geary_c )
    
    ## SAVE PEARSONS R ARRAYS AS CSV FILES
    ligand = 'amine'
    type = 'all'
    
    np.savetxt('{}_{}_GPR_preds_{}_pearsonsR_{}_geary_c.csv'.format( ligand, type, n_pred, padding_type ),
               p_r_geary_c, delimiter = ","
               )
    np.savetxt('{}_{}_GPR_preds_{}_pearsonsR_{}_morans_i.csv'.format( ligand, type, n_pred, padding_type ),
               p_r_morans_i, delimiter = ","
               )
    np.savetxt('{}_{}_GPR_preds_{}_pearsonsR_{}_adj_geary_c.csv'.format( ligand, type, n_pred, padding_type ),
               p_r_adj_geary_c, delimiter = ","
               )
    
    
