# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 05/04/2021

"""


PARAMETER_NAMES = ['node_conn_normal_0',
                   'node_conn_normal_1',
                   'node_conn_padded_0',
#                       'edge_conn', 
                   'avg_clust_normal_0',
                   'avg_clust_normal_1',
                   'avg_clust_padded_0',
                   'padded adj geary C', 
                   'padded morans I',
                   'adj geary C', 
                   'morans I',
                   ]


PARAMETER_NAMES_AREA_FRAC = ['node_conn_normal_0',
                             'node_conn_normal_1',
                             'node_conn_padded_0',
                             'avg_clust_normal_0',
                             'avg_clust_normal_1',
                             'avg_clust_padded_0',
                             'padded adj geary C', 
                             'padded morans I',
                             'adj geary C', 
                             'morans I',
                             'polar area frac'
                             ]