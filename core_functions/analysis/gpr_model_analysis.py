# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 01/25/2021

"""

import numpy as np, pandas as pd, sys

## IMPORT SUPPORT FUNCTIONS
from analysis_scripts.python_scripts.gpr.active_learning.make_hypercube import gen_all_patterns

from analysis_scripts.python_scripts.gpr.core_functions.analysis.pattern_analysis import set_nan_0, plot_by_num_polar_geary_C, plot_linear_interpolation

import analysis.plotting_functions as plot

        
class Predictions( ):
    
    def __init__( self, 
                 folder_name, 
                 file_name, 
                 ):
        self.folder_name = folder_name
        self.file_name = file_name
        self.n_dim = 16 # Change this if using bigger or smaller patterns
        
        
    '''
    Function to load predictions given folder name and file name
    '''
    def load_predictions( self ):
        self.predictions = np.load( self.folder_name + self.file_name, allow_pickle = True )
        self.mu = self.predictions[ 'mu' ]
        self.sigma = self.predictions[ 'sigma' ]
        self.patterns_all = gen_all_patterns( self.n_dim )
        self.num_polar_all = np.sum( self.patterns_all, axis = 1 )

    '''
    Function to calculate correlation between mole fraction and prediction mu
    '''
    def calc_mole_frac_corr( self ):
        return np.corrcoef( np.sum( self.patterns_all, axis = 1), 
                           np.ndarray.flatten( self.mu )
                           )[0,1]

    '''
    Function to load mean and standard deviation from master file 
    (of all predicted values on which GPR model is based)
    '''
    def load_mean_std( self, master_folder_name, master_file_name, n_current ):
        data = np.load( master_folder_name + master_file_name, allow_pickle = True )
        self.mean = data[ : n_current, 1].mean()
        self.std = data[ : n_current, 1].std()

    '''
    Function to unnormalize mu and sigma from GPR model
    '''
    def unscale_mu_sigma( self ):
        self.unscaled_mu = self.mu * self.std + self.mean
        self.unscaled_sigma = self.sigma * self.std
    
    @staticmethod
    def make_pattern_array( X ):
        a = [x[0] for x in X]
        return np.array( a )
    
    '''
    Function to load master predictions file with CNN or INDUS-calculated HFEs and relevant
    patterns and sigmas
    '''
    def load_master_hfe( self, master_folder_name, master_file_name, n_current ):
        data = np.load( master_folder_name + master_file_name, allow_pickle = True )
        data = data[ :n_current ]
        self.master_patterns = self.make_pattern_array( data )
        self.master_labels = data[:, 1]
        self.master_sigma = data[:, 2]
    
    '''
    Function to split arrays into INDUS and CNN HFEs
    '''
    def split_indus_cnn( self, indus_sigma, cnn_sigma ):
        ## [:-1] TO SELECT ALL ELEMENTS EXCEPT LAST ONE
        ## RATIONALE FOR NOT SELECTING ELEMENT -
        ##      LAST ELEMENT IS FROM INDUS RUN AFTER PREDICTIONS, HENCE NOT INCORPORATED
        self.master_patterns_indus = self.master_patterns[ self.master_sigma == indus_sigma ][:-1]
        self.master_labels_indus = self.master_labels[ self.master_sigma == indus_sigma ][:-1]
        self.master_sigma_indus = self.master_sigma[ self.master_sigma == indus_sigma ][:-1]
        
        self.master_patterns_cnn = self.master_patterns[ self.master_sigma == cnn_sigma ]
        self.master_labels_cnn = self.master_labels[ self.master_sigma == cnn_sigma ]
        self.master_sigma_cnn = self.master_sigma[ self.master_sigma == cnn_sigma ]
    
    '''
    Function to create mask for INDUS or CNN patterns
    '''
    def make_mask( self, what_to_mask ):
        if what_to_mask == 'indus':
            array = self.master_patterns_indus
        elif what_to_mask == 'cnn':
            array = self.master_patterns_cnn
        else:
            print("***** Please input either 'indus' or 'cnn' *****")
        
        mask_array = np.repeat( False , self.patterns_all.shape[0] )
        for pattern in array:
            mask_array = np.any( np.stack((mask_array, 
                                          (self.patterns_all == pattern).all( axis = 1 )),
                                          axis = -1), 
                        axis = 1 )
        
        return mask_array

    '''
    Order HFE values given a mask
    '''
    def order_hfe_values( self, mask ):
        curr_patterns = self.patterns_all[ mask ] # Patterns under consideration
        hfe_pred = np.zeros( curr_patterns.shape[0] )
        hfe_sigma = np.zeros( curr_patterns.shape[0] )
        
        count = 0
        for pattern in curr_patterns:
            curr_mask = ( self.master_patterns == pattern ).all( axis = 1 )
            curr_hfe = self.master_labels[ curr_mask ]
            curr_sigma = self.master_sigma[ curr_mask ]
            hfe_pred[ count ] = curr_hfe
            hfe_sigma[ count ] = curr_sigma
            count += 1
        return hfe_pred, hfe_sigma
    
    '''
    Function to make data_array, a writeable array that can be used to read GPR predictions in an Excel file
        Inputs:
            - indus_mask: 1 if INDUS calculated, 0 if not
            - cnn_mask: 1 if CNN calculated, 0 if not
    '''
    def make_data_array( self, indus_mask, cnn_mask ):
    
        self.data_array = np.concatenate((self.num_polar_all[:,None],
                                                 self.unscaled_mu,
                                                 self.unscaled_sigma,
                                                 indus_mask[:,None],
                                                 cnn_mask[:,None]),
                                            axis = -1
                                            )
    
    
    '''
    Calculate Geary's C of every n_dim pattern 
        - Inputs:
                - geary_c_type: can be 'normal' or 'padded'
                - metric: 'moransI' or 'gearyC' or 'adjusted gearyC'

    '''
    
    def calc_moransI_gearyC( self, geary_c_type, metric ):
        self.autocorrelation = set_nan_0( plot.calc_autocorrelation( self.patterns_all,
                                                                   geary_c_type,
                                                                   metric
                                                                   ) )
        

            
            
if __name__ == "__main__":
    
    '''
    Define folder and file name
    '''
    n_current = int(sys.argv[1])
    ligand = 'amide'
    base_dir = '../../active_learning_amide/'
    folder_name = base_dir + 'active_learning_patterns/'
    file_name = 'gpr_prediction_iteration_{:d}.pickle'.format( n_current )
    
    '''
    Define master folder and master file name
    '''
    master_folder_name = base_dir +  'pattern_labels/'
    master_file_name = 'master_patterns_labels.pickle'
    
    '''
    Create object of Predictions class
    '''
    predictions = Predictions( folder_name , file_name )
    
    '''
    Load all predictions, and mean and std
    '''
    predictions.load_predictions()
    predictions.load_mean_std( master_folder_name, master_file_name, n_current )
    
    '''
    Calculate correlation between mole fraction and HFE prediction
    '''
    mole_fraction_corr = predictions.calc_mole_frac_corr()
    predictions.unscale_mu_sigma()
    
    '''
    Plot GPR model mu as a function of mole fraction
    '''
#    plot_mu_vs_mf( predictions )
    
    '''
    Load master file HFEs and patterns
    '''
    predictions.load_master_hfe( master_folder_name, master_file_name, n_current )
    
    '''
    Split master file HFEs and patterns into INDUS and CNN values
    '''
    predictions.split_indus_cnn( 2.0, 6.0 ) # 2.0 --> INDUS sigma, 6.0 --> CNN sigma
    
    '''
    Make mask for INDUS and CNN patterns among all patterns respectively
    '''
    indus_mask = predictions.make_mask( 'indus' )
    cnn_mask = predictions.make_mask( 'cnn' )
    other_mask = ~(indus_mask | cnn_mask)

    '''
    Make data_array to process data into readable format
    '''
    num_polar = np.sum( predictions.patterns_all, axis = 1 )
    predictions.make_data_array( indus_mask, 
                                cnn_mask 
                                )
    
    ## SAVE DATA_ARRAY IN CSV FILE
#    np.savetxt('hydrox_1500_gpr_pred.csv', predictions.data_array, 
#               delimiter = ","
#               )
    
    '''
    Plot GPR model HFE predictions as a function of number of polar groups
    Differentiate surfaces with and without INDUS and CNN predictions
    '''
    fig, ax = plot.plot_mu_vs_mf_by_type( predictions.num_polar_all,
                                         predictions.unscaled_mu, 
                                         indus_mask, 
                                         cnn_mask, 
                                         other_mask,
                                         'Number of polar groups'
                                         )
#    plot.plt.savefig('20210414_gpr_model_predictions_differentiated_{:d}.png'.format(n_current), 
#                bbox_inches = 'tight',
#                format = 'png'
#                )

    to_plot_dev = True
    
    if to_plot_dev:
        '''
        Import negative deviation points from neg_dev_indus
        '''
        indus_file = 'dev_hfe/dev_all.xls'
        sheet_name = 'neg_{}'.format( ligand )
        x_label = 'Number of polar'
        y_label = 'HFE (kT)'
        marker = 'x'
        label = 'Negative deviation INDUS'
        
        ## READ EXCEL FILE
        data = pd.read_excel( indus_file, 
                             sheet_name
                             )
        
        x = data['Number of polar']
        y = data['HFE (kT)']
        ax = plot.plot_dev_indus( ax,
                                x, 
                                y,
                                x_label,
                                y_label,
                                marker,
                                label
                                )
        
        
        '''
        Import positive deviation points from pos_dev_indus
        All parameters stay the same except indus_file, label, marker
        '''
        sheet_name = 'pos_{}'.format( ligand )
        marker = '+'
        label = 'Positive deviation INDUS'
        
        ## READ EXCEL FILE
        data = pd.read_excel( indus_file, 
                             sheet_name
                             )
        
        x = data['Number of polar']
        y = data['HFE (kT)']
        ax = plot.plot_dev_indus( ax,
                                x, 
                                y,
                                x_label,
                                y_label,
                                marker,
                                label
                                )
    
        ## ADJUST LEGEND ACCORDING TO NEED    
        ax.get_legend().remove()
    #    leg = ax.legend(fontsize = 14)
    #    leg.get_frame().set_edgecolor('k')
    #    leg.get_frame().set_linewidth(2)
        
        plot.plt.savefig('figures/20210602_max_dev_gpr_model_predictions_num_polar_differentiated_{}_{:d}.png'.format(ligand,n_current), 
                        bbox_inches = 'tight',
                        format = 'png'
                        )
    '''
    Parity plots of INDUS HFEs and GPR predictions with error bars
    '''
#    plot.parity_plot( predictions, 'indus', indus_mask )
#    plot.plt.gcf().subplots_adjust(bottom=0.15)
#    plot.plt.savefig('20210308_parity_plot_indus_gpr.png', bbox_inches = 'tight')
    
    '''
    Histogram unscaled sigma values
    '''
#    plot.histogram_unscaled_sigma( predictions.unscaled_sigma[ other_mask ], 300 )
#    plot.plt.savefig('20210205_sigma_histogram_300.png')
    
    '''
    Calculate Moran's I or Geary's C of all patterns
    '''
#    predictions.calc_moransI_gearyC( 'padded' )
#    plot.plot_by_num_polar_geary_C( predictions.patterns_all, predictions.unscaled_mu, predictions.autocorrelation )
#    plot.plt.savefig('20210205_gpr_predictions_gearyC.png', bbox_inches = 'tight' )
    
