# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 07/04/2021

"""

import numpy as np



if __name__ == "__main__":
    
    ## DEFINE FILE NAME
    ligand = 'hydrox'
    file_name = '{}_1500/pattern_order_parameters_{}_1500_area_frac.pickle'.format( ligand, ligand )
    metric = 'avg_clust_normal_0'
    
    ## LOAD DATA
    data = np.load( file_name, 
                   allow_pickle = True
                   )
    
    ## SAVE METRIC AS TXT FILE
    np.savetxt( '{}.csv'.format( metric ),
               data[ metric ],
               delimiter = ","
               )
    


