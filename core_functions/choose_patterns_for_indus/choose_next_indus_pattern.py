# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 02/01/2021

"""

'''
03/05/2021 - Changed [f(x) - linear_interpolation] to [linear_interpolation - f(x)] 
'''

import numpy as np


def make_pattern_array( X ):
    array = [x for x in X]
    return np.array( array )

'''
Function to define linear interpolation based on number of polar groups and HFE values of extremes
'''
def make_linear_interpolation( num_polar_groups, n_dim, hfe_nonpolar, hfe_polar ):
    return ( num_polar_groups * hfe_polar + ( n_dim - num_polar_groups ) * hfe_nonpolar )/n_dim

if __name__ == "__main__":
    
    testing = True
    
    if testing:
        patterns_file = 'pattern_labels/master_patterns_labels.pickle'
        check_patterns = 50
        n_dim = 16
    
    '''
    Load master patterns file
    '''
    data_all = np.load( patterns_file )
    
    '''
    Load nonpolar and polar extremes
    '''
    hfe_nonpolar = data_all[ 50 ][1]
    hfe_polar = data_all[ 51 ][1]
    
    '''
    Filter only relevant part of array
    '''
    data = data_all[ - check_patterns : ]
    
    '''
    Make sum of patterns array
    '''
    patterns = make_pattern_array( data[:,0] )
    num_polar_groups = np.sum( patterns, axis = 1 )
    
    '''
    Make linearly interpolated HFE array
    '''
    linear_interpolated_hfe = make_linear_interpolation( num_polar_groups, 
                                                        n_dim,
                                                        hfe_nonpolar,
                                                        hfe_polar
                                                        )
    
    '''
    Calculate difference between linearly interpolated HFE and actual HFE
    '''
    delta_linear_cnn = -( data[:, 1] - linear_interpolated_hfe )
    
    '''
    Print position of maximum delta
    '''
    print( np.where( data_all[:, 1] == 
                    data[ np.where( delta_linear_cnn == delta_linear_cnn.max() ) ][0,1] ) )
    
    
    
    
    
    
