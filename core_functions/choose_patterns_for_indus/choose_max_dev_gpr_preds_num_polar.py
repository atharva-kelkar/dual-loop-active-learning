# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 04/22/2021

"""

import numpy as np
import sys
sys.path.append('/home/kelkar2/analysis_scripts/python_scripts/gpr/core_functions/')
from choose_patterns_for_indus.choose_next_indus_pattern import make_pattern_array
from analysis.gpr_model_analysis import Predictions
from active_learning_functions.active_learning_functions import write_pattern_file
from choose_patterns_for_indus.choose_lowest_n_gpr_preds import make_linear_interpolation


'''
Class that chooses max deviation patterns from given set

    Inputs:
        - folder_name: Name of folder containing GPR predictions
        - file_name: Name of file containing GPR predictions
        - master_folder_name: Name of folder containing master HFE predictions pickle
        - master_file_name: Name of file of master HFE predictions pickle
        - n_current: Number of current prediction (Usually 1500 - last index of GPR predictions)
        - voronoi_areas_pickle: Pickle containing mapped Voronoi areas from Voronoi calculations
        - bin_width: Width of area fraction histogram
        - n_choose: Number of patterns with deviation to choose 
        - num_threshold: Minimum number of patterns in histogram bin to choose bin
        - quantify_dev: 'positive' or 'negative' depending on if you want to select patterns with max positive or negative dev

'''
class Choose_MaxDev( Predictions ):
    
    '''
    __init__ constructor
    '''
    def __init__( self,
                 folder_name, 
                 file_name, 
                 master_folder_name, 
                 master_file_name,
                 n_current,
                 ligand,
                 n_choose = 10,
                 quantify_dev = 'positive',
                 choose_num_polar = False,
                 num_polar = 8
                 ):
        ## INITIATE PARENT CLASS
        Predictions.__init__( self, 
                             folder_name,
                             file_name
                             )
        ## LOAD PREDICTIONS
        self.load_predictions()
        ## LOAD MASTER MEAN AND STD
        self.load_mean_std( master_folder_name, master_file_name, n_current )
        ## UNSCALE MU AND SIGMA VALUES
        self.unscale_mu_sigma()        

        ## LOAD MASTER HFE SINCE NOT LOADED BEFORE
        self.load_master_hfe( master_folder_name, 
                             master_file_name, 
                             n_current 
                             )
        self.split_indus_cnn( 2.0, 6.0 ) # indus_sigma = 2.0, cnn_sigma = 6.0
        
        
        ## ALLOCATE VARIABLES
        self.ligand = ligand
        self.n_choose = n_choose
        self.quantify_dev = quantify_dev
        self.choose_num_polar = choose_num_polar
        self.num_polar = num_polar
        
    
    '''
    Function to make INDUS and non-INDUS mask
    '''
    def make_masks( self, choose_num_polar = False ):
        self.indus_mask = self.make_mask( 'indus' )
        self.not_indus_mask = ~( self.indus_mask )
        
        if choose_num_polar:
            self.not_indus_mask = self.not_indus_mask * (self.num_polar_all == self.num_polar)
            
            
    '''
    Function to assign pure surface HFEs (typically indices 50 and 51)
    '''
    def assign_pure_hfe( self ):
        self.hfe_nonpolar = self.master_labels[ 50 ] # Nonpolar is 50th pattern
        self.hfe_polar = self.master_labels[ 51 ] # Polar is 51st pattern

    '''
    Static function to calculate linear interpolation HFEs
    '''
    @staticmethod
    def make_linear_interpolation( polar_frac, nonpolar_hfe, polar_hfe ):
        ## SELF-EXPLANATORY
        return polar_frac * polar_hfe + ( 1 - polar_frac ) * nonpolar_hfe

    '''
    Function to calculate deviation based on quantify_dev
    '''
    def calc_deviation( self ):
        if self.quantify_dev == 'positive':
            flag = 1
        elif self.quantify_dev == 'negative':
            flag = -1
        else:
            print("*** Please input 'positive' or 'negative' ***")
        
        self.deviation = flag * (self.unscaled_mu - self.linear_interpolation[ :, None ])

    '''
    Static method to sort deviation array
    '''
    @staticmethod
    def sort_deviation_array( array_to_sort_with, array_to_sort ):
        
        sorted_indices = array_to_sort_with[:,0].argsort()
        array_to_sort_sorted = array_to_sort[ sorted_indices[::-1], 0 ]
        array_to_sort_with_sorted = array_to_sort_with[ sorted_indices[::-1], 0 ]
    
        return array_to_sort_sorted, array_to_sort_with_sorted
    
    
    '''
    Function to write pattern files
    '''
    def write_pattern_files( self ):
        for chosen_pattern_number in self.chosen_pattern_num:
            folder_name = 'deviation_patterns/{}_deviation_indus/'.format( quantify_dev )
            
            if self.choose_num_polar:
                file_name   = '{}_{}_dev_{}_num_polar_{}.dat'.format( self.ligand, 
                                                                     self.quantify_dev, 
                                                                     chosen_pattern_number, 
                                                                     self.num_polar
                                                                     )
            else:
                file_name   = '{}_{}_dev_{}.dat'.format( self.ligand, self.quantify_dev, chosen_pattern_number )
                
            write_pattern_file( self.patterns_all[ chosen_pattern_number ],
                               8,
                               folder_name,
                               file_name
                               )
    
    
    '''
    Compute function
    '''
    def compute_max_dev( self, out_file ):
        ## MAKE INDUS AND NOT-INDUS MASK
        self.make_masks( choose_num_polar = self.choose_num_polar )
        ## ASSIGN PURE HFE VALUES
        self.assign_pure_hfe()
        ## MAKE LINEAR INTERPOLATION BASED ON POLAR AREA FRAC
        self.linear_interpolation = self.make_linear_interpolation( self.num_polar_all,
                                                                   self.hfe_nonpolar,
                                                                   self.hfe_polar
                                                                   )
        ## CALCULATE DEVIATION
        self.calc_deviation()
        ## MAKE PATTERN_NUM ARRAY
        self.pattern_num = np.arange( self.deviation.shape[0] )[ :, None ]
        ## MAKE NOT-INDUS ARRAYS TO CHOOSE PATTERNS FOR WHICH INDUS HAS NOT BEEN PERFORMED YET
        self.not_indus_deviation = self.deviation[ self.not_indus_mask ]
        self.not_indus_pattern_num = self.pattern_num[ self.not_indus_mask ]
        ## SORT DEVIATION ARRAYS
        self.pattern_num_sorted, self.deviation_sorted = self.sort_deviation_array( self.not_indus_deviation,
                                                                                   self.not_indus_pattern_num
                                                                                   )
        ## CHOOSE N_CHOOSE NUMBER OF PATTERNS
        self.chosen_pattern_num = self.pattern_num_sorted[ : self.n_choose ]
        ## WRITE PATTERN NUMBERS TO TEXT FILE
        np.savetxt( out_file,
                   self.chosen_pattern_num,
                   fmt = '%d'
                   )
        ## WRITE ALL PATTERNS AS INPUT FILES FOR MD SIMULATION
        self.write_master_function()
        
        


if __name__ == "__main__":
    
    testing = True
    
    if testing:
        n_current = 1500
        bin_width = 0.04
    else:
        n_current = int( sys.argv[1] )
        bin_width = float( sys.argv[2] )
    ## DEFINE FILE AND FOLDER NAMES
    ligand = 'amide'
    base_dir = '../../active_learning_amide/'
    folder_name = base_dir + 'active_learning_patterns/'
    file_name = 'gpr_prediction_iteration_{:d}.pickle'.format( n_current )
    
    ## DEFINE MASTER FOLDER AND MASTER FILE NAME
    master_folder_name = base_dir +  'pattern_labels/'
    master_file_name = 'master_patterns_labels.pickle'
    
    ## DEFINE PARAMETERS FOR NUMBER OF PATTERNS TO CHOOSE ETC.
    n_choose = 5 # Choose n_choose patterns with highest -ve dev. Can easily modify code to choose +ve dev
    quantify_dev = 'negative' # positive or negative
    threshold_num_patterns = 4000
    
    ## WHETHER TO CHOOSE FROM A SINGLE NUMBER OF POLAR GROUPS OR NOT
    choose_given_num_polar = True
    num_polar = 4
    
    ## DEFINE OUTPUT FILE NAME
    if not choose_given_num_polar:
        out_file = '{}_{}_most_{}_dev_patterns.txt'.format( ligand, n_choose, quantify_dev )
    else:
        out_file = '{}_{}_most_{}_dev_patterns_num_polar_{}.txt'.format( ligand, n_choose, quantify_dev, num_polar )
        
        
    ## CREATE OBJECTS OF HISTOGRAM_AREAFRAC CLASS
    choose_max_dev = Choose_MaxDev( folder_name,
                                   file_name, 
                                   master_folder_name, 
                                   master_file_name, 
                                   n_current,
                                   ligand,
                                   n_choose = n_choose,
                                   quantify_dev = quantify_dev,
                                   choose_num_polar = choose_given_num_polar,
                                   num_polar = num_polar
                                   )
    
    ## RUN COMPUTE_MAX_DEV FUNCTION
    choose_max_dev.compute_max_dev( out_file )
        
    
    
    
    
    
    
    
    
