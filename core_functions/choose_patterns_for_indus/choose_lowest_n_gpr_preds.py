# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 04/22/2021

"""

import numpy as np
import sys
sys.path.append('/home/kelkar2/analysis_scrips/python_scripts/gpr/core_functions/')
from gpr.core_functions.choose_patterns_for_indus.choose_next_indus_pattern import make_pattern_array
from gpr.core_functions.analysis.gpr_model_analysis import Predictions
from gpr.core_functions.active_learning_functions.active_learning_functions import write_pattern_file


'''
Function to make linear interpolation based on polar and nonpolar HFE values
'''
def make_linear_interpolation( patterns_all, hfe_nonpolar, hfe_polar, n_dim ):
    ## Make linear interpolation
    linear_interpolation = np.sum( patterns_all, axis = 1 ) * hfe_polar + \
                            (n_dim - np.sum( patterns_all, axis = 1 )) * hfe_nonpolar
    ## Divide by n_dim to adjust to correct value
    linear_interpolation = linear_interpolation / n_dim
    
    return linear_interpolation

if __name__ == "__main__":
    
    n_pred = 1500
    base_folder = '../../active_learning_run3/'
    ligand = 'hydrox'
    ## GPR prediction folder and file
    gpr_pred_folder = 'active_learning_patterns/'
    gpr_pred_file = 'gpr_prediction_iteration_{}.pickle'.format( n_pred )
    ## Master pattern folder and file
    master_hfe_folder = 'pattern_labels/'
    master_hfe_file = 'master_patterns_labels.pickle'
    n_choose = 1 # Choose n_choose patterns with highest -ve dev. Can easily modify code to choose +ve dev
    quantify_dev = 'negative' # positive or negative
    choose_given_num_polar = True
    num_polar = 11
    if not choose_given_num_polar:
        out_file = '{}_{}_most_{}_dev_patterns.txt'.format( ligand, n_choose, quantify_dev )
    else:
        out_file = '{}_{}_most_{}_dev_patterns_num_polar_{}.txt'.format( ligand, n_choose, quantify_dev, num_polar )

    '''
    Instantiate an object of class Predictions
    '''
    predictions = Predictions( base_folder + gpr_pred_folder, 
                              gpr_pred_file 
                              )
    
    '''
    Load all GPR predictions and relevant mean and std, unscale mu and sigma gotten from patterns file
    '''
    predictions.load_predictions()
    predictions.load_mean_std( base_folder + master_hfe_folder, 
                              master_hfe_file, 
                              n_pred 
                              )
    predictions.unscale_mu_sigma()
    
    '''
    Load master HFE file
    '''
    predictions.load_master_hfe( base_folder + master_hfe_folder, 
                                master_hfe_file, 
                                n_pred
                                )
    
    '''
    Make INDUS and CNN mask, and then other mask based on INDUS and CNN masks
    '''
    predictions.split_indus_cnn( 2.0, 6.0 )
    indus_mask = predictions.make_mask( 'indus' )
    cnn_mask = predictions.make_mask( 'cnn' )
    not_indus_mask = ~( indus_mask )
    
    '''
    Make linear interpolation based on nonpolar and polar values
    '''
    hfe_nonpolar = predictions.master_labels[ 50 ] # Nonpolar is 50th pattern
    hfe_polar = predictions.master_labels[ 51 ] # Polar is 51st pattern
    linear_interpolation = make_linear_interpolation( predictions.patterns_all,
                                                     hfe_nonpolar,
                                                     hfe_polar,
                                                     predictions.n_dim
                                                     )
    
    '''
    Measure deviations from linearity
    '''
    if quantify_dev == 'negative':
        deviation = linear_interpolation[:, None] - predictions.unscaled_mu
    elif quantify_dev == 'positive':
        deviation = predictions.unscaled_mu - linear_interpolation[:, None] 
    
    '''
    Make pattern numbers array
    '''
    pattern_num = np.arange( deviation.shape[0] )[:, None]
    
    '''
    Make not_indus_mask to also choose given num_polar, if choose_given_num_polar flag is True
    '''
    if choose_given_num_polar:
        not_indus_mask = not_indus_mask * (predictions.num_polar_all == num_polar)
    
    '''
    Only choose "not_indus_mask" entries to not repeat INDUS calculations 
    '''
    deviation = deviation[ not_indus_mask ]
    pattern_num = pattern_num[ not_indus_mask ]
    
    
    '''
    Sort pattern_num based on deviation
    '''
    deviation_sorted_indices = deviation[:, 0].argsort()
    pattern_num_sorted = pattern_num[ deviation_sorted_indices[::-1], 0 ]
    deviation_sorted = deviation[ deviation_sorted_indices[::-1], 0 ]
    
    '''
    Select n_choose patterns from sorted patterns array
    '''
    chosen_pattern_numbers = pattern_num_sorted[ : n_choose ]
    
    '''
    Save patterns in out_file
    '''
    np.savetxt( out_file,
               chosen_pattern_numbers,
               fmt = '%d'
               )
    
    '''
    Write pattern files from chosen patterns
    '''
    for chosen_pattern_number in chosen_pattern_numbers:
        folder_name = 'deviation_lists/{}_deviation_indus/'.format( quantify_dev )

        ## DEFINE FILE NAME BASED ON WHETHER NUMBER OF POLAR GROUPS ARE TO BE CHOSEN
        if choose_given_num_polar:
            file_name   = '{}_{}_dev_{}_num_polar_{}.dat'.format( ligand, 
                                                                   quantify_dev, 
                                                                   chosen_pattern_number, 
                                                                   num_polar
                                                                   )
        else:
            file_name   = '{}_{}_dev_{}.dat'.format( ligand, quantify_dev, chosen_pattern_number )
            
        write_pattern_file( predictions.patterns_all[ chosen_pattern_number ],
                           8,
                           folder_name,
                           file_name
                           )
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
