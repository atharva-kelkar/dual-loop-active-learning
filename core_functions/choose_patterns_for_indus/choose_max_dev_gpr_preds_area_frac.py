# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 04/22/2021

"""

import numpy as np
import sys
sys.path.append('/home/kelkar2/analysis_scripts/python_scripts/gpr/core_functions/')
from choose_patterns_for_indus.choose_next_indus_pattern import make_pattern_array
from analysis.histogram_gpr_preds_per_area_frac import Histogram_AreaFrac
from active_learning_functions.active_learning_functions import write_pattern_file
from choose_patterns_for_indus.choose_lowest_n_gpr_preds import make_linear_interpolation
from choose_patterns_for_indus.choose_max_dev_gpr_preds_num_polar import Choose_MaxDev

'''
Class that chooses max deviation patterns from given set

    Inputs:
        - folder_name: Name of folder containing GPR predictions
        - file_name: Name of file containing GPR predictions
        - master_folder_name: Name of folder containing master HFE predictions pickle
        - master_file_name: Name of file of master HFE predictions pickle
        - n_current: Number of current prediction (Usually 1500 - last index of GPR predictions)
        - voronoi_areas_pickle: Pickle containing mapped Voronoi areas from Voronoi calculations
        - bin_width: Width of area fraction histogram
        - n_choose: Number of patterns with deviation to choose 
        - num_threshold: Minimum number of patterns in histogram bin to choose bin
        - quantify_dev: 'positive' or 'negative' depending on if you want to select patterns with max positive or negative dev

'''
class Choose_MaxDev_AreaFrac( Histogram_AreaFrac, Choose_MaxDev ):
    
    '''
    __init__ constructor
    '''
    def __init__( self,
                 folder_name, 
                 file_name, 
                 master_folder_name, 
                 master_file_name,
                 n_current,
                 ligand,
                 voronoi_areas_pickle = '../gridding/average_voronoi_areas.pkl',
                 bin_width = 0.04,
                 n_choose = 10,
                 num_threshold = 4000,
                 quantify_dev = 'positive'
                 ):
        
        ## INITIALIZE PARENT CLASSES
        Histogram_AreaFrac.__init__( self, 
                                    folder_name,
                                    file_name,
                                    master_folder_name,
                                    master_file_name,
                                    n_current,
                                    voronoi_areas_pickle = voronoi_areas_pickle,
                                    bin_width = bin_width
                                    )
        
        ## RUN COMPUTE FUNCTION OF HISTOGRAM CLASS
        self.compute_hist()
        
        Choose_MaxDev.__init__( self,
                               folder_name,
                               file_name, 
                               master_folder_name, 
                               master_file_name,
                               n_current,
                               ligand,
                               n_choose = n_choose,
                               quantify_dev = quantify_dev,
                               )
        
        ## ALLOCATE REMAINING VARIABLES
        self.num_threshold = num_threshold
        
    '''
    Method to allocate bins to all polar area fraction points
    '''
    @staticmethod
    def allocate_bins( polar_area_frac_all, bins ):
        ## MAKE ARRAY OF ZEROS
        area_frac_bins = np.zeros( polar_area_frac_all.shape[0] )
        
        for count in range( len(bins) - 1 ):
            ## ALLOCATE BIN NUMBER COUNT TO POLAR AREA FRAC WITHIN BIN ENDS [ lower, upper )    
            area_frac_bins[ (polar_area_frac_all >= bins[count]) * (polar_area_frac_all < bins[count+1]) ] = count
        
        return area_frac_bins
    
    '''
    Static method to return bin numbers for which number of samples is above threshold number
    '''
    @staticmethod
    def select_bins_above_threshold( histogram, num_threshold ):
        return np.arange( histogram.shape[0] )[ histogram >= num_threshold ]
    
    '''
    Static method to make mask for given bin number and not in indus
    '''
    @staticmethod
    def make_bin_mask( indus_mask, bin_array, num_bin ):
        not_indus_mask = ~( indus_mask ) 
        num_bin_mask = bin_array == num_bin
        
        return not_indus_mask * num_bin_mask
        
    
    '''
    Method to iterate over chosen bins and choose n_choose patterns from each of those bins
    '''
    def choose_n_patterns_per_bin( self ):
        
        chosen_patterns = []
        
        for num_bin in self.bins_chosen:
            
            ## MAKE MASK WITH BIN NUMBER AND NOT INDUS MASK
            mask = self.make_bin_mask( self.indus_mask,
                                      self.area_frac_bins,
                                      num_bin 
                                      )
            ## CHOOSE PATTERNS AND DEVIATIONS BASED ON MASK
            deviation_chosen = self.deviation[ mask ]
            pattern_num_chosen = self.pattern_num[ mask ]
            ## SORT DEVIATION ARRAYS
            pattern_num_sorted, deviation_sorted = self.sort_deviation_array( deviation_chosen,
                                                                             pattern_num_chosen
                                                                             )
            
            ## CHOOSE N_CHOOSE NUMBER OF PATTERNS
            chosen_pattern_num = pattern_num_sorted[ : self.n_choose ]
            
            ## APPEND TO ARRAY OF CHOSEN PATTERNS
            chosen_patterns.append( [ num_bin, 
                                     chosen_pattern_num, 
                                     self.num_polar_all[ chosen_pattern_num ]
                                     ] )
            
        return np.array( chosen_patterns )
    
    '''
    Function to write pattern files
    '''
    def write_pattern_files( self, pattern_nums ):
        
        for pattern_num in pattern_nums:
            folder_name = 'area_frac_deviation_patterns/{}_deviation_indus/'.format( quantify_dev )
            
            file_name   = '{}_{}_dev_{}_area_frac.dat'.format( self.ligand, self.quantify_dev, pattern_num )
                
            write_pattern_file( self.patterns_all[ pattern_num ],
                               8,
                               folder_name,
                               file_name
                               )
    
    '''
    Function to call write_pattern_files
    '''
    def write_master_function( self ):
        
        pattern_list = np.concatenate( self.chosen_patterns[:,1] )
        
        self.write_pattern_files( pattern_list )
            
        out_file = '{}_{}_most_{}_dev_patterns_per_bin.txt'.format( ligand, n_choose, quantify_dev )
            
        np.savetxt( out_file,
                   pattern_list,
                   fmt = '%d'
                   )
    
    '''
    Compute method
    '''
    def compute_max_dev_area_frac( self ):
        ## ASSIGN PURE HFE VALUES
        self.assign_pure_hfe()
        ## MAKE LINEAR INTERPOLATION BASED ON POLAR AREA FRAC
        self.linear_interpolation = self.make_linear_interpolation( self.polar_area_frac_all,
                                                                   self.hfe_nonpolar,
                                                                   self.hfe_polar
                                                                   )
        ## CALCULATE DEVIATION
        self.calc_deviation()
        ## MAKE INDUS MASK
        self.indus_mask = self.make_mask( 'indus' )
        ## MAKE PATTERN_NUM ARRAY
        self.pattern_num = np.arange( self.polar_area_frac_all.shape[0] )[ :, None ]
        ## ALLOCATE BINS TO POLAR_AREA_FRAC_ALL
        self.area_frac_bins = self.allocate_bins( self.polar_area_frac_all,
                                                 self.bins
                                                 )
        ## CHOOSE BINS WITH NUMBER OF SAMPLES ABOVE THRESHOLD
        self.bins_chosen = self.select_bins_above_threshold( self.histogram,
                                                            self.num_threshold
                                                            )
        ## ITERATE THROUGH CHOSEN BINS, CHOOSE N_CHOOSE PATTERNS PER BIN
        self.chosen_patterns = self.choose_n_patterns_per_bin()
        ## RUN MASTER WRITE FUNCTION
        self.write_master_function()
        

if __name__ == "__main__":
    
    testing = True
    
    if testing:
        n_current = 1500
        bin_width = 0.04
    else:
        n_current = int( sys.argv[1] )
        bin_width = float( sys.argv[2] )
    ## DEFINE FILE AND FOLDER NAMES
    ligand = 'hydrox'
    base_dir = '../../active_learning_hydrox/'
    folder_name = base_dir + 'active_learning_patterns/'
    file_name = 'gpr_prediction_iteration_{:d}.pickle'.format( n_current )
    
    ## DEFINE MASTER FOLDER AND MASTER FILE NAME
    master_folder_name = base_dir +  'pattern_labels/'
    master_file_name = 'master_patterns_labels.pickle'
    
    ## DEFINE PARAMETERS FOR NUMBER OF PATTERNS TO CHOOSE ETC.
    n_choose = 2 # Choose n_choose patterns with highest -ve dev. Can easily modify code to choose +ve dev
    quantify_dev = 'positive' # positive or negative
    threshold_num_patterns = 4000
    
#    ## WHETHER TO CHOOSE FROM A SINGLE NUMBER OF POLAR GROUPS OR NOT
#    choose_given_num_polar = True
#    num_polar = 4
    
    ## DEFINE OUTPUT FILE NAME
#    if not choose_given_num_polar:
#        out_file = '{}_{}_most_{}_dev_patterns.txt'.format( ligand, n_choose, quantify_dev )
#    else:
#        out_file = '{}_{}_most_{}_dev_patterns_num_polar_{}.txt'.format( ligand, n_choose, quantify_dev, num_polar )
        
        
    ## CREATE OBJECTS OF HISTOGRAM_AREAFRAC CLASS
    choose_max_dev = Choose_MaxDev_AreaFrac( folder_name,
                                            file_name, 
                                            master_folder_name, 
                                            master_file_name, 
                                            n_current,
                                            ligand,
                                            bin_width = bin_width,
                                            n_choose = n_choose,
                                            quantify_dev = quantify_dev,
                                            num_threshold = threshold_num_patterns
                                            )
    
    ## RUN COMPUTE_MAX_DEV FUNCTION
   # choose_max_dev.compute_max_dev_area_frac()
    
    
    
    
    
    
    
    
