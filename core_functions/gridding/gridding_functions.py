# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 04/06/2021

"""
'''

Script with functions for gridding

'''


import numpy as np

'''
Function to make a hexagonal grid as follows

    x  x  x  x  x
   x  x  x  x  x
    x  x  x  x  x
   x  x  x  x  x
   <-->
   
    Inputs:
        - n_x: Number of grid points in the x-dimension (horizontal) [ 5 in given example ]
        - n_y: Number of grid points in the y-dimension (vertical) [ 4 in given example ]
        - r: Distance between grid points shown by the dimension arrow

'''
def make_hex_grid( n_x, n_y, r ):
    grid_indexing = np.arange( int(n_x * n_y) )
    grid_x = np.mod( grid_indexing, n_x ) * r + np.mod( grid_indexing//n_x , 2 ) * -r/2
    grid_y = grid_indexing // n_x * - 3**0.5/2 * r
    return grid_x, grid_y

'''
Function acts the same way as make_hex_grid but makes a padded grid as follows

    o  o  o  o  o  o
     o  x  x  x  x  o
    o  x  x  x  x  o
     o  x  x  x  x  o
    o  x  x  x  x  o
     o  o  o  o  o  o
'''
def make_padded_hex_grid( n_x, n_y, r ):
    grid_indexing = np.arange( int(n_x * n_y) )
    grid_x = np.mod( grid_indexing, n_x ) * r + np.mod( grid_indexing//n_x , 2 ) * +r/2
    grid_y = grid_indexing // n_x * - 3**0.5/2 * r
    return grid_x, grid_y


'''
Function to make amenable pattern array (basically a pattern array instead of an array of arrays)
'''
def make_patterns_array( patterns ):
    arr = [ x for x in patterns ]
    return np.array( arr )    
