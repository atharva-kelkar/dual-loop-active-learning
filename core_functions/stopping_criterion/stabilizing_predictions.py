# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 07/07/2021
"""
'''

This code calculates the distance between consecutive GPR predictions either as
    a. Bhattacharyya distance
    b. Euclidean distance
    c. Scaled Euclidean distance (Scaled by value of the absolute value of the prediction mean)

'''

import numpy as np
import pickle
import socket
if "swarm" in socket.gethostname():
    import matplotlib
    matplotlib.use('agg')
import matplotlib.pyplot as plt
from analysis_scripts.python_scripts.plotting.plotting_functions import format_axes

'''
Function loads GPR prediction pickle given folder, file prefix, file postfix ('.pickle'), 
    and index of prediction file
'''
def load_gpr_prediction( folder, prefix, postfix, n ):
    return np.load( folder + prefix + str(n) + postfix, 
                   allow_pickle = True 
                   )
    
'''
Function returns Euclidean distance between two vectors x and y
'''
def euclidean_norm( x, y ):
    return(np.sqrt((x - y)**2))

'''
Function returns Bhattacharyya distance between two vectors x and y
'''
def bhattacharyya_distance( x, y ):
    BC = np.sum(np.sqrt( np.abs(x) * np.abs(y) ))
    return -np.log(BC)
   
'''
Function returns Euclidean distance between consecutive GPR predictions in matrix stacked with GPR predictions
'''
def find_consecutive_euclidean_distance( X ):
    dist = [ euclidean_norm(X[:,i], X[:,i+1]) for i in range(X.shape[1]-1) ]
    return np.array(dist)

'''
Function returns scaled Euclidean distance between consecutive GPR predictions in matrix stacked with GPR predictions
'''
def find_consecutive_scaled_euclidean_distance( X ):
    dist = [ euclidean_norm(X[:,i], X[:,i+1])/np.abs(X[:,i]) for i in range(X.shape[1]-1) ]
    return np.array(dist)

'''
Function returns Bhattacharyya distance between consecutive GPR predictions in matrix stacked with GPR predictions
'''
def find_consecutive_bhattacharyya_distance( X ):
    dist = [ bhattacharyya_distance(X[:,i], X[:,i+1]) for i in range(X.shape[1]-1) ]
    return np.array(dist)

'''
Function calculates moving average of x over a horizon w
'''
def moving_average(x, w):
    return np.convolve(x, np.ones(w), 'valid') / w

'''
Function to eliminate value above given threshold
'''
def eliminate_above_threshold( y , threshold ):
    return np.delete( y, np.where( y > threshold) )

if __name__ == "__main__":
    
    ligand = 'amide'
    
    base_folder = '/home/kelkar2/analysis_scripts/python_scripts/gpr/active_learning_{}/'.format( ligand )
    predictions_folder = 'active_learning_patterns/'
    predictions_prefix = 'gpr_prediction_iteration_'
    predictions_postfix = '.pickle'
    n_start = 52
    n_end = 1500
    stabilization_indices = np.load( 'stabilizing_predictions_set.pickle', 
                                    allow_pickle = True 
                                    )
    
    figure_name = 'figures/20210707_sp_euclidean_distance_{}_movAvg_10_{}.svg'.format( ligand, n_end )
    
    '''
    Number of predictions will be end_value - start_value + 1
    '''
    num_predictions = n_end - n_start + 1
    
    '''
    Load sample pickle to get shape of arrays to be made
    '''
    sample_data = load_gpr_prediction( base_folder + predictions_folder, 
                                      predictions_prefix,
                                      predictions_postfix,
                                      n_start
                                      )
    
    '''
    Define zeros array to be populated with values of GPR predictions
    '''
    shape = [ sample_data['mu'].shape[0], num_predictions ]
    mu_all = np.zeros( shape )
    sigma_all = np.zeros( shape )
    
    '''
    Populate mu and sigma arrays with all mu values
    '''
    for i in range( n_start, n_end + 1 ):
        index = i - n_start
        data_curr = load_gpr_prediction( base_folder + predictions_folder, 
                                      predictions_prefix,
                                      predictions_postfix,
                                      i
                                      )
        mu_all[ :, index ][:,None] = data_curr['mu']
        sigma_all[ :, index ][:,None] = data_curr['sigma']
    
    distance_between_predictions = find_consecutive_scaled_euclidean_distance( mu_all )
    
    # Choose subset of 500 predictions, called "stop set"
    distance_between_predictions_500 = distance_between_predictions[ :, stabilization_indices ]
    
    
    fig, ax = plt.subplots()
    y = np.mean(distance_between_predictions_500, axis = 1)
    y = eliminate_above_threshold( y, 0.8 )
    y = moving_average(y, 10)
    
    x = np.arange(n_start, y.shape[0] + n_start)

    ax.plot( x, y )
    ax.set_xlabel('Prediction #', fontsize = 15)
    ax.set_ylabel('SP (Euclidean distance)', fontsize = 15)
    ax.tick_params(axis='both', which='major', labelsize= 12)
    format_axes( ax, labelsize = 14 )
    plt.savefig( figure_name, 
                bbox_inches = 'tight',
                format = figure_name.split('.')[-1]
                )
    
    
    
    
    
    

'''
Trash code
'''

#    fig, ax = plt.subplots()
#    y = np.mean(distance_between_predictions, axis = 1)
#    y = eliminate_above_threshold( y, 1 )
#    y = moving_average(y, 10)
#    
#    x = np.arange(n_start, y.shape[0] + n_start)
#    
#    
#    ax.plot( x , y)
#    ax.set_xlabel('Prediction #')
#    ax.set_ylabel('Scaled Euclidean distance between predictions')
#    plt.show()
