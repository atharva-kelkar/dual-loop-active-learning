# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 11/19/2020

"""

import numpy as np
import sys
from active_learning_functions import find_pairwise_distance,\
                                        calc_kernel_matrix,\
                                        multiply_three_matrices, \
                                        calc_mu_f_all, \
                                        calc_sigma_f_all, \
                                        calc_Z_all, \
                                        calc_u_all, \
                                        calc_linear_interpolation, \
                                        write_pattern_file
                                        
from make_hypercube import gen_all_patterns
from scipy.stats import norm


def center( X ):
    return np.mean(X), np.std(X), X - np.mean(X)

'''
Function that returns mean, standard deviation, and (y-mean)/std_dev for a given y array
'''
def normalize( X ):
    return np.mean(X), np.std(X), (X - np.mean(X))/np.std(X)

def center_to_zero( X ):
    return np.mean(X), np.std(X), X - X

def find_argmax_max( X ):
    return np.where( X == X.max() )[0][0], X.max()

'''
Function that returns (y-mean)/std_dev for a given y, mean, and std dev
'''
def normalize_given_mean( y, mean, std ):
    return (y - mean)/std

if __name__ == "__main__":
    testing = True
    
    if testing:
        pattern_pickle_file = '../seed_pattern_generation/compiled_pickle_patterns.pickle'
        pattern_labels_file = '../seed_pattern_generation/seed_pattern_labels.txt'
        gamma = np.sqrt(2) # Width of Gaussian kernel - hyperparameter to be optimized
        n_dim = 16 # Dimensionality of pattern space
        ksi = 0.01 # Hyperparameter controlling exploration-exploitation trade-off
        function_type = 'max deviation'
        n_tot = 8
        output_folder = './'
        output_file = 'wc_seed_pattern'
        
    '''
    Load pickle of all seed patterns
    '''
    seed_patterns = np.load( pattern_pickle_file )
    seed_pattern_labels = np.loadtxt( pattern_labels_file )
    # Remove this line after testing
#    seed_patterns = seed_patterns[:10, :4]
#    seed_pattern_labels = seed_pattern_labels[:10]
    
    mean, std, seed_pattern_labels = normalize( seed_pattern_labels )
    
    '''
    Find pairwise distance between all seed patterns
    '''
    distance_matrix = find_pairwise_distance( seed_patterns , -1 )
    
    '''
    Define kernel matrix given pairwise distance matrix
    '''
    kernel_matrix_seed = calc_kernel_matrix( distance_matrix, gamma )
    
    '''
    Define heteroskedastic noise vector. Temporarily it's set as homoskedastic noise with
    sigma_rep = 2
    '''
    sigma_vector = np.zeros( (seed_patterns.shape[0], 1) )
    sigma_vector[:] = 2/std # Value of sigma_{rep}
    
    '''
    Calculate noisy kernel inverse
    '''
    noisy_kernel = kernel_matrix_seed + sigma_vector**2 * np.eye(kernel_matrix_seed.shape[0])
    noisy_kernel_inverse = np.linalg.inv( noisy_kernel )
    
    '''
    Generate matrix with all patterns
    '''
    patterns_all = gen_all_patterns( n_dim )
    
    '''
    Make distance matrix and kernel matrix of pairwise distances 
    between all patterns and seed patterns
    '''
    distance_matrix_all = find_pairwise_distance( patterns_all, seed_patterns )
    kernel_matrix_all = calc_kernel_matrix( distance_matrix_all, gamma )
    
    '''
    Find mu_f for all patterns
    '''
    mu_f_all = calc_mu_f_all( kernel_matrix_all, \
                             noisy_kernel_inverse, \
                             seed_pattern_labels \
                             )
    '''
    Define distance matrix of all points with themselves (basically 0-matrix)
    Then define kernel matrix using this self-distance matrix
    '''
    self_distance_matrix = np.zeros( (patterns_all.shape[0], 1) )
    self_kernel_matrix = calc_kernel_matrix( self_distance_matrix, gamma )
    
    '''
    Calculate sigma_f for all patterns 
    '''
#    sigma_f_all = self_kernel_matrix - multiply_three_matrices( kernel_matrix_all, \
#                                                               noisy_kernel_inverse, \
#                                                               kernel_matrix_all.T \
#                                                               )
    sigma_f_all = calc_sigma_f_all( self_kernel_matrix, \
                                   kernel_matrix_all, \
                                   noisy_kernel_inverse \
                                   )
    
    '''
    Define Z array given mu_f, max value of current array, hyperparameter xi, 
    '''
    if function_type == 'classical':
        y_max = seed_pattern_labels.max()
    elif function_type == 'max deviation':
        hfe_0 = seed_pattern_labels.min()
        hfe_1 = seed_pattern_labels.max()
        mu_g_all = calc_linear_interpolation( patterns_all, hfe_0, hfe_1 )
#        mu_g_all = normalize_given_mean( mu_g_all, mean, std ) # Redundant since already calculated on normalized linear interpolation
    
    
    '''
    Define mu_delta_all = mu_f_all - mu_g_all
    
    Define delta_max --> mu_delta_all.max()
    '''
    mu_delta_all = mu_f_all - mu_g_all # Since all points show positive deviations, use just the f(x) - g(x) instead of |f(x) - g(x)|
    mu_delta_with_sign = mu_f_all - mu_g_all
    sigma_delta_all = sigma_f_all
    Z_all = calc_Z_all( mu_delta_all, \
                       mu_delta_all.max(), \
                       ksi, \
                       sigma_delta_all\
                       )
    
    '''
    Define acquisition function u(x/X) as expectation of improvement
    '''
    u_x_given_X = calc_u_all( mu_delta_all, \
                             mu_delta_all.max(), \
                             ksi, \
                             Z_all, \
                             sigma_f_all \
                             )
    
    '''
    Define mask of all seed patterns for ease of debugging
    '''
    mask = [ np.all( patterns_all == x, axis = 1 ) for x in seed_patterns ]
    mask = np.array( mask )
    mask = np.any( mask, axis = 0 )
    
    '''
    Define matrices based on mask (patterns which are new, and not part of seed set)
    '''
    mu_f_new = mu_f_all[ ~mask ]
    sigma_f_new = sigma_f_all[ ~mask ]
    u_x_given_X_new = u_x_given_X[ ~mask ]
    patterns_new = patterns_all[ ~mask ]
    
    mu_delta_new = mu_delta_all[ ~mask ]
    sigma_delta_new = sigma_delta_all[ ~mask ]
    u_x_given_X_new = u_x_given_X[ ~mask ]
    patterns_new = patterns_all[ ~mask ]
    mu_delta_with_sign_new = mu_delta_with_sign[ ~mask ]
    
    '''
    Store where max predicted mean and standard deviation are, and where max of acq func is
    '''
    argmax_mu_f, max_mu_f = find_argmax_max( mu_f_new )
    argmax_mu_delta, max_mu_delta = find_argmax_max( mu_delta_new )
    argmax_mu_delta_with_sign, max_mu_delta_with_sign = find_argmax_max( mu_delta_with_sign_new )
    
    
    argmax_sigma_f, max_sigma_f = find_argmax_max( sigma_f_new )
    argmax_sigma_delta, max_sigma_delta = find_argmax_max( sigma_delta_new )
    
    argmax_u, max_u = find_argmax_max( u_x_given_X_new )
    
    
    '''
    Print all the relevant output values
    '''

    print('\n~~~~~~~~~~~~~~ ksi = {} ~~~~~~~~~~~~~~~\n'.format( ksi ) )
    
    print('max predicted mean is at - {}, value is {:.4f}'.format( argmax_mu_f, \
                                                                  max_mu_f ) )
    
    print('max predicted mean of Delta is at - {}, value is {:.4f}'.format( argmax_mu_delta, \
                                                                  max_mu_delta ) )

    print('max predicted std is at - {}, value is {:.4f}\n'.format( argmax_sigma_f, \
                                                                  max_sigma_f ) )
    
    print('max acquisition function is at - {}, value is {:e}\n'.format( argmax_u,\
                                                         max_u ) )
    print('max acquisition predicted mean is {:.4f} and predicted std dev is {:.4f}\n'.format\
                                                          (mu_f_new[argmax_u][0],\
                                                           sigma_f_new[argmax_u][0] ) )
    print('max acquisition predicted mean of Delta is {:.4f} and predicted std dev is {:.4f}\n'.format\
                                                          (mu_delta_new[argmax_u][0],\
                                                           sigma_f_new[argmax_u][0] ) )
    
    print('max acquisition predicted mean of signed Delta is {:.4f}'.format\
                                                          (mu_delta_with_sign_new[argmax_u][0]))
    
    print('\n\n')
    
    '''
    Store pattern obtained in a matrix like in the seed generation case
    '''
    output_file = output_file + '_' + str( seed_patterns.shape[0] ) + '.dat'
#    write_pattern_file( patterns_new[ argmax_u ],
#                       n_tot,
#                       output_folder,
#                       output_file
#                       )
    
    
    
    
    
    
    
    
    
    
    
    
