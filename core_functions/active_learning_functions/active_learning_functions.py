# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 11/19/2020

Program containing functions to run active learning framework

"""

import numpy as np
from scipy.stats import norm
import pickle

'''
Function returns pairwise distance between all points in matrix X
'''
def find_pairwise_distance( X , y = -1 ):
    if np.sum(y) == -1:
        y = X
        
    return np.linalg.norm( X[:, None, :] - y[None, :, :], axis = -1 )


'''
Function returns pairwise distance between all points in matrix X
'''
def find_pairwise_distance_memefficient( X , y = -1 ):
    if np.sum(y) == -1:
        y = X
        
    return np.linalg.norm( X[:, None, :] - y[None, :, :], axis = -1 )


'''
Function returns kernel matrix given pairwise distances and width of Gaussian kernel
'''
def calc_kernel_matrix( D, gamma ):
    '''
    Square distance matrix
    '''
    D_square = D**2
    
    '''
    Calculate and return kernel matrix given width of Gaussian kernel
    '''
    return np.exp( -1/(2*gamma**2) * D_square )
    

'''
Function multiplies 3 matrices using matmul in serial steps
'''
def multiply_three_matrices( X, Y, z):
    '''
    Multiply first two matrices
    '''
    XY = np.matmul( X, Y )
    '''
    Multiply third matrix to product of first 2 and return all matrices
    '''
    return np.matmul( XY, z )
    
'''
Function calculates mu_f of points to be predicted
'''
def calc_mu_f_all( kT, K_plus_sigma_I_inv, y ):
    '''
    Inputs:
        - kT : [k(x1, x*), k(x2, x*), k(x3, x*) ... k(xn, x*)]
        - K_plus_sigma_I_inv : (K + sigma*I)^-1
        - y : array of labels
    Formula used:
        - mu_f = k^T[K + sigma*I ]^-1 y
    Returns:
        - mu_f (Scalar)
    '''
    return multiply_three_matrices( kT, K_plus_sigma_I_inv, y )[:, np.newaxis ]

'''
Function calculates sigma_f of points to be predicted
'''
def calc_sigma_f_all( k_self, kernel_matrix_all, K_plus_sigma_I_inv ):
    '''
    Inputs:
        - k_self : array of kernel values of points with themselves
        - kernel_matrix_all : kernel values of points to be predicted with seed points
        - K_plus_sigma_I_inv: (K + sigma*I)^-1
    '''
    return k_self - np.sum( kernel_matrix_all * \
                           np.matmul( kernel_matrix_all, \
                                     K_plus_sigma_I_inv), \
                            axis = 1 )[:, None]
    
'''
Function calculates Z (normalized value) of mu_f array
'''
def calc_Z_all( mu_f, y_x_plus, ksi, sigma_f ):
    Z = (mu_f - y_x_plus - ksi)/sigma_f
    Z[ np.isnan(Z) ] = 0
    return Z
    
'''
Function defines expectation of improvement using the GPR surrogate model
'''
def calc_u_all( mu_f, y_x_plus, ksi, Z, sigma_f ):
    EI_x = ( mu_f - y_x_plus - ksi ) * norm.cdf( Z ) + sigma_f * norm.pdf( Z )
    EI_x[ np.isnan(EI_x) ] = 0
    return EI_x
    
'''
Function defines linear interpolation based on mole fraction of polar and nonpolar groups
and values at extremes
'''
def calc_linear_interpolation( patterns_all, hfe_0, hfe_1 ):
    return (( np.sum( patterns_all, axis = 1 ) * hfe_1 \
            + (patterns_all.shape[1] - np.sum( patterns_all, axis = 1 )) * hfe_0 )/\
            patterns_all.shape[1])[:, None]
    
'''
Function writes file given pattern, output folder, and output file name
'''    
def write_pattern_file( pattern, n_tot, output_folder, output_file ):
    n = int(pattern.shape[0]**(1/2))
    pattern = pattern.reshape( n, n )
    full_pattern_arr = np.zeros( (n_tot, n_tot) )
    full_pattern_arr[:] = 2
    full_pattern_arr[:n, :n] = pattern
    np.savetxt(output_folder + output_file, full_pattern_arr.astype('int'), fmt = '%1d')

'''
Function writes predicted mean and std of all points given
    mean, sigma, output folder, output file name, pattern number
'''
def write_mean_std( mu, sigma, output_folder, output_file, pattern_num ):
    dict = { 'mu': mu,
            'sigma': sigma
            }
    file_name = output_folder + output_file + str(pattern_num) + '.pickle'
    pickle.dump( dict, open(file_name, 'wb') )










