# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 11/19/2020

"""

import numpy as np


def get_bin(x, n=0):
    """
    Get binary representation of x

    Input
    ----------
    x : int
    n : int
        Minimum number of digits. If x needs less digits in binary, the rest
        is filled with zeros.

    Returns
    -------
    str
    """
    return format(x, 'b').zfill(n)

def split_to_int(x):
    """
    Split x into integer array of each character of x

    Input
    ----------
    x : str

    Returns
    -------
    numpy array of data type int8 and length = len(x)
    """
    return np.array([int(char) for char in x]).astype('int8')

def gen_all_patterns( n ):
    """
    Get all 1/0 patterns of n-dimensions
    Alternatively, get all vertices of an n-dimensional hypercube

    Input
    ----------
    n : int

    Returns
    -------
    numpy array
    """
    patterns_int = np.arange( 2**n )
    patterns = [ split_to_int( get_bin(pattern_int, n) ) for pattern_int in patterns_int ]
    return np.array( patterns )

if __name__ == "__main__":
    n = 16
    patterns_int = np.arange( 2**n )
    patterns = gen_all_patterns( n )