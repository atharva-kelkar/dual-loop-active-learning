# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 11/19/2020

"""

import numpy as np
import sys
sys.path.append('/home/kelkar2/analysis_scripts/python_scripts/gpr/core_functions/')
from active_learning_functions.active_learning_functions import find_pairwise_distance,\
                                                                            calc_kernel_matrix,\
                                                                            multiply_three_matrices, \
                                                                            calc_mu_f_all, \
                                                                            calc_sigma_f_all, \
                                                                            calc_Z_all, \
                                                                            calc_u_all, \
                                                                            calc_linear_interpolation, \
                                                                            write_pattern_file
                                        
from active_learning_functions.make_hypercube import gen_all_patterns

from scipy.stats import norm

'''
Function that returns mean, standard deviation, and (y-mean)/std_dev for a given y array
'''
def normalize( X ):
    return np.mean(X), np.std(X), (X - np.mean(X))/np.std(X)

def find_argmax_max( X ):
    return np.where( X == X.max() )[0][0], X.max()

'''
Function that returns (y-mean)/std_dev for a given y, mean, and std dev
'''
def normalize_given_mean( y, mean, std ):
    return (y - mean)/std

'''
Function to calculate RMSE
'''
def calc_rmse( X, Y ):
    return np.sqrt( np.sum((X - Y)**2)/X.shape[0] )

'''
Class to select hyperparameter
'''
class Hyperparameter_Selection():
    
    '''
    __init__ constructor
    '''
    def __init__( self,
                 pattern_pickle_file = 'compiled_pickle_patterns.pickle',
                 pattern_labels_file = 'labels/seed_pattern_labels_amine.txt',
                 gamma = np.sqrt(2),
                 index_file = '5_fold_indices_52.pickle'
                 ):
        ## ALLOCATE ALL VARIABLES TO SELF
        self.pattern_pickle_file = pattern_pickle_file
        self.pattern_labels_file = pattern_labels_file
        self.gamma = gamma
        self.index_file = index_file
        
        ## ALLOCATE DEFAULT VALUES
        self.n_dim = 16 # Dimensionality of pattern space
        self.sigma_rep = 2 # Since all patterns are only INDUS

    '''
    Function to load seed patterns and seed pattern labels
    '''
    def load_seed_data( self ):

        return np.load( self.pattern_pickle_file, allow_pickle = True ), np.loadtxt( self.pattern_labels_file )
    
    '''
    Choose validation and training set based on "fold" - validation set indices
    '''
    @staticmethod
    def choose_validation_set( patterns, fold ):
        
        val_set = patterns[ fold ]
        train_set = np.delete( patterns, 
                              fold,
                              axis = 0
                              )
        
        return train_set, val_set
        

    '''
    Compute function to run all computations
    '''        
    @staticmethod
    def compute_single_fold( seed_patterns, seed_pattern_labels, val_patterns, val_pattern_labels, sigma_rep, gamma ):
        ## NORMALIZE ALL SEED PATTERN LABELS
        mean, std, seed_pattern_labels = normalize( seed_pattern_labels )

        ## FIND PAIRWISE DISTANCE BETWEEN ALL SEED PATTERNS
        distance_matrix = find_pairwise_distance( seed_patterns , -1 )
        
        ## CALCULATE KERNEL MATRIX FROM SEED PATTERNS
        kernel_matrix_seed = calc_kernel_matrix( distance_matrix, gamma )
        
        ## DEFINE SIGMA_REP MATRIX WITH VALUE OF SIGMA_REP = 2
        sigma_vector = np.zeros( (seed_patterns.shape[0], 1) )
        sigma_vector[:] = sigma_rep/std # Value of sigma_{rep}

        ## CALCULATE NOISY KERNEL AND ITS INVERSE
        noisy_kernel = kernel_matrix_seed + sigma_vector**2 * np.eye( kernel_matrix_seed.shape[0] )
        noisy_kernel_inverse = np.linalg.inv( noisy_kernel )
        
        ## DEFINE KERNEL MATRIX BASED ON DISTANCE BETWEEN VAL_PATTERNS AND SEED_PATTERNS
        distance_matrix_val = find_pairwise_distance( val_patterns, seed_patterns )
        kernel_matrix_val = calc_kernel_matrix( distance_matrix_val, gamma )
    
        ## CALCULATE MU FOR ALL SEED PATTERNS
        mu_f_all = calc_mu_f_all( kernel_matrix_val,
                                 noisy_kernel_inverse,
                                 seed_pattern_labels
                                 )
        
        ## CALCULATE DISTANCE MATRIX FROM SELF
        self_distance_matrix = np.zeros( (val_patterns.shape[0], 1) )
        self_kernel_matrix = calc_kernel_matrix( self_distance_matrix, gamma )
        
        ## CALCULATE SIGMA FOR ALL NEW PATTERNS
        sigma_f_all = calc_sigma_f_all( self_kernel_matrix,
                                       kernel_matrix_val,
                                       noisy_kernel_inverse
                                       )
        
        return np.array([ (mu_f_all * std + mean).squeeze(), val_pattern_labels ]) , \
                np.corrcoef( [mu_f_all.squeeze(), val_pattern_labels] )[0,1], \
                calc_rmse( (mu_f_all * std + mean).squeeze(), val_pattern_labels ), \
                np.array( np.mean( sigma_f_all * std ) )

    '''
    Function to run n-fold CV, n = 5
    '''
    def compute_nfold_CV( self ):
        ## LOAD PICKLE OF SEED PATTERNS
        self.seed_patterns, self.seed_pattern_labels = self.load_seed_data()
        
        ## LEAVE OUT PURE SURFACES
        self.seed_patterns = self.seed_patterns[:-2]
        self.seed_pattern_labels = self.seed_pattern_labels[:-2]
        
        ## LOAD INDICES FOR VALIDATION
        indices = np.load( self.index_file,
                          allow_pickle = True
                          )
        
        ## DEFINE BLANK RESULTS ARRAY
        res = []
        corr = []
        rmse = []
        std = []
        
        ## LOOP THROUGH ALL INDICES
        for fold in indices:
            
            ## MAKE VALIDATION AND TRAINING SET FOR PATTERNS
            train_seed_patterns, val_seed_patterns = self.choose_validation_set( self.seed_patterns, 
                                                                                fold 
                                                                                )
            
            ## MAKE VALIDATION AND TRAINING SET FOR LABELS
            train_seed_labels, val_seed_labels = self.choose_validation_set( self.seed_pattern_labels,
                                                                            fold
                                                                            )
            
            temp_res, temp_corr, temp_rmse, temp_std =  self.compute_single_fold( train_seed_patterns,
                                                                                 train_seed_labels,
                                                                                 val_seed_patterns,
                                                                                 val_seed_labels,
                                                                                 self.sigma_rep,
                                                                                 self.gamma 
                                                                                 )
            
            ## RUN SINGLE FOLD COMPUTE AND APPEND TO RESULTS ARRAY
            res.append( [ temp_res ] )
            corr.append( [ temp_corr ] )
            rmse.append( [ temp_rmse ] )
            std.append( [ temp_std ] )
            
            
            self.results = res
            self.corr = corr
            self.rmse = rmse
            self.std = std


if __name__ == "__main__":
    testing = True
    
    if testing:
        
        gamma = np.sqrt(2) # Width of Gaussian kernel - hyperparameter to be optimized
        n_dim = 16 
        ksi = 0.01 
        function_type = 'max deviation'
        n_tot = 8
        output_folder = './'
        output_file = 'wc_seed_pattern'
        pattern_pickle_file = 'compiled_pickle_patterns.pickle'
        pattern_labels_file = 'labels/seed_pattern_labels_amide.txt'
        
    ## MAKE HYPERPARAMETER SELECTION OBJECT
#    hyperparam_opt = Hyperparameter_Selection()
    
    ## RUN COMPUTE FUNCTION
#    hyperparam_opt.compute_nfold_CV()
    
    ## DEFINE LAMBDA ARRAY
    gamma_arr = np.array( [ 1, 2**(1/2), 2, 5, 10, 15, 30 ] )
    
    res = []
    corr = []
    rmse = []
    std = []
    
    ## ITERATE OVER GAMMA ARRAY
    for gamma in gamma_arr:
        hyperparam_opt = Hyperparameter_Selection( gamma = gamma,
                                                  pattern_pickle_file = pattern_pickle_file,
                                                  pattern_labels_file = pattern_labels_file,
                                                  index_file = '5_fold_indices_50.pickle'
                                                  )
        
        hyperparam_opt.compute_nfold_CV()
        
        corr.append( [ hyperparam_opt.corr ] )
        res.append( [ hyperparam_opt.results ] )
        rmse.append( [ hyperparam_opt.rmse ] )
        std.append( [ hyperparam_opt.std ])
        
    corr = np.array( corr ).squeeze()
    rmse = np.array( rmse ).squeeze()
    std = np.array( std ).squeeze()
    
    rmse_mean = rmse.mean( axis = 1 )
    corr_mean = corr.mean( axis = 1 )
    
#    res = np.array( res ).squeeze()
        
        
        
        
        
        
        
        
        
        
        
        
    
    
    
    
    
    
