#!/bin/bash

echo "Hello from trajectory_tools.sh" 

function center_patterned_trajectory () {

	base_folder=$1
	start_time=$2
	dump_time=$3
	traj_type=${4:-prod}
	ligand=${5:-HYD}
	ligand_atom=${6:-O41}

	source ~/.bashrc	
	cd $base_folder
	# Center patterned 5x5 patch on SAM

	sim_name=sam

    # Step 1 - Make index file with patch indices
    gmx make_ndx -f ${sim_name}_${traj_type}.gro -o index.ndx <<END
r TRI & a C38
r ${ligand} & a ${ligand_atom}
8 | 9
name 10 patch
q
END

    # Step 2 - Convert trajectory to a nojump trajectory
    gmx trjconv -f ${sim_name}_${traj_type}.xtc -s ${sim_name}_${traj_type}.tpr -pbc nojump -o ${sim_name}_${traj_type}_nojump.xtc <<INPUT
0
INPUT

    # Step 3 - Center nojump trajectory to patch
    gmx trjconv -f ${sim_name}_${traj_type}_nojump.xtc -s ${sim_name}_${traj_type}.tpr -n index.ndx -center -o ${sim_name}_${traj_type}_nojump_center.xtc <<INPUT
patch
0
INPUT

    # Step 4 - Convert trajectory to pbc mol
    gmx trjconv -f ${sim_name}_${traj_type}_nojump_center.xtc -s ${sim_name}_${traj_type}.tpr -pbc mol -o ${sim_name}_${traj_type}_nojump_center_mol.xtc <<INPUT
0
INPUT

    # Step 5 - Output the first frame of the centered, pbc mol trajectory as the gro file
    gmx trjconv -f ${sim_name}_${traj_type}_nojump_center_mol.xtc -s ${sim_name}_${traj_type}.tpr -b ${start_time} -dump ${dump_time} -o ${sim_name}_${traj_type}_nojump_center_mol.gro <<INPUT
0
INPUT

}


function center_single_component_patterned_trajectory () {

	base_folder=$1
	start_time=$2
	dump_time=$3
	residue=$4
	resnr=$5
	traj_type=$6

	source ~/.bashrc	
	cd $base_folder
	# Center patterned 5x5 patch on SAM

	sim_name=sam

    # Step 1 - Make index file with patch indices
    gmx make_ndx -f ${sim_name}_${traj_type}.gro -o index.ndx <<END
r ${residue} & a ${resnr}
name 7 patch
q
END

    # Step 2 - Convert trajectory to a nojump trajectory
    gmx trjconv -f ${sim_name}_${traj_type}.xtc -s ${sim_name}_${traj_type}.tpr -pbc nojump -o ${sim_name}_${traj_type}_nojump.xtc <<INPUT
0
INPUT

    # Step 3 - Center nojump trajectory to patch
    gmx trjconv -f ${sim_name}_${traj_type}_nojump.xtc -s ${sim_name}_${traj_type}.tpr -n index.ndx -center -o ${sim_name}_${traj_type}_nojump_center.xtc <<INPUT
patch
0
INPUT

    # Step 4 - Convert trajectory to pbc mol
    gmx trjconv -f ${sim_name}_${traj_type}_nojump_center.xtc -s ${sim_name}_${traj_type}.tpr -pbc mol -o ${sim_name}_${traj_type}_nojump_center_mol.xtc <<INPUT
0
INPUT

    # Step 5 - Output the first frame of the centered, pbc mol trajectory as the gro file
    gmx trjconv -f ${sim_name}_${traj_type}_nojump_center_mol.xtc -s ${sim_name}_${traj_type}.tpr -b ${start_time} -dump ${dump_time} -o ${sim_name}_${traj_type}_nojump_center_mol.gro <<INPUT
0
INPUT

}
